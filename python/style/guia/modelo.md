---
slug: nome do arquivo na página
tags: [COVID-19, Artigos LabRI] 
[comment]: # (As tags devem estar dentro dos [], com cada uma separada por vírgula)
title: "Seu Título Deve Vir Aqui"
subtitulo: Subtítulo da Publicação
author: Todos os Autores, separados por vírgulas
author_title: Títulação dos Autores, separados por vírgulas, mesma ordem dos autores
tipo_publicacao: Cadernos LabRI/UNESP - Núm. 4
[comment]: # (Este campo é para publicações dentro de um tipo, não é um problema deixar em branco)
image_url: https://i.imgur.com/72sXyjt.jpg
[comment]: # (Insira aqui o link da imagem que deseja estar com seu post no site )
descricao: Breve descrição do conteúdo do texto

---

[TOC]

[comment]: # (Toc é o sumário, não se preocupe, ele será gerado automaticamente)

### Título da primeira seçao (ível 1 / não é necessário repedir o tíitulo no corpo do texto)

orem ipsum dolor sit amet, consectetur adipiscing elit. Ut est nibh, rutrum vel malesuada non, volutpat nec odio. Curabitur posuere suscipit arcu. Fusce augue tortor, porttitor quis enim et, convallis vestibulum justo. Ut fringilla ornare odio, nec fringilla turpis feugiat a. Interdum et malesuada fames ac ante ipsum primis in faucibus. Vestibulum convallis felis nec elit vehicula vestibulum. Duis a iaculis sapien. Morbi dignissim lacinia ante, eu fringilla metus pulvinar a. Praesent dapibus elementum congue. Quisque aliquam interdum gravida. Sed consequat vel massa non tempus. Vivamus euismod metus ligula. Suspendisse potenti. Pellentesque varius dapibus sapien nec accumsan. Nulla magna odio, ullamcorper vitae diam sed, viverra euismod dolor.

<span>  Frase muito importante aqui </span>

Sed pretium orci in cursus viverra. Aenean quis felis condimentum, volutpat nisi non, porttitor urna. Curabitur ullamcorper risus eget mollis rhoncus. Cras sed diam at nisl varius venenatis at sit amet ante. Fusce ultricies risus eu lacus lacinia volutpat. Duis eget consequat lorem, vel auctor odio. Interdum et malesuada fames ac ante ipsum primis in faucibus. Nunc molestie dui sed nulla viverra fermentum.

Caso queira inserir uma imagem faça como na linha abaixo
`![Texto alternativo da imagem](link_da_imagem)`
<figcaption> Fonte: Sempre coloque fonte nas imagens</figcaption>


orem ipsum dolor sit amet, consectetur adipiscing elit. Ut est nibh, rutrum vel malesuada non, volutpat nec odio. Curabitur posuere suscipit arcu. Fusce augue tortor, porttitor quis enim et, convallis vestibulum justo. Ut fringilla ornare odio, nec fringilla turpis feugiat a. Interdum et malesuada fames ac ante ipsum primis in faucibus. Vestibulum convallis felis nec elit vehicula vestibulum. Duis a iaculis sapien. Morbi dignissim lacinia ante, eu fringilla metus pulvinar a. Praesent dapibus elementum congue. Quisque aliquam interdum gravida. Sed consequat vel massa non tempus. Vivamus euismod metus ligula. Suspendisse potenti. Pellentesque varius dapibus sapien nec accumsan. Nulla magna odio, ullamcorper vitae diam sed, viverra euismod dolor.

Sed pretium orci in cursus viverra. Aenean quis felis condimentum, volutpat nisi non, porttitor urna. Curabitur ullamcorper risus eget mollis rhoncus. Cras sed diam at nisl varius venenatis at sit amet ante. Fusce ultricies risus eu lacus lacinia volutpat. Duis eget consequat lorem, vel auctor odio. Interdum et malesuada fames ac ante ipsum primis in faucibus. Nunc molestie dui sed nulla viverra fermentum.


orem ipsum dolor sit amet, consectetur adipiscing elit. Ut est nibh, rutrum vel malesuada non, volutpat nec odio. Curabitur posuere suscipit arcu. Fusce augue tortor, porttitor quis enim et, convallis vestibulum justo. Ut fringilla ornare odio, nec fringilla turpis feugiat a. Interdum et malesuada fames ac ante ipsum primis in faucibus. Vestibulum convallis felis nec elit vehicula vestibulum. Duis a iaculis sapien. Morbi dignissim lacinia ante, eu fringilla metus pulvinar a. Praesent dapibus elementum congue. Quisque aliquam interdum gravida. Sed consequat vel massa non tempus. Vivamus euismod metus ligula. Suspendisse potenti. Pellentesque varius dapibus sapien nec accumsan. Nulla magna odio, ullamcorper vitae diam sed, viverra euismod dolor.

Sed pretium orci in cursus viverra. Aenean quis felis condimentum, volutpat nisi non, porttitor urna. Curabitur ullamcorper risus eget mollis rhoncus. Cras sed diam at nisl varius venenatis at sit amet ante. Fusce ultricies risus eu lacus lacinia volutpat. Duis eget consequat lorem, vel auctor odio. Interdum et malesuada fames ac ante ipsum primis in faucibus. Nunc molestie dui sed nulla viverra fermentum.



### Título da Segunda seção (nível 1)

**Texto que se deseja colocar em negrito**

orem ipsum dolor sit amet, consectetur adipiscing elit. Ut est nibh, rutrum vel malesuada non, volutpat nec odio. Curabitur posuere suscipit arcu. Fusce augue tortor, porttitor quis enim et, convallis vestibulum justo. Ut fringilla ornare odio, nec fringilla turpis feugiat a. Interdum et malesuada fames ac ante ipsum primis in faucibus. Vestibulum convallis felis nec elit vehicula vestibulum. Duis a iaculis sapien. Morbi dignissim lacinia ante, eu fringilla metus pulvinar a. Praesent dapibus elementum congue. Quisque aliquam interdum gravida. Sed consequat vel massa non tempus. Vivamus euismod metus ligula. Suspendisse potenti. Pellentesque varius dapibus sapien nec accumsan. Nulla magna odio, ullamcorper vitae diam sed, viverra euismod dolor.

Sed pretium orci in cursus viverra. Aenean quis felis condimentum, volutpat nisi non, porttitor urna. Curabitur ullamcorper risus eget mollis rhoncus. Cras sed diam at nisl varius venenatis at sit amet ante. Fusce ultricies risus eu lacus lacinia volutpat. Duis eget consequat lorem, vel auctor odio. Interdum et malesuada fames ac ante ipsum primis in faucibus. Nunc molestie dui sed nulla viverra fermentum.

#### Título de Subseção (nível 2)

orem ipsum dolor sit amet, consectetur adipiscing elit. Ut est nibh, rutrum vel malesuada non, volutpat nec odio. Curabitur posuere suscipit arcu. Fusce augue tortor, porttitor quis enim et, convallis vestibulum justo. Ut fringilla ornare odio, nec fringilla turpis feugiat a. Interdum et malesuada fames ac ante ipsum primis in faucibus. Vestibulum convallis felis nec elit vehicula vestibulum. Duis a iaculis sapien. Morbi dignissim lacinia ante, eu fringilla metus pulvinar a. Praesent dapibus elementum congue. Quisque aliquam interdum gravida. Sed consequat vel massa non tempus. Vivamus euismod metus ligula. Suspendisse potenti. Pellentesque varius dapibus sapien nec accumsan. Nulla magna odio, ullamcorper vitae diam sed, viverra euismod dolor[^1].

Sed pretium orci in cursus viverra. Aenean quis felis condimentum, volutpat nisi non, porttitor urna. Curabitur ullamcorper risus eget mollis rhoncus. Cras sed diam at nisl varius venenatis at sit amet ante. Fusce ultricies risus eu lacus lacinia volutpat. Duis eget consequat lorem, vel auctor odio. Interdum et malesuada fames ac ante ipsum primis in faucibus. Nunc molestie dui sed nulla viverra fermentum.

#### Título da Segunda sub-seção (Nível 2)

##### sub-sub-Seção (nível 3)

orem ipsum dolor sit amet, consectetur adipiscing elit. Ut est nibh, rutrum vel malesuada non, volutpat nec odio. Curabitur posuere suscipit arcu. Fusce augue tortor, porttitor quis enim et, convallis vestibulum justo. Ut fringilla ornare odio, nec fringilla turpis feugiat a. Interdum et malesuada fames ac ante ipsum primis in faucibus. Vestibulum convallis felis nec elit vehicula vestibulum. Duis a iaculis sapien. Morbi dignissim lacinia ante, eu fringilla metus pulvinar a. Praesent dapibus elementum congue. Quisque aliquam interdum gravida. Sed consequat vel massa non tempus. Vivamus euismod metus ligula. Suspendisse potenti. Pellentesque varius dapibus sapien nec accumsan. Nulla magna odio, ullamcorper vitae diam sed, viverra euismod dolor.

Sed pretium orci in cursus viverra. Aenean quis felis condimentum, volutpat nisi non, porttitor urna. Curabitur ullamcorper risus eget mollis rhoncus. Cras sed diam at nisl varius venenatis at sit amet ante. Fusce ultricies risus eu lacus lacinia volutpat. Duis eget consequat lorem, vel auctor odio. Interdum et malesuada fames ac ante ipsum primis in faucibus. Nunc molestie dui sed nulla viverra fermentum.

##### sub-sub-Seção (nível 3)

orem ipsum dolor sit amet, consectetur adipiscing elit. Ut est nibh, rutrum vel malesuada non, volutpat nec odio. Curabitur posuere suscipit arcu. Fusce augue tortor, porttitor quis enim et, convallis vestibulum justo. Ut fringilla ornare odio, nec fringilla turpis feugiat a. Interdum et malesuada fames ac ante ipsum primis in faucibus. Vestibulum convallis felis nec elit vehicula vestibulum. Duis a iaculis sapien. Morbi dignissim lacinia ante, eu fringilla metus pulvinar a. Praesent dapibus elementum congue. Quisque aliquam interdum gravida. Sed consequat vel massa non tempus. Vivamus euismod metus ligula. Suspendisse potenti. Pellentesque varius dapibus sapien nec accumsan. Nulla magna odio, ullamcorper vitae diam sed, viverra euismod dolor.

Sed pretium orci in cursus viverra. Aenean quis felis condimentum, volutpat nisi non, porttitor urna. Curabitur ullamcorper risus eget mollis rhoncus. Cras sed diam at nisl varius venenatis at sit amet ante. Fusce ultricies risus eu lacus lacinia volutpat. Duis eget consequat lorem, vel auctor odio. Interdum et malesuada fames ac ante ipsum primis in faucibus. Nunc molestie dui sed nulla viverra fermentum.



### Seção (Nível 1)

**Texto em Negrito**

orem ipsum dolor sit amet, consectetur adipiscing elit. Ut est nibh, rutrum vel malesuada non, volutpat nec odio. Curabitur posuere suscipit arcu. Fusce augue tortor, porttitor quis enim et, convallis vestibulum justo. Ut fringilla ornare odio, nec fringilla turpis feugiat a. Interdum et malesuada fames ac ante ipsum primis in faucibus. Vestibulum convallis felis nec elit vehicula vestibulum. Duis a iaculis sapien. Morbi dignissim lacinia ante, eu fringilla metus pulvinar a. Praesent dapibus elementum congue. Quisque aliquam interdum gravida. Sed consequat vel massa non tempus. Vivamus euismod metus ligula. Suspendisse potenti. Pellentesque varius dapibus sapien nec accumsan. Nulla magna odio, ullamcorper vitae diam sed, viverra euismod dolor.

[comment]: # (Notas de fim ficam aqui)

[^1] Nota de Fim 1

<article></article>

[comment]: # (Fim do texto)

### Referências Bibliográficas

[comment]: # (Favor não alterar o nome das referências, inserí-las no modelo ABNT)

Irã anuncia mais 49 mortos por coronavírus, maior aumento em 24h. Por: France Press. G1. Bem Estar, 08/03/2020. Disponível em: https://g1.globo.com/mundo/noticia/2020/03/20/eua-dizem-ao-ira-que-coronavirus-nao-poupara-pais-de-sancoes.ghtml. Acesso em 10 abr. 2020.

Número de mortos no Irã por coronavírus supera 4.000. Por: France Press. G1. Mundo, 09/04/2020. Disponível em: https://g1.globo.com/mundo/noticia/2020/04/09/numero-de-mortos-no-ira-por-coronavirus-supera-4-000.ghtml. Acesso em 10 abr. 2020.

Irã pede empréstimo de urgência ao fmi para combater coronavírus. Jornal Estado de Minas. Disponível em: https://www.em.com.br/app/noticia/internacional/2020/04/08/interna_internacional,1136788/ira-pede-emprestimo-de-urgencia-ao-fmi-para-combater-coronavirus.shtml Acesso em 10 abr. 2020.

Número de mortos no Irã por coronavírus supera 4.000. G1, Ciência e saúde, 19/02/2020. Disponível em: https://g1.globo.com/ciencia-e-saude/noticia/2020/02/19/ira-confirma-primeiros-casos-de-coronavirus-no-pais.ghtml. Acesso em 10 abr. 2020.


Berço da covid-19, Wuhan, começa sair da quarentena e reabre ferrovias. O Globo. 2020. Disponível em: https://oglobo.globo.com/mundo/berco-da-covid-19-wuhan-comeca-sair-da-quarentena-reabre-ferrovias-24335432 Acesso em: 12/04/2020.

China intensifica medidas de controle nas fronteiras após alta nos casos de importação de coronavírus.O Globo. Mundo, 2020. Disponível em: https://oglobo.globo.com/mundo/china-intensifica-medidas-de-controle-nas-fronteiras-apos-alta-nos-casos-de-importacao-de-coronavirus-24366504 Acesso em: 12/04/2020.

China declara fim de pico do surto de novo coronavírus no país. G1. Bem Estar, 12/03/2020. Disponível em: https://g1.globo.com/bemestar/coronavirus/noticia/2020/03/12/china-declara-fim-de-pico-do-surto-de-novo-coronavirus-no-pais.ghtml Acesso em: 12/04/2020.

Coronavírus: O médico chinês que tentou alertar colegas sobre surto, mas acabou enquadrado pela polícia e infectado pela doença. BBC, 2020. Disponível em: https://g1.globo.com/ciencia-e-saude/noticia/2020/02/05/coronavirus-o-medico-chines-que-tentou-alertar-colegas-sobre-surto-mas-acabou-enquadrado-pela-policia-e-infectado-pela-doenca.ghtml Acesso em: 12/04/2020.

DONG, Lisheng. Abrindo os olhos para a China. BELLUCCI, Beluce, 2004. Publicado pelo Centro de Estudos Afro-Asiáticos (CEAA), Universidade Candido Mendes (Ucam) e Editora Universitária Candido Mendes (Educam). Disponível online em: http://209.177.156.169/libreria_cm/archivos/pdf_831.pdf#page=24. Acesso em: 12/04/2020.

Maiores Economias do Mundo (PIB em trilhões de US$ - 2013-2020 – ordem decrescente de 2014). Fonte: FMI, World Economic Outlook Database, abril de 2015. Elaboração: IPRI. Disponível em: http://www.funag.gov.br/ipri/images/analise-pesquisa/tabelas/top15pib.pdf Acesso em: 22/04/2020.

Science: medidas drásticas na China reduziram disseminação da covid-19. O Globo. Época Negócios, 2020. Disponível em: https://epocanegocios.globo.com/Mundo/noticia/2020/03/epoca-negocios-science-medidas-drasticas-na-china-reduziram-disseminacao-da-covid-19.html Acesso em: 12/04/2020.

YI-ZHENG, Lian.  Por que o surto de coronavírus começou na China? O Estado de S.Paulo, 2020. Disponível em: https://saude.estadao.com.br/noticias/geral,por-que-o-surto-de-coronavirus-comecou-na-china,70003217688 Acesso em: 12/04/2020.

PIB da China deve ter a primeira queda em 40 anos, diz estudo. Estadão Conteúdo, 2020. Disponível em: https://exame.abril.com.br/economia/pib-da-china-deve-ter-a-primeira-queda-em-40-anos-diz-estudo/ Acesso em: 09/04/2020.


Coronavírus dispara em Tóquio, e governo classifica como “crise nacional”. Por: Reuters. 26 de março de 2020. Disponível em: https://exame.abril.com.br/mundo/coronavirus-dispara-em-toquio-e-governo-classifica-como-crise-nacional/ Acesso em: 09 abr. 2020.

Japão ultrapassa 5 mil casos de coronavírus e estado de emergência falha em manter as pessoas em casa. Por: Reuters. 09 de abril de 2020. Disponível em: https://oglobo.globo.com/mundo/japao-ultrapassa-5-mil-casos-de-coronavirus-estado-de-emergencia-falha-em-manter-as-pessoas-em-casa-1-24361537 Acesso em: 10 abr. 2020.

A calma do Japão diante a crise do coronavírus. Por: Mirela Mazzola. 10 de abril de 2020. Disponível em: https://epoca.globo.com/mundo/a-calma-do-japao-diante-da-crise-do-coronavirus-24364204 Acesso em: 10 abr. 2020.

Coronavírus: como o Japão tem conseguido conter o avanço da doença sem quarentena em massa. Por: Fernanda Paúl. 26 de março de 2020. Disponível em: https://www.bbc.com/portuguese/internacional-52047782 Acesso em 10 abr. 2020.

Japan urges 70% commuter cut in emergency zones and asks nation to avoid nightlife. Por: Jiji. Disponível em: [https://www.japantimes.co.jp/news/2020/04/12/national/japanese-government-urges](https://www.google.com/url?q=https%3A%2F%2Fwww.japantimes.co.jp%2Fnews%2F2020%2F04%2F12%2Fnational%2Fjapanese-government-urges&sa=D&sntz=1&usg=AFQjCNHOizfFp5ecOurIf6ZbtwAbY0kqJQ) 70-commuter-cut-emergency-zones-extends-nightlife-restriction-request-nationwide/#.XpPE6MhKjIU. Acesso: 12 abr 2020.

Governos asiáticos procuram conscientizar as populações a ficarem em casa. Por: Fantástico. Disponível em: https://globoplay.globo.com/v/8476651/programa/ Acesso: 12 abr. 2020.

O envelhecimento do Japão em dados e gráficos. Por: Gabriela Ruic. 21 set. 2016. Disponível em: https://exame.abril.com.br/mundo/o-envelhecimento-do-japao-em-dados-e-graficos/ Acesso: 15 abr. 2020.

Japão: o pico repentino de coronavírus após adiamento olímpico gera polêmica. Por: Redação Veja Esporte. 1 abr. 2020. Disponível em: https://veja.abril.com.br/esporte/japao-pico-repentino-de-coronavirus-apos-adiamento-olimpico-gera-polemica/ Acesso em: 15 abr. 2020.

Japan: International Health Care System. Por: Ryozo Matsuda. Disponível em: https://international.commonwealthfund.org/countries/japan/ Acesso em: 15 abr. 2020.


1. Texto desenvolvido por membros do Centro Acadêmico de Relações Internacionais “João Cabral de Melo Neto”, gestão 2020 “Vozes do Sul”.

2. Estudante do 3° ano de Relações Internacionais da Universidade Estadual Paulista (UNESP), campus de Franca.

3. Estudante do 4º ano de Relações Internacionais da Universidade Estadual Paulista (UNESP), campus de Franca.

4. Economia da Coreia do Sul. Santander Trade Markets. Disponível em: https://santandertrade.com/pt/portal/analise-os-mercados/coreia-do-sul/economia.

5. Country Profile South-Korea. Global Trade. Disponível em: https://www.globaltrade.net/international-trade-import-exports/f/business/text/South-Korea/Business-Environment-Country-Profile-South-Korea.html. Acesso em 12/04/2020.

6. Iamarino, Atila. Especial coronavírus #02. PodCast Xadrez verbal. Disponível em:  [https://xadrezverbal.com/](https://www.google.com/url?q=https%3A%2F%2Fxadrezverbal.com%2F&sa=D&sntz=1&usg=AFQjCNEVrtbukcnFR_TvmjAXB1oJprfN6Q).

7. Iamarino, Atila. op. cit., loc. cit.

8.  [Aless](https://www.google.com/url?q=https%3A%2F%2Fbrasil.elpais.com%2Fautor%2Fgil-alessi%2F&sa=D&sntz=1&usg=AFQjCNFytaptkEEfoUFkzuiFhi20dS0t8g)i,  [Gil](https://www.google.com/url?q=https%3A%2F%2Fbrasil.elpais.com%2Fautor%2Fgil-alessi%2F&sa=D&sntz=1&usg=AFQjCNFytaptkEEfoUFkzuiFhi20dS0t8g). As lições contra o coronavírus que Coreia do Sul e China podem dar ao mundo, incluindo o Brasil. El País. Disponível em: https://brasil.elpais.com/internacional/2020-03-30/as-licoes-contra-o-coronavirus-que-coreia-do-sul-e-china-podem-dar-ao-mundo-incluindo-o-brasil.html.

9. Canário, Tiago. Normalidade na Coreia do Sul durou até mulher ignorar sintomas e ir a culto religioso. Folha de S. Paulo. Disponível em: https://www1.folha.uol.com.br/mundo/2020/03/normalidade-na-coreia-do-sul-durou-ate-mulher-ignorar-sintomas-e-ir-a-culto-religioso.shtml.

10.  [Linde](https://www.google.com/url?q=https%3A%2F%2Fbrasil.elpais.com%2Fautor%2Fpablo-linde%2F&sa=D&sntz=1&usg=AFQjCNE-RO5t2X-gh8xruMrH7twAulV2qA),  [Pablo](https://www.google.com/url?q=https%3A%2F%2Fbrasil.elpais.com%2Fautor%2Fpablo-linde%2F&sa=D&sntz=1&usg=AFQjCNE-RO5t2X-gh8xruMrH7twAulV2qA). Espanha e Coreia do Sul, exemplos opostos de controle epidemiológico do coronavírus. El País. Disponível em: https://brasil.elpais.com/sociedade/2020-03-16/coreia-e-espanha-exemplos-opostos-de-controle-epidemiologico-do-coronavirus.html.

11. AFP. Como a Coreia do Sul virou modelo no combate ao coronavírus. Disponível em: https://noticias.uol.com.br/ultimas-noticias/afp/2020/03/12/o-exemplo-da-coreia-do-sul-no-combate-ao-coronavirus.htm?cmpid=copiaecola.

12. Iamarino, Atila. op. cit., loc. cit.

13.  [Perassolo](https://www.google.com/url?q=https%3A%2F%2Fwww1.folha.uol.com.br%2Fautores%2Fjoao-perassolo.shtml&sa=D&sntz=1&usg=AFQjCNHtKQu0H5FNyCzxTVwOGe1dIJrE2w),  [João](https://www.google.com/url?q=https%3A%2F%2Fwww1.folha.uol.com.br%2Fautores%2Fjoao-perassolo.shtml&sa=D&sntz=1&usg=AFQjCNHtKQu0H5FNyCzxTVwOGe1dIJrE2w). Confúcio, experiência com epidemias e tecnologia ajudam no controle do coronavírus na Ásia. Jornal Folha de S. Paulo. Disponível em: https://www1.folha.uol.com.br/mundo/2020/04/confucio-experiencia-com-epidemias-e-tecnologia-ajudam-no-controle-do-coronavirus-na-asia.shtml?origin=folha.


AFP. Como a Coreia do Sul virou modelo no combate ao coronavírus. Disponível em: https://noticias.uol.com.br/ultimas-noticias/afp/2020/03/12/o-exemplo-da-coreia-do-sul-no-combate-ao-coronavirus.htm?cmpid=copiaecola. Acesso em 5 de abril de 2020.

[Aless](https://www.google.com/url?q=https%3A%2F%2Fbrasil.elpais.com%2Fautor%2Fgil-alessi%2F&sa=D&sntz=1&usg=AFQjCNFytaptkEEfoUFkzuiFhi20dS0t8g)i, [Gil](https://www.google.com/url?q=https%3A%2F%2Fbrasil.elpais.com%2Fautor%2Fgil-alessi%2F&sa=D&sntz=1&usg=AFQjCNFytaptkEEfoUFkzuiFhi20dS0t8g). As lições contra o coronavírus que Coreia do Sul e China podem dar ao mundo, incluindo o Brasil. El País. Disponível em: https://brasil.elpais.com/internacional/2020-03-30/as-licoes-contra-o-coronavirus-que-coreia-do-sul-e-china-podem-dar-ao-mundo-incluindo-o-brasil.html. Acesso em 5 de abril de 2020.

Canário, Tiago. Normalidade na Coreia do Sul durou até mulher ignorar sintomas e ir a culto religioso. Folha de S. Paulo. Disponível em: https://www1.folha.uol.com.br/mundo/2020/03/normalidade-na-coreia-do-sul-durou-ate-mulher-ignorar-sintomas-e-ir-a-culto-religioso.shtml. Acesso em 5 de abril de 2020.

Country Profile South-Korea. Global Trade. Disponível em: https://www.globaltrade.net/international-trade-import-exports/f/business/text/South-Korea/Business-Environment-Country-Profile-South-Korea.html. Acesso em 12/04/2020.Acesso em 5 de abril de 2020.

Iamarino, Atila. Especial coronavírus #02. PodCast Xadrez verbal. Disponível em: https://xadrezverbal.com/. Acesso em 5 de abril de 2020.

[Linde](https://www.google.com/url?q=https%3A%2F%2Fbrasil.elpais.com%2Fautor%2Fpablo-linde%2F&sa=D&sntz=1&usg=AFQjCNE-RO5t2X-gh8xruMrH7twAulV2qA), [Pablo](https://www.google.com/url?q=https%3A%2F%2Fbrasil.elpais.com%2Fautor%2Fpablo-linde%2F&sa=D&sntz=1&usg=AFQjCNE-RO5t2X-gh8xruMrH7twAulV2qA). Espanha e Coreia do Sul, exemplos opostos de controle epidemiológico do coronavírus. El País. Disponível em: https://brasil.elpais.com/sociedade/2020-03-16/coreia-e-espanha-exemplos-opostos-de-controle-epidemiologico-do-coronavirus.html. Acesso em 5 de abril de 2020.

[Perassolo](https://www.google.com/url?q=https%3A%2F%2Fwww1.folha.uol.com.br%2Fautores%2Fjoao-perassolo.shtml&sa=D&sntz=1&usg=AFQjCNHtKQu0H5FNyCzxTVwOGe1dIJrE2w), [João](https://www.google.com/url?q=https%3A%2F%2Fwww1.folha.uol.com.br%2Fautores%2Fjoao-perassolo.shtml&sa=D&sntz=1&usg=AFQjCNHtKQu0H5FNyCzxTVwOGe1dIJrE2w). Confúcio, experiência com epidemias e tecnologia ajudam no controle do coronavírus na Ásia. Jornal Folha de S. Paulo. Disponível em: https://www1.folha.uol.com.br/mundo/2020/04/confucio-experiencia-com-epidemias-e-tecnologia-ajudam-no-controle-do-coronavirus-na-asia.shtml?origin=folha. Acesso em 5 de abril de 2020.

Santander Trade Markets. Economia da Coreia do Sul. Disponível em: https://santandertrade.com/pt/portal/analise-os-mercados/coreia-do-sul/economia. Acesso em 5 de abril de 2020.

14. Estudante do 2º ano de Relações Internacionais da Universidade Estadual Paulista (UNESP), campus de Franca.

15. Estudante do 2º ano de Relações Internacionais da Universidade Estadual Paulista (UNESP), campus de Franca.

Endnotes

1. Estudante do 3° ano de Relações Internacionais da Universidade Estadual Paulista (UNESP), campus de Franca.

2. Estudante do 4º ano de Relações Internacionais da Universidade Estadual Paulista (UNESP), campus de Franca.

3. Economia da Coreia do Sul. Santander Trade Markets. Disponível em: https://santandertrade.com/pt/portal/analise-os-mercados/coreia-do-sul/economia.

4. Country Profile South-Korea. Global Trade. Disponível em: https://www.globaltrade.net/international-trade-import-exports/f/business/text/South-Korea/Business-Environment-Country-Profile-South-Korea.html. Acesso em 12/04/2020.

5. Iamarino, Atila.Especial coronavírus #02. PodCast Xadrez verbal. Disponível em: https://xadrezverbal.com/.

6. Iamarino, Atila. op. cit., loc. cit.

7.  [Aless](https://www.google.com/url?q=https%3A%2F%2Fbrasil.elpais.com%2Fautor%2Fgil-alessi%2F&sa=D&sntz=1&usg=AFQjCNFytaptkEEfoUFkzuiFhi20dS0t8g)i,  [Gil](https://www.google.com/url?q=https%3A%2F%2Fbrasil.elpais.com%2Fautor%2Fgil-alessi%2F&sa=D&sntz=1&usg=AFQjCNFytaptkEEfoUFkzuiFhi20dS0t8g). As lições contra o coronavírus que Coreia do Sul e China podem dar ao mundo, incluindo o Brasil. El País. Disponível em: https://brasil.elpais.com/internacional/2020-03-30/as-licoes-contra-o-coronavirus-que-coreia-do-sul-e-china-podem-dar-ao-mundo-incluindo-o-brasil.html.

8. Canário, Tiago. Normalidade na Coreia do Sul durou até mulher ignorar sintomas e ir a culto religioso. Folha de S. Paulo. Disponível em: https://www1.folha.uol.com.br/mundo/2020/03/normalidade-na-coreia-do-sul-durou-ate-mulher-ignorar-sintomas-e-ir-a-culto-religioso.shtml.

9.  [Linde](https://www.google.com/url?q=https%3A%2F%2Fbrasil.elpais.com%2Fautor%2Fpablo-linde%2F&sa=D&sntz=1&usg=AFQjCNE-RO5t2X-gh8xruMrH7twAulV2qA),  [Pablo](https://www.google.com/url?q=https%3A%2F%2Fbrasil.elpais.com%2Fautor%2Fpablo-linde%2F&sa=D&sntz=1&usg=AFQjCNE-RO5t2X-gh8xruMrH7twAulV2qA). Espanha e Coreia do Sul, exemplos opostos de controle epidemiológico do coronavírus. El País. Disponível em: https://brasil.elpais.com/sociedade/2020-03-16/coreia-e-espanha-exemplos-opostos-de-controle-epidemiologico-do-coronavirus.html.

10. AFP. Como a Coreia do Sul virou modelo no combate ao coronavírus. Disponível em: https://noticias.uol.com.br/ultimas-noticias/afp/2020/03/12/o-exemplo-da-coreia-do-sul-no-combate-ao-coronavirus.htm?cmpid=copiaecola.

11. Iamarino, Atila. op. cit., loc. cit.

12.  [Perassolo](https://www.google.com/url?q=https%3A%2F%2Fwww1.folha.uol.com.br%2Fautores%2Fjoao-perassolo.shtml&sa=D&sntz=1&usg=AFQjCNHtKQu0H5FNyCzxTVwOGe1dIJrE2w),  [João](https://www.google.com/url?q=https%3A%2F%2Fwww1.folha.uol.com.br%2Fautores%2Fjoao-perassolo.shtml&sa=D&sntz=1&usg=AFQjCNHtKQu0H5FNyCzxTVwOGe1dIJrE2w). Confúcio, experiência com epidemias e tecnologia ajudam no controle do coronavírus na Ásia. Jornal Folha de S. Paulo. Disponível em: https://www1.folha.uol.com.br/mundo/2020/04/confucio-experiencia-com-epidemias-e-tecnologia-ajudam-no-controle-do-coronavirus-na-asia.shtml?origin=folha.

13. Estudante do 2º ano de Relações Internacionais da Universidade Estadual Paulista (UNESP), campus de Franca.

14. Estudante do 2º ano de Relações Internacionais da Universidade Estadual Paulista (UNESP), campus de Franca.
