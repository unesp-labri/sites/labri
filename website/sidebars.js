const { Link } = require("react-router-dom");

module.exports = {
  info: [
    //informações gerais sobre funcionamento e estágio labri
    {
      type: "category",
      label: "Informações",
      items: [
        "geral/info/atendimento",
        "geral/info/estagio",
        "geral/info/processo-seletivo",
        "geral/info/estagio-voluntario",
        "geral/info/emprestimos",
        {
          type: "link",
          href: "/docs/equipe",
          label: "Equipe",
        },
      ],
    },
    {
      type: "category",
      label: "Gestão",
      items: [
        "geral/gestao/intro",
        "geral/gestao/gestao-de-atividades",
        "geral/gestao/google-drive",
        {
          type: "category",
          label: "Comunicação",
          items: [
        "geral/gestao/comunicacao/redes-sociais",
        "geral/gestao/comunicacao/página-colaborador",
        "geral/gestao/comunicacao/templates-instagram",
        "geral/gestao/comunicacao/tutorial-gif",
        "geral/gestao/comunicacao/templates-figma",
        "geral/gestao/comunicacao/tutorial-openshot",
        "geral/gestao/comunicacao/tutorial-site",
        "geral/gestao/comunicacao/templates-eventos",
        "geral/gestao/comunicacao/edição-trilha"
          ],
        },
        "geral/gestao/contatos-unesp",
        "geral/gestao/acesso-labri",
        "geral/gestao/inventário",
        "geral/gestao/manutenção-infra",
        "geral/gestao/conferências",
        "geral/gestao/audiovisual"
      ],
    },
  ],

  cadernos: [
    // publicação LabRI/UNESP
    {
      type: "category",
      label: "Cadernos LabRI/UNESP",
      items: [
        "cadernos/intro",
        "cadernos/expediente",
        {
          type: "link",
          href: "/cadernos",
          label: "Edição Atual/Anteriores",
        },
        "cadernos/series-labri-unesp",
        "cadernos/como-publicar",
      ],
    },
  ],

  projetosEnsinoTecDigitais: [
    // material de apoio
    {
      type: "link",
      href: "/projetos/ensino/",
      label: "Projetos de Ensino",
    },
    {
      type: "category",
      label: "Tecnologias Digitais",
      items: [
        "projetos/ensino/tec-digitais/intro",
        "projetos/ensino/tec-digitais/id-visual",
        "projetos/ensino/tec-digitais/conhecimentos/intro",
        "projetos/ensino/tec-digitais/anotacoes/intro",
        "projetos/ensino/tec-digitais/edicao-texto/intro",
        "projetos/ensino/tec-digitais/buscadores/intro",
        "projetos/ensino/tec-digitais/audio/intro",
      ],
    },
  ],

  materialBibliografico: [
    // material bibliográfico
    {
      type: "link",
      href: "/projetos/ensino/",
      label: "Projetos de Ensino",
    },
    {
      type: "category",
      label: "Material Bibliográfico",
      items: [
        "projetos/ensino/material-bibliografico/intro",
        "projetos/ensino/material-bibliografico/classicos-ipri",
        "projetos/ensino/material-bibliografico/mapas",
        "projetos/ensino/material-bibliografico/dicas-base-de-dados",
      ],
    },
  ],
  projetosDadosAcervoRedalint: [
    {
      type: "link",
      href: "/projetos/dados/",
      label: "Projetos de Dados",
    },
    {
      type: "category",
      label: "Acervo REDALINT",
      items: [
        "projetos/dados/acervo-redalint/info",
        "projetos/dados/acervo-redalint/equipe",
        "projetos/dados/acervo-redalint/atividades",
        "projetos/dados/acervo-redalint/doc",
        "projetos/dados/acervo-redalint/citar",
      ],
    },
  ],
  projetosDadosDiariosbr: [
    {
      type: "link",
      href: "/projetos/dados/",
      label: "Projetos de Dados",
    },
    {
      type: "category",
      label: "DiáriosBR",
      items: [
        "projetos/dados/diariosbr/info",
        "projetos/dados/diariosbr/equipe",
        "projetos/dados/diariosbr/atividades",

        {
          type: "category",
          label: "Documentação",
          items: [
            "projetos/dados/diariosbr/infos/intro",
            "projetos/dados/diariosbr/infos/dados-metadados",
            "projetos/dados/diariosbr/infos/location",
            "projetos/dados/diariosbr/infos/banco-termos",
            "projetos/dados/diariosbr/infos/fluxograma",
          ],
        },
        "projetos/dados/diariosbr/citar",
      ],
    },
  ],

  projetosDadosInterneteRelacoesInternacionais: [
    {
      type: "link",
      href: "/projetos/dados/",
      label: "Projetos de Dados",
    },
    {
      type: "category",
      label: "Internet e Relações Internacionais",
      items: [
        "projetos/dados/internet-relacoes-internacionais/info",
        "projetos/dados/internet-relacoes-internacionais/equipe",
        "projetos/dados/internet-relacoes-internacionais/atividades",
        "projetos/dados/internet-relacoes-internacionais/doc",
        "projetos/dados/internet-relacoes-internacionais/citar",
      ],
    },
  ],

  projetosDadosFullText: [
    {
      type: "link",
      href: "/projetos/dados/",
      label: "Projetos de Dados",
    },
    {
      type: "category",
      label: "Full Text",
      items: [
        "projetos/dados/full-text/info",
        "projetos/dados/full-text/equipe",
        "projetos/dados/full-text/atividades",
        {
          type: "category",
          label: "Documentação",
          items: [
            "projetos/dados/full-text/infos/intro",
            "projetos/dados/full-text/infos/digitalizacao",
            "projetos/dados/full-text/infos/ocr",
          ],
        },
        "projetos/dados/full-text/identidade-visual",
        "projetos/dados/full-text/citar",
      ],
    },
  ],

  projetosDadosHemerotecaPEB: [
    {
      type: "link",
      href: "/projetos/dados/",
      label: "Projetos de Dados",
    },
    {
      type: "category",
      label: "Hemeroteca PEB",
      items: [
        "projetos/dados/hemeroteca-peb/info",
        "projetos/dados/hemeroteca-peb/equipe",
        "projetos/dados/hemeroteca-peb/atividades",
        {
          type: "category",
          label: "Documentação",
          items: [
            "projetos/dados/hemeroteca-peb/infos/intro",
            "projetos/dados/hemeroteca-peb/infos/arquivo-json",
            "projetos/dados/hemeroteca-peb/infos/temas",
          ],
        },
        "projetos/dados/hemeroteca-peb/citar",
      ],
    },
  ],

  projetosDadosIRJournalsbr: [
    {
      type: "link",
      href: "/projetos/dados/",
      label: "Projetos de Dados",
    },
    {
      type: "category",
      label: "IRJournalsBR",
      items: [
        "projetos/dados/irjournalsbr/info",
        "projetos/dados/irjournalsbr/equipe",
        "projetos/dados/irjournalsbr/atividades",
        {
          type: "category",
          label: "Documentação",
          items: [
            "projetos/dados/irjournalsbr/infos/intro",
            "projetos/dados/irjournalsbr/infos/metadados",
            "projetos/dados/irjournalsbr/infos/codigo",
            "projetos/dados/irjournalsbr/infos/location",
            "projetos/dados/irjournalsbr/infos/pastas",
            "projetos/dados/irjournalsbr/infos/arquivos-controle",
          ],
        },
        "projetos/dados/irjournalsbr/identidade-visual",
        "projetos/dados/irjournalsbr/citar",
      ],
    },
  ],

  projetosDadosMercoDocs: [
    {
      type: "link",
      href: "/projetos/dados/",
      label: "Projetos de Dados",
    },
    {
      type: "category",
      label: "MercoDocs",
      items: [
        "projetos/dados/mercodocs/info",
        "projetos/dados/mercodocs/equipe",
        "projetos/dados/mercodocs/atividades",
        "projetos/dados/mercodocs/doc",
        "projetos/dados/mercodocs/citar",
      ],
    },
  ],

  projetosDadosNewsCloud: [
    {
      type: "link",
      href: "/projetos/dados/",
      label: "Projetos de Dados",
    },
    {
      type: "category",
      label: "NewsCloud",
      items: [
        "projetos/dados/newscloud/geral/info",
        "projetos/dados/newscloud/geral/equipe",
        "projetos/dados/newscloud/geral/atividades",
        "projetos/dados/newscloud/geral/info-coleta",
        {
          type: "category",
          label: "Documentação",
          items: [
            "projetos/dados/newscloud/infos/intro",
            "projetos/dados/newscloud/infos/elementos",
            "projetos/dados/newscloud/infos/elementos-peru",
            "projetos/dados/newscloud/infos/elementos-equador",
            "projetos/dados/newscloud/infos/elementos-colombia",
            "projetos/dados/newscloud/infos/elementos-brasil",
            "projetos/dados/newscloud/infos/fluxograma",
          ],
        },
        "projetos/dados/newscloud/geral/citar",
      ],
    },
  ],

  projetosDadosTweepina: [
    {
      type: "link",
      href: "/projetos/dados/",
      label: "Projetos de Dados",
    },
    {
      type: "category",
      label: "TweePiNa",
      items: [
        "projetos/dados/tweepina/info",
        "projetos/dados/tweepina/equipe",
        "projetos/dados/tweepina/atividades",
        {
          type: "category",
          label: "Documentação",
          items: [
            "projetos/dados/tweepina/infos/intro",
            "projetos/dados/tweepina/infos/script",
            "projetos/dados/tweepina/infos/variaveis",
            "projetos/dados/tweepina/infos/fluxograma",
            "projetos/dados/tweepina/infos/identificacao-status",
            "projetos/dados/tweepina/infos/projetos",
            "projetos/dados/tweepina/infos/bibliotecas",
          ],
        },
        "projetos/dados/tweepina/identidade-visual",
        "projetos/dados/tweepina/citar",
      ],
    },
  ],

  projetosDadosGovLatinAmerica: [
    {
      type: "link",
      href: "/projetos/dados/",
      label: "Projetos de Dados",
    },
    {
      type: "category",
      label: "GovLatinAmerica",
      items: [
        "projetos/dados/gov-latin-america/info",
        "projetos/dados/gov-latin-america/equipe",
        "projetos/dados/gov-latin-america/atividades",
        "projetos/dados/gov-latin-america/dados_coletados",
        "projetos/dados/gov-latin-america/doc",
        "projetos/dados/gov-latin-america/identidade-visual",
        "projetos/dados/gov-latin-america/citar",
      ],
    },
  ],

  projetosExtensao: [
    {
      type: "category",
      label: "Projetos de Extensão",
      items: [
        {
          type: "link",
          href: "/projetos/extensao/",
          label: "Iniciativas",
        },
        ,
        "projetos/extensao/id-visual",
      ],
    },
  ],
  projetosExtensaoCidadesSustentaveis: [
    {
      type: "category",
      label: "Cidades Sustentáveis",
      items: [
        {
          type: "link",
          href: "/projetos/extensao/",
          label: "Projetos de Extensão",
        },
        "projetos/extensao/cidades-sustentaveis/equipe",
      ],
    },
  ],

  projetosExtensaoConhecerparaAcolher: [
    {
      type: "category",
      label: "Conhecer para Acolher",
      items: [
        {
          type: "link",
          href: "/projetos/extensao/",
          label: "Projetos de Extensão",
        },
        "projetos/extensao/conhecer-para-acolher/equipe",
      ],
    },
  ],

  projetosExtensaoPodRI: [
    {
      type: "category",
      label: "Pod-RI",
      items: [
        {
          type: "link",
          href: "/projetos/extensao/",
          label: "Projetos de Extensão",
        },
        "projetos/extensao/pod-ri/intro",
        "projetos/extensao/pod-ri/equipe",
        "projetos/extensao/pod-ri/episodios",
        "projetos/extensao/pod-ri/id-visual",
      ],
    },
  ],

  projetosExtensaoPandemiaRI: [
    {
      type: "category",
      label: "Pandemia e as Relações Internacionais",
      items: [
        {
          type: "link",
          href: "/projetos/extensao/",
          label: "Projetos de Extensão",
        },
        "projetos/extensao/covid19/intro",
        "projetos/extensao/covid19/grupos-trabalho",
        "projetos/extensao/covid19/postagens",
        "projetos/extensao/covid19/id-visual",
      ],
    },
  ],

  projetosEnsino: [
    {
      type: "category",
      label: "Projetos de Ensino",
      items: [
        {
          type: "link",
          href: "/projetos/ensino/",
          label: "Projetos de Ensino",
        },
        "projetos/ensino/a1-geral/intro",
        "projetos/ensino/a1-geral/lista",
        "projetos/ensino/id-visual",
      ],
    },
  ],

  equipe_nova: [
    {
      type: "category",
      label: "Equipe",
      items: [
        "equipe/equipe",
        "equipe/equipe-inativa",

        {
          type: "link",
          href: "/docs/atendimento",
          label: "Atendimento",
        },
      ],
    },
  ],

  projetosEnsinoTrilhaDadosPython: [
    {
      type: "link",
      href: "/projetos/ensino/",
      label: "Projetos de Ensino",
    },
    {
      type: "category",
      label: "Trilha de Dados - Python",
      items: [
        "projetos/ensino/trilha-dados/intro",
        {
          type: "category",
          label: "Paradas",
          items: [
            {
              type: "category",
              label: "Ambiente de Trabalho",
              items: [
                "projetos/ensino/trilha-dados/ambiente/intro",
                {
                  type: "category",
                  label: "Versionamento",
                  items: [
                    "projetos/ensino/trilha-dados/ambiente/versionamento/nocoes-gerais",
                    "projetos/ensino/trilha-dados/ambiente/versionamento/dinamica-versionamento",
                    {
                      type: "category",
                      label: "Repositório",
                      items: [
                        "projetos/ensino/trilha-dados/ambiente/versionamento/tipos-licencas",
                        "projetos/ensino/trilha-dados/ambiente/versionamento/criar-repositorio",
                        "projetos/ensino/trilha-dados/ambiente/versionamento/acessar-repositorio",
                      ],
                    },
                    {
                      type: "category",
                      label: "Utilizando o GIT",
                      items: [
                        "projetos/ensino/trilha-dados/ambiente/versionamento/git-basico",
                      {
                        type: "category",
                        label: "GIT - Indo além",
                        items: [
                          "projetos/ensino/trilha-dados/ambiente/versionamento/chave-ssh",
                          "projetos/ensino/trilha-dados/ambiente/versionamento/integrar-branches",

                          
                        ],
                      },
                        
                      ],
                    },
                  ],
                },
                {
                  type: "category",
                  label: "Markdown",
                  items: [
                    "projetos/ensino/trilha-dados/ambiente/markdown/nocoes-gerais",
                    "projetos/ensino/trilha-dados/ambiente/markdown/sintaxe",
                    "projetos/ensino/trilha-dados/ambiente/markdown/github-profile"
                  ],
                },
                "projetos/ensino/trilha-dados/ambiente/comandos-linux",
                "projetos/ensino/trilha-dados/ambiente/editor-codigo",
                "projetos/ensino/trilha-dados/ambiente/ambiente-virtual",
                "projetos/ensino/trilha-dados/ambiente/certificação"
              ],
            },
            {
              type: "category",
              label: "Fundamentos Python",
              items: [
                "projetos/ensino/trilha-dados/linguagens/python/intro",
            {
              type: "category",
              label: "Básico 1",
              items: [
                "projetos/ensino/trilha-dados/linguagens/python/p00-02-zen-python",
                {
                  type: "category",
                  label: "Tipos Básicos",
                  items: [
                    "projetos/ensino/trilha-dados/linguagens/python/p01-01-variaveis-e-numeros",
                    "projetos/ensino/trilha-dados/linguagens/python/p01-02-strings",
                    "projetos/ensino/trilha-dados/linguagens/python/p01-02b-operadores",
                    "projetos/ensino/trilha-dados/linguagens/python/p01-03-conversao-tipos",
                  ],
                },
                "projetos/ensino/trilha-dados/linguagens/python/p02-01-funcoes",
                "projetos/ensino/trilha-dados/linguagens/python/certificacao-b1",
              ],
              },
              {
                type: "category",
                label: "Básico 2",
                items: [
                  {
                    type: "category",
                    label: "Tipos Avançados",
                    items: [
                      "projetos/ensino/trilha-dados/linguagens/python/p03-01-listas",
                      "projetos/ensino/trilha-dados/linguagens/python/p03-02-tuplas",
                      "projetos/ensino/trilha-dados/linguagens/python/p03-03-conjuntos",
                      "projetos/ensino/trilha-dados/linguagens/python/p03-04-dicionarios",
                    ],
                  },
                  "projetos/ensino/trilha-dados/linguagens/python/p03-05-mutabilidade",
                  {
                    type: "category",
                    label: "Controle de Fluxo",
                    items: [
                      "projetos/ensino/trilha-dados/linguagens/python/p04-01-booleanos",
                      "projetos/ensino/trilha-dados/linguagens/python/p04-02-declarações-de-controle",
                    ],
                  },
                  "projetos/ensino/trilha-dados/linguagens/python/certificacao-b2"
                ],
              },
                {
                  type: "category",
                  label: "Básico 3",
                  items: [
                    "projetos/ensino/trilha-dados/linguagens/python/p05-01-arquivos",
                    "projetos/ensino/trilha-dados/linguagens/python/p05-02-diretorios",
                    "projetos/ensino/trilha-dados/linguagens/python/p05-03-bibliotecas",
                    "projetos/ensino/trilha-dados/linguagens/python/certificacao-b3",
                  ],
                },
                
            {
              type: "category",
              label: "Intermediário",
              items: [
                "projetos/ensino/trilha-dados/linguagens/python/p06-01-compreensao",
                "projetos/ensino/trilha-dados/linguagens/python/p06-02-itertools",
                "projetos/ensino/trilha-dados/linguagens/python/p06-03-encode",
                "projetos/ensino/trilha-dados/linguagens/python/p07-01-objetos",
                "projetos/ensino/trilha-dados/linguagens/python/certificacao-int"
              ],
            },
            ],
            },
            {
              type: "category",
              label: "Análise de Dados",
              items: [
                "projetos/ensino/trilha-dados/analise-dados/intro",
                "projetos/ensino/trilha-dados/analise-dados/conceitos-basicos",
                "projetos/ensino/trilha-dados/analise-dados/gerar-graficos",
                "projetos/ensino/trilha-dados/analise-dados/combinando-dataframes",
                "projetos/ensino/trilha-dados/analise-dados/tratamento-de-dados",
        
              ],
            },
            {
              type: "category",
              label: "Coleta de dados",
              items: [
                "projetos/ensino/trilha-dados/coleta-dados/intro",
              ],
            },
            {
              type: "category",
              label: "Banco de dados",
              items:[
                "projetos/ensino/trilha-dados/banco-dados/intro",
              ] 
            },
            {
              type: "category",
              label: "Métodos Quantitativos",
              items:[
                "projetos/ensino/trilha-dados/metodos-quanti/intro",
              ] 
            },
          ],
        },
        "projetos/ensino/trilha-dados/indicacoes",
        "projetos/ensino/trilha-dados/id-visual",
      ],
    },
    {
      type: "category",
      label: "Trilha de Dados - R",
      items: ["projetos/ensino/trilha-dados/linguagens/r-lang/intro"],
    },
    {
      type: "category",
      label: "Trilha JS",
      items: [
        "projetos/ensino/trilha-dados/linguagens/js/intro",
        "projetos/ensino/trilha-dados/linguagens/js/acessibilidade-web",
        "projetos/ensino/trilha-dados/linguagens/js/html",
        "projetos/ensino/trilha-dados/linguagens/js/css",
        "projetos/ensino/trilha-dados/linguagens/js/sass",
        "projetos/ensino/trilha-dados/linguagens/js/javascript",
        "projetos/ensino/trilha-dados/linguagens/js/nodejs",
        "projetos/ensino/trilha-dados/linguagens/js/jquery",
        "projetos/ensino/trilha-dados/linguagens/js/bootstrap",
        "projetos/ensino/trilha-dados/linguagens/js/react",
        "projetos/ensino/trilha-dados/linguagens/js/d3",
        "projetos/ensino/trilha-dados/linguagens/js/exercicios-js",
      ],
    },
  ],
 
  projetosEnsinoAtividades: [
    {
      type: "link",
      href: "/projetos/ensino/",
      label: "Projetos de Ensino",
    },
    {
      type: "category",
      label: "Treinamento",
      items: [
        "projetos/ensino/atividades/intro",
        {
          type: "category",
          label: "GovLatinAmerica01",
          items: [
            "projetos/ensino/atividades/trilha-dados-python/govlatinamerica01/intro",
            "projetos/ensino/atividades/trilha-dados-python/govlatinamerica01/tarefas",
            "projetos/ensino/atividades/trilha-dados-python/govlatinamerica01/atividades",
          ],
        },
      ],
    },
  ],

  projetosEnsinoRecoll: [
    {
      type: "link",
      href: "/projetos/ensino/",
      label: "Projetos de Ensino",
    },
    {
      type: "category",
      label: "Recoll",
      items: [
        "projetos/ensino/recoll/info",
        "projetos/ensino/recoll/conceitos",
        "projetos/ensino/recoll/configurando",
        "projetos/ensino/recoll/utilizando",
        "projetos/ensino/recoll/pesquisas",
        "projetos/ensino/recoll/atividades",
        "projetos/ensino/recoll/colaboradores",
      ],
    },
  ],
  projetosEnsinoGovDaInternet: [
    {
      type: "link",
      href: "/projetos/ensino/",
      label: "Projetos de Ensino",
    },
    {
      type: "category",
      label: "Governança da Internet",
      items: ["projetos/ensino/governanca/intro"],
    },
  ],

  projetosEnsinoAcessoRemoto: [
    {
      type: "link",
      href: "/projetos/ensino/",
      label: "Projetos de Ensino",
    },
    {
      type: "category",
      label: "Acesso Remoto",
      items: [
        "projetos/ensino/acesso-remoto/intro",
        "projetos/ensino/acesso-remoto/explicar",
        "projetos/ensino/acesso-remoto/solicitar-acesso",
        "projetos/ensino/acesso-remoto/instrucao-inicial",
        "projetos/ensino/acesso-remoto/tutoriais",
    ],
    },
  ],

  projetosEnsinoFilezilla: [
    {
      type: "link",
      href: "/projetos/ensino/",
      label: "Projetos de Ensino",
    },
    {
      type: "category",
      label: "FileZilla",
      items: [
        "projetos/ensino/filezilla/intro",
        "projetos/ensino/filezilla/configuracao",
    ],
    },
  ],  

  projetosEnsinoProcessamentoImagens: [
    {
      type: "link",
      href: "/projetos/ensino/",
      label: "Projetos de Ensino",
    },
    {
      type: "category",
      label: "Pós-processamento de Imagens",
      items: [
        "projetos/ensino/processamento-imagens/intro",
        "projetos/ensino/processamento-imagens/formatos",
        "projetos/ensino/processamento-imagens/ocr",
    ],
    },
    {
      type: "category",
      label: "Scanners",
      items: [
        "projetos/ensino/processamento-imagens/scanners/avision",
        "projetos/ensino/processamento-imagens/scanners/opticbook",
        "projetos/ensino/processamento-imagens/scanners/smartoffice",
        "projetos/ensino/processamento-imagens/scanners/laserjet",
      ],
    },
  ],

  projetosEnsinoAudiovisual: [
    {
      type: "link",
      href: "/projetos/ensino/",
      label: "Projetos de Ensino",
    },
    {
      type: "category",
      label: "OpenShot",
      items: [
        "projetos/ensino/audiovisual/openshot",
        "projetos/ensino/audiovisual/podri"
    ],
    },
  ],

  projetosSistemas: [
    {
      type: "category",
      label: "Projetos de sistemas",
      items: [
        {
          type: "link",
          href: "/projetos/sistemas/",
          label: "Iniciativas",
        },
        "projetos/sistemas/id-visual",
      ],
    },
  ],

  projetosSistemasInfraComputacional: [
    {
      type: "link",
      href: "/projetos/sistemas/",
      label: "Projetos de Sistemas",
    },
    {
      type: "category",
      label: "Infraestrutura Computacional",
      items: [
        "projetos/sistemas/intro-infracomputacional",
        "projetos/sistemas/atividades",
        {
          type: "category",
          label: "Linux Admin",
          items: [
            "projetos/sistemas/linux/alterar-nome-pc",
            "projetos/sistemas/linux/usuarios",
            "projetos/sistemas/linux/formatar-hd",
            "projetos/sistemas/linux/tela-preta",
            "projetos/sistemas/linux/atualizar",
            "projetos/sistemas/linux/tutorial-pendrive",
            "projetos/sistemas/linux/acesso-remoto",
            "projetos/sistemas/linux/resolucao-erros",
            "projetos/sistemas/linux/info-hardware",
            "projetos/sistemas/linux/ventoy",
            "projetos/sistemas/linux/verificar-linux",
            "projetos/sistemas/linux/initrafms-boot",
            "projetos/sistemas/linux/ubuntu-initrafms",
            "projetos/sistemas/linux/rein-network",
            "projetos/sistemas/linux/senha-root",
            "projetos/sistemas/linux/reparar-fsck",
            "projetos/sistemas/linux/lock-screen",
          ],
        },
        {
          type: "category",
          label: "INFRA-LabRI/UNESP",
          items: [
            "projetos/sistemas/infralabriunesp/equipslabri",
            "projetos/sistemas/infralabriunesp/dualboot",
            "projetos/sistemas/infralabriunesp/scanners",
            {
              type: "category",
              label: "Configurações BIOS",
              items: [
                "projetos/sistemas/infralabriunesp/bios/dell",
                "projetos/sistemas/infralabriunesp/bios/ibm",
                "projetos/sistemas/infralabriunesp/bios/ryzen",
                "projetos/sistemas/infralabriunesp/bios/118",
                "projetos/sistemas/infralabriunesp/bios/119",
                "projetos/sistemas/infralabriunesp/bios/121",
                "projetos/sistemas/infralabriunesp/bios/123",
                "projetos/sistemas/infralabriunesp/bios/itautec",
              ],
            },
          ],
        },
        {
          type: "category",
          label: "INFRA-IPRI/UNESP",
          items: [
            "projetos/sistemas/infraipriunesp/fapesp",
          ],
        },
        {
          type: "category",
          label: "Redes",
          items: ["projetos/sistemas/cadernos/info",
          {
            type: "category",
            label: "Proxmox",
            items: [
              "projetos/sistemas/redes/proxmox/intro",
              "projetos/sistemas/redes/proxmox/instalacao",
              "projetos/sistemas/redes/proxmox/zfs",
              "projetos/sistemas/redes/proxmox/storage",
              "projetos/sistemas/redes/proxmox/apoio",
            ],
          },
          {
            type: "category",
            label: "Firewall",
            items: [
              "projetos/sistemas/redes/firewall/intro",
              "projetos/sistemas/redes/firewall/apoio",
            ],
          },
          {
            type: "category",
            label: "Container",
            items: [
              "projetos/sistemas/redes/container/intro",
              "projetos/sistemas/redes/container/apoio",
            ],
          },
          {
            type: "category",
            label: "Kubernetes",
            items: [
              "projetos/sistemas/redes/kubernetes/intro",
              "projetos/sistemas/redes/kubernetes/apoio"
            ],
          },
          {
            type: "category",
            label: "Rancher",
            items: [
              "projetos/sistemas/redes/rancher/intro",
              "projetos/sistemas/redes/rancher/apoio",
            ],
          },
          "projetos/sistemas/redes/google-authenticator"
        ],
        },
        {
          type: "category",
          label: "Infra-as-Code",
          items: [
            {
              type: "category",
              label: "Ansible",
              items: [
                "projetos/sistemas/infra-as-code/ansible/intro",
                "projetos/sistemas/infra-as-code/ansible/apoio",
              ],
            },
            {
              type: "category",
              label: "Terraform",
              items: [
                "projetos/sistemas/infra-as-code/terraform/intro",
                "projetos/sistemas/infra-as-code/terraform/apoio",
              ],
            },
          ],
        },
      ],
    },
  ],

  projetossistemasSite: [
    {
      type: "link",
      href: "/projetos/sistemas/",
      label: "Projetos de Sistemas",
    },
    {
      type: "category",
      label: "Website",
      items: [
        "projetos/sistemas/site/intro",
        {
          type: "category",
          label: "Editar",
          items: [
            "projetos/sistemas/site/editar/gepdai",
            "projetos/sistemas/site/editar/acessar",
            "projetos/sistemas/site/editar/paginas",
            "projetos/sistemas/site/editar/estrutura",
            "projetos/sistemas/site/editar/equipe",
            "projetos/sistemas/site/editar/md",
            "projetos/sistemas/site/editar/docusaurus",
            "projetos/sistemas/site/editar/sidebar",
            "projetos/sistemas/site/editar/static",
            "projetos/sistemas/site/editar/cadernos",
            "projetos/sistemas/site/editar/paginasjs",
          ],
        },
        {
          type: "category",
          label: "Markdown",
          items: [
            "projetos/sistemas/site/markdown/geral",
            "projetos/sistemas/site/markdown/especifico",
          ],
        },
        "projetos/sistemas/site/atividades",
      ],
    },
  ],
  
  projetossistemasCadernos: [
    {
      type: "link",
      href: "/projetos/sistemas/",
      label: "Projetos de Sistemas",
    },
    {
      type: "category",
      label: "Cadernos LabRI/UNESP",
      items: [
        "projetos/sistemas/cadernos/info",
        "projetos/sistemas/cadernos/proximos",
        "projetos/sistemas/cadernos/comandos-linux",
        "projetos/sistemas/cadernos/editor-codigo",
        "projetos/sistemas/cadernos/versionamento",
        "projetos/sistemas/cadernos/ambiente-virtual",
        "projetos/sistemas/cadernos/ambiente-virtual-windows",
        "projetos/sistemas/cadernos/dados-basicos",
        "projetos/sistemas/cadernos/ambiente-virtual-windows",
        "projetos/sistemas/cadernos/publi01",
        "projetos/sistemas/cadernos/publi02",
        "projetos/sistemas/cadernos/publi03",
        "projetos/sistemas/cadernos/publi04",
        "projetos/sistemas/cadernos/publi05",
        "projetos/sistemas/cadernos/publi07",
        "projetos/sistemas/cadernos/publi08",
        "projetos/sistemas/cadernos/publi09",
        "projetos/sistemas/cadernos/publi10",
      ],
    },
  ],

  projetossistemasCertificados: [
    {
      type: "link",
      href: "/projetos/sistemas/",
      label: "Projetos de Sistemas",
    },
    {
      type: "category",
      label: "Certificação",
      items: [
        "projetos/sistemas/certificados",

      ],
    },
  ],

  projetossistemasOCR: [
    {
      type: "link",
      href: "/projetos/sistemas/",
      label: "Projetos de Sistemas",
    },
    {
      type: "category",
      label: "Sistema de OCR",
      items: [
        "projetos/sistemas/ocr/intro",

      ],
    },
  ],

  newsletter: [
    {
      type: "link",
      href: "/projetos/sistemas/",
      label: "Projetos de Sistemas",
    },
    {
      type: "category",
      label: "Newsletter",
      items: [
        "newsletter/intro",
        "newsletter/001",
        {
          type: "category",
          label: "Newsletter 002",
          items: [
            "newsletter/002/intro",
            "newsletter/002/dados",
            "newsletter/002/sistemas",
            "newsletter/002/ensino",
            "newsletter/002/extensao",
            "newsletter/002/gestao-de-redes-sociais",
            "newsletter/002/divulgacoes",
            "newsletter/002/proximos-passos",
          ],
        },
      ],
    },
  ],

  gepdai: [
    {
    type: "category",
    label: "GEPDAI",
    items: [
      "grupo/gepdai/info",
      "grupo/gepdai/reunioes",
    ],
  },
],
};
