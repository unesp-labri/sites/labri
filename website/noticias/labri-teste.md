---
slug: labri-teste
tags: [COVID-19]
title: "Cooperação Internacional"
subtitulo: "Uma introdução"
author: Leonardo Landucci e Mayara Zorzo
author_title: "Graduandos em Relações Internacionais"
tipo_publicacao: Cadernos LabRI/UNESP - Núm. 1
image_url: https://i.imgur.com/po6ZnHc.jpg
descricao: Certos assuntos e temáticas permeiam o mundo e as Relações Internacionais de forma muito contundente, e um dos que aparece com mais força em todas as teorias é relativo à temática da cooperação internacional...
---

## Cooperação Internacional: Uma Introdução
_**Por Leonardo Landucci e Mayara Zorzo**_

Certos assuntos e temáticas permeiam o mundo e as Relações Internacionais de forma muito contundente, e um dos que aparece com mais força em todas as teorias é relativo à temática da cooperação internacional. Desde o cerne de criação da área, a temática em questão foi marcante, visto que evitar guerras foi a razão principal para a formulação dos estudos da política internacional. Dos seus primeiros pensadores até os atuais, todos levantaram questões como a possibilidade de uma cooperação concisa, os freios da ação estatal frente a outros países, assim como os motores para a mesma. A temática sempre deteve, e ainda mantém, uma presença notória na área, sendo o objetivo deste texto ressaltar sua importância e seus principais conceitos. 

![](https://i.imgur.com/po6ZnHc.jpg?2)
Vale mencionar também que a atuação e a importância da cooperação internacional se apresentam muito antes das formulações iniciadas pelos teóricos que se dedicaram ao estudo da área, já surgindo em estudos dos próprios formuladores das noções gerais do Estado. Nesse sentido, cooperar inclui atuar em conjunto com outros atores - sendo eles instituições ou Estados - para se atingir o mesmo fim. Essa prática se mostra presente desde negociações comerciais entre países até em resoluções de conflitos, como por exemplo as duas grande guerras mundiais.

Adentrando no pensamento teórico especificamente internacionalista, o conceito apareceu pela primeira vez fortemente durante o Primeiro Debate Teórico das Relações Internacionais, entre Realistas e Idealistas. Uma das premissas iniciais das teorias realistas está pautada no motor da ação estatal sendo o interesse, em linhas gerais, egoísta, em uma espécie de anarquia hobbesiana (guerra de todos contra todos, percepção de inimizade) no Sistema Internacional. Segundo Aron (1985), a tomada de decisão de um líder político será baseada no seu interesse em manter a proteção de seu Estado, visto que a garantia da segurança seria a motivação da união entre as nações.

Nesse sentido, a cooperação não era imaginada como uma possibilidade viável, com poucos riscos, já que, com base nos princípios de Maquiavel (2015), ela poderia fortalecer possíveis futuros inimigos e enfraquecer o agente central. Por outro lado, durante o Terceiro Debate (ou Debate Interparadigmático), entre neoliberais e neorrealistas, pela voz dos primeiros, adotando uma postura mais economicista, foi reforçada a interdependência internacional, trazendo uma nova perspectiva para a discussão sobre a cooperação.

Partindo da questão da interdependência e de uma anarquia no sentido lockeano (percepção de rivalidade), por não haver um regulador acima dos Estados, uma interligação e proximidade maior, o mercado e a economia seriam favorecidos, como um todo. Nesse sentido, não cooperar comprometeria toda a lógica internacional e afetaria o país que tomasse essa decisão de isolamento, tendo restringido sua própria atuação. De acordo com os pensamentos trabalhados por Keohane e Nye (1977) - pautados em certos preceitos kantianos - a cooperação possibilitaria um jogo de trocas que não excluiria a competição e as especificidades políticas e econômica entre os países.

Por outra visão, agora Construtivista, cabe ressaltar que, de certa forma, nenhuma dessas teorias está completamente errada. De acordo com Wendt (2006), nenhum desses conceitos, como anarquia e a própria cooperação, são monolíticos e estáticos, como expostos pelos autores anteriores. Partindo de uma visão de possibilidade de mudança e de dinâmica no cenário internacional, a cooperação partiria puramente das decisões daqueles com poder: se optariam por utilizar práticas, como as realistas ou as neoliberais, realmente construindo internacionalmente tais atividades conjuntas e como o fariam.

Também torna-se importante ressaltar a diferenciação entre a ação coordenada de Estados com um objetivo comum e a dependência que pode se apresentar de países menores em relação a grandes potências. O conceito de dependência (de países periféricos em relação aos considerados mais influentes e fortes) possui grande herança de todo o contexto da Guerra Fria, no qual a lógica do interesse estratégico dos Estados Unidos ou da União Soviética, era a motivação das alianças. Porém dentro do cenário em que estamos inseridos no momento e apesar de determinadas lideranças demonstrarem suas particularidades egoístas mesmo dentro de toda essa situação, a cooperação não deve ser ignorada e nem vista apenas a partir de óticas de interesses políticos. Sendo assim, alianças e auxílios recíprocos devem ser coordenadas visando unicamente a resolução de uma das maiores crises de saúde enfrentada pela humanidade.

Para além dos conceitos de Relações Internacionais, a cooperação pode ser realizada de diversas maneiras em diferentes âmbitos. Algumas delas que valem menção são: a nacional, bilateral, multilateral, judiciária, administrativa, técnica, financeira, humanitária, cultural e econômica. Essa tipologia e esses conceitos aqui apresentados estarão presentes em todos os nossos trabalhos sobre cooperação daqui em diante, tendo em mente a necessidade de ação e a importância do tema. Dessa maneira, é possível aferir, de forma breve, que da mesma forma que o isolamento social protege os indivíduos, a cooperação internacional pode proteger os Estados.

**REFERÊNCIAS BIBLIOGRÁFICAS**

ARON, Raymond. **Estudos Políticos**. 2. ed. Brasília: Universidade de Brasília, 1985. Tradução de Sérgio Bath.

JUS. **Princípio da cooperação internacional**. Disponível em: https://jus.com.br/artigos/26542/principio-da-cooperacao-internacional. Acesso em: 20 abr. 2020.

NYE, Joseph; KEOHANE, Robert. **Power and Interdependence**: world politics in transition. 4. ed. Us: Pearson Education (us), 1977.

MAQUIAVEL, Nicolau. **O Príncipe**. Sp: Novo Século, 2015.

WENDT, Alexander. **Social Theory of International Politics**. 9. ed. Cambridge: Cambridge University Press, 2006.
