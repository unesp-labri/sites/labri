---
slug: labri-n0041
tags: [COVID-19, CADERNOS]
title: "Futuro do controle de dados pós pandemia"
subtitulo: "as relações internacionais e a privacidade de dados"
author: Debate realizado pelo GT3 
author_title: Grupo de Trabalho 3 - As tecnologias e o controle da pandemia - do projeto LabRI Pandemia
tipo_publicacao: Cadernos LabRI/UNESP - Núm. 41
image_url: https://i.imgur.com/VjAEkhf.jpg
descricao: "Debate realizado no dia 29/10 com a participação da doutoranda Jaqueline Pigatto..."
download_pdf: /arquivos/2020-11-12-labri-041.pdf
serie: COVID-19
---
<div align="center">

![](https://i.imgur.com/VjAEkhf.jpg)

</div>

>*Este debate, intitulado "Futuro do controle de dados pós pandemia: às relações internacionais e a privacidade de dados", realizado no dia 29/10...*

<!--truncate-->

<div align="justify">

Este debate, intitulado "Futuro do controle de dados pós pandemia: às relações internacionais e a privacidade de dados", realizado no dia 29/10, contou com a participação da doutoranda Jaqueline Pigatto e do professor da UNESP Marcelo Mariano, ambos estudiosos do tema da tecnologia e às relações internacionais.


A discussão contou com a discussão dos temas acerca da Lei Geral de Proteção de Dados (LGPD), com a definição dos termos contidos nesta nova lei, o uso dos dados pessoais durante a pandemia do Novo Coronavírus e sugestões de como a coleta de dados pode ser usada em políticas públicas democráticas.

https://youtu.be/huTlMX7KCu0

</div>