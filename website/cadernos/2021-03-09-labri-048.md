---
slug: labri-n0048
tags: [COVID-19, CADERNOS]
title: "Os efeitos da pandemia na Agenda 2030 da ONU"
author: Ana Júlia Diniz Neves do Lago, Bianca Regina Poltronieri, Gabriela Guillardi e Natalia Ayumi G. Yoshida
tipo_publicacao: Cadernos LabRI/UNESP - Núm. 48
image_url: https://i.imgur.com/X6iHSVC.jpg
descricao: A partir de 1987, o termo desenvolvimento sustentável se tornou central na agenda internacional, o que acarretou em várias Conferências...
download_pdf: /arquivos/2021-03-09-labri-048.pdf
serie: COVID-19
---

<div align="center">

![](https://i.imgur.com/X6iHSVC.jpg)

</div>

>*A partir de 1987, o termo desenvolvimento sustentável[^1] se tornou central na agenda internacional, o que acarretou em várias Conferências sobre o tema na Organização das Nações Unidas (ONU) e levou à adoção da Agenda 21, na Rio 92...*

<!--truncate-->

### Introdução

<div align="justify">

A partir de 1987, o termo desenvolvimento sustentável[^1] se tornou central na agenda internacional, o que acarretou em várias Conferências sobre o tema na Organização das Nações Unidas (ONU) e levou à adoção da Agenda 21, na Rio 92; dos Objetivos de Desenvolvimento do Milênio (ODM), em 2000; e, por fim, da Agenda 2030, a qual é resultado da Rio+20. Esta última consiste em 17 Objetivos de Desenvolvimento Sustentável (ODS) e 169 metas, sendo indivisíveis, integradas e mescladas nas dimensões econômica, social e ambiental. Além disso, ela incentiva e apoia ações essenciais para a humanidade, como Parcerias, Prosperidade, Paz, Planeta e Pessoas.


No entanto, pode haver interferências no avanço dessa Agenda, como a descrença nas organizações internacionais e a pandemia do novo coronavírus. O primeiro é marcado pela crise do multilateralismo, o que provoca fragilização e desconfianças desses organismos, por exemplo, a própria ONU e suas agências, como a Organização Mundial da Saúde (OMS). Já o segundo, influencia de uma forma substancial os ODS e suas metas; entretanto, pode-se perguntar se essa influência seria positiva ou negativa, se a pandemia da Covid-19 impulsiona ou atrasa a Agenda 2030. Portanto, para uma melhor compreensão dos desafios enfrentados pela Agenda 2030, em decorrência da pandemia, separamos os pontos principais nos seguintes eixos:



### Saúde

Os primeiros efeitos da pandemia foram sentidos na área da saúde, com muitas pessoas infectadas pelo vírus e muitas morrendo. Devido a essa situação, a Covid-19 passou a estar no centro das atenções da comunidade internacional, levando-a a pensar em ações de curto prazo em detrimento das ações de longo prazo. Dessa forma, muitas das metas passaram a ser negligenciadas e deixadas em segundo plano, porém, elas continuam presentes e geram preocupações se serão alcançadas até 2030. Apesar da crise fazer os Estados focarem na política doméstica, a cooperação internacional continua sendo indispensável.



Nesse sentido, a pandemia do novo coronavírus afetou as pessoas e comunidades de modos diferentes, escancarando e intensificando as desigualdades e injustiças existentes. Os sistemas de saúde de grande parte dos Estados estão colapsando, inclusive dos países desenvolvidos, e os serviços básicos se mostram insuficientes.



No início da pandemia, muitos postos de saúde foram fechados, como medida do isolamento social, levando à interrupção de procedimentos essenciais, como a aplicação de vacinas, exames médicos e atendimento médico urgente. Mas, após a reabertura com os protocolos de segurança e serviços limitados, várias pessoas ainda sentem medo de ir até esses postos. Essa situação gera efeitos alarmantes, podendo regredir décadas de melhoras nos resultados da saúde.



De acordo com o Relatório de Objetivos de Desenvolvimento Sustentável 2020 da ONU, tal regressão está relacionada ao aumento da mortalidade materna e infantil – menores de 5 anos; da gravidez indesejada; de doenças não transmissíveis; de doenças transmissíveis, por exemplo o paludismo, o qual cresceu em 100% o número de mortes por essa doença na África Subsaariana; além disso, os esforços para a imunização infantil foram interrompidos. Ademais, os gastos diretos com atenção médica evoluíram consideravelmente, levando milhões de pessoas à extrema pobreza, há uma escassez de profissionais de saúde no mundo e uma grande carga de trabalho nas enfermarias para as mulheres. Assim, é preciso que os Estados estejam mais bem preparados para as futuras emergências de saúde pública e reunir esforços para que até 2030 seja possível alcançar uma cobertura sanitária universal.



Ainda segundo o Relatório da ONU, milhões de pessoas no mundo carecem de água e saneamento, dessa forma, não possuem instalações básicas para lavar as mãos com sabão e água, principal medida na prevenção da Covid-19, logo, não têm uma higiene adequada. Infelizmente, essa situação atinge as populações mais pobres dos Estados, aprofundando ainda mais as desigualdades existentes e afetando diretamente outros ODS. Junto a isso, o enorme estresse hídrico em várias regiões, sobretudo na África Setentrional e Ásia Central, gera preocupações para o desenvolvimento sustentável. Contudo, mesmo diante desse cenário, os financiamentos disponíveis para o Objetivo 6 são insuficientes para satisfazer as necessidades dos países, sendo de extrema importância o compromisso dos doadores.



Diante disso, empresas em diversos países estão desenvolvendo vacinas contra o novo coronavírus, com algumas em estágio avançado, no entanto, a principal preocupação é em sua distribuição, devido às questões logísticas dos Estados. Para vencer esse obstáculo, os governos precisam definir estratégias e estruturas, além de propor a colaboração entre o público e privado e entre governos, a fim de obter uma efetiva resposta da cadeia de suprimentos, e adotar a transparência da cadeia[^2]. Além disso, a iniciativa Covax, em que a OMS é uma de suas lideranças, tem o objetivo de garantir o acesso igualitário às vacinas aprovadas contra a Covid-19 aos países que aderirem à iniciativa, sendo uma ação coordenada de saúde pública global de grande importância.



### Social

É importante ressaltar que a pandemia não atinge a todos da mesma forma. Olhando pelo recorte social dos impactos nos ODS, é nítido como as camadas mais baixas e marginalizadas sofrem consideravelmente mais os impactos da COVID-19, direta ou indiretamente, em setores como o da alimentação, educação, igualdade de gênero, redução das desigualdades e paz, todos presentes nos objetivos da Agenda 2030.



Nesse viés, no setor da alimentação, destaca-se a importância dos pequenos produtores, responsáveis pela distribuição de insumos para os estabelecimentos os quais a população em geral terá acesso. Com a pandemia, esses agricultores foram prejudicados e a produção de alimentos diminuiu, o que, consequentemente, reduziu a oferta e elevou os preços do que estava disponível, de forma que as pessoas de baixa renda tiveram mais dificuldade no acesso a tais insumos, afetando sua nutrição. Por outro aspecto, destaca-se que muitas pessoas perderam parte ou a totalidade de suas rendas, o que também resultou nessa impossibilidade. Desse modo, a busca pela “fome zero” prevista na Agenda 2030 fica comprometida, considerando que o número de pessoas que estão em situação de insegurança alimentar aumentou durante o ano de 2020 e, provavelmente, continuará aumentando enquanto houver pandemia.



Outro objetivo da Agenda também afetado foi a educação de qualidade para todos. A pandemia impediu que aulas presenciais fossem viáveis devido a contaminação da COVID-19, e, por isso, foram realizadas por via remota em mais de 190 países[^3]. No entanto, a educação por tal meio implica em certas condições que nem todos os alunos têm acesso, como internet e aparelhos eletrônicos, e, assim, somam-se ao menos 500 milhões de crianças[^4] que não puderam ter o ensino de modo adequado ao longo de 2020. Ademais, conforme há o avanço no combate à pandemia, surge a tendência do retorno às aulas presenciais com todos os requisitos de segurança, o que configura mais um obstáculo para parte dos alunos que frequentam escolas as quais não possuem a infraestrutura adequada para recebê-los e promover um ambiente seguro, sem lugares apropriadas para higienização das mãos, por exemplo. Desse modo, aumenta o problema de crianças e adolescentes que não conseguem ter acesso à educação de qualidade, tornando o desafio de cumprir o ODS ainda maior.



Por outra perspectiva, ainda, o isolamento social exigido para a contenção da doença interferiu na forma como as desigualdades de gênero vinham sendo tratadas. Nesse âmbito, muitas mulheres condicionadas em seus lares tiveram que enfrentar o dobro de trabalho que realizavam antes da pandemia, tendo que, além de manter seus trabalhos “oficiais”, mesmo que em home office, lidar com a sobrecarga dos afazeres domésticos. Ainda que homens estejam ajudando mais com tais tarefas nos últimos anos, as maiores responsabilidades ainda caem sobre as mulheres, evidenciando o problema a ser enfrentado em busca de se atingir um dos objetivos da Agenda 2030. Além disso, a violência doméstica durante o referido período aumentou, já que o tempo de permanência em casa dos moradores foi maior devido ao confinamento, assim, crianças, especialmente meninas, e mulheres ficaram sob o risco maior de sofrerem abusos físicos e psicológicos.



Nesse seguimento que aborda a violência, também deve ser apontada seus efeitos em outras esferas para além da doméstica, como em conflitos armados. Destaca-se que, embora a pandemia tenha impedido que diversas atividades ocorressem a fim de evitar a transmissão do vírus, não aconteceu o mesmo com esses conflitos que ocorrem em diversas partes do mundo, pelo contrário, além de não acabarem, ainda comprometeram diversas instituições e ações destinadas ao combate da pandemia, principalmente o sistema de saúde.



Desse modo, além das dificuldades relatadas anteriormente, há, por consequência, o problema na questão da redução das desigualdades sociais prevista na Agenda. O fato de que, em outras áreas do desenvolvimento, aqueles que compõem a parcela mais pobre da população terem sido os principais afetados, aumenta ainda mais a dificuldade de alcançar tal objetivo, que consta como o ODS 10, uma vez que o contraste social foi intensificado durante o ano de 2020 e os problemas permanecerão não só enquanto a pandemia durar, mas também durante o período de recuperação dela. A exemplo da gravidade de tal intensificação, pontua-se que, somente no ano passado, mais de 68 milhões de pessoas passaram a viver com menos de U$ 1,90 por dia[^5], que representa a linha de extrema pobreza, e em paralelo a isso, a fortuna dos bilionários cresceu ainda mais, somando mais de U$ 10,2 trilhões[^6]. Dessa forma, cabe uma análise de que o problema da intensificação das desigualdades sociais não reside apenas no sentido de que milhões de pessoas se encontram em situação de extrema vulnerabilidade, mas também que, para que o objetivo seja cumprido, consiga-se notar que algumas pessoas continuam crescendo sobre as mazelas de outra parte da população. Nesse sentido, é muito importante que existam políticas que busquem ajudar os mais vulneráveis, mas também que atuem sobre os mais poderosos.



### Economia

Não bastasse uma desaceleração econômica cumulativa dos anos anteriores, a economia mundial sofreu graves abalos em 2020 por um fator não esperado: a pandemia de Covid-19. Com a necessidade de adoção do protocolo de isolamento e de restrição de circulação de pessoas, diversas atividades sofreram sequelas, segundo as quais a muito custo poderão recuperar-se, o que impactou diretamente o ODS 8 de Trabalho Decente e Crescimento Econômico. Os países em desenvolvimento - que já concentravam dilemas estruturais em seu cerne - foram ainda mais fragilizados pela pandemia, que desmascarou a precariedade do trabalho informal e a vulnerabilidade do mercado de trabalho. Segundo o Relatório de ODS 2020, devido ao desemprego e ao subemprego causados pela pandemia, cerca de 1.6 bilhão de trabalhadores informais serão consideravelmente afetados e, para além disso, registra-se que os mais afetados são os grupos socialmente vulneráveis de mulheres, jovens e pessoas com deficiência.



Entretanto, o auxílio emergencial do governo brasileiro amparou esses grupos vulneráveis e prorrogou um cenário muito pior, mas que, de toda a forma, não foi eximido, em razão das reformas socioeconômicas e políticas morosamente adiadas por toda a América Latina. Ainda, em 2021, o cenário que prevalece é o de incerteza no Brasil, posto que o auxílio se encerra de forma bruta - com apenas uma transição em duas etapas: de quatro parcelas de R$ 300 para zero - e sem a formulação de nenhuma política social em seu lugar. Não só o Brasil, como também todos os outros países latino-americanos, marcados por suas heterogeneidades, passam por um momento de muita debilidade em suas economias, agravada ainda mais por fatores estruturais, como os seus graus de integração no comércio internacional e nas cadeias globais de valor; os seus aspectos demográficos; as suas assimetrias socioeconômicas; e os níveis de formalização de seus mercados de trabalho.



Outra meta significativamente comprometida pela Covid-19 foi o ODS 9 de Indústria, Inovação e Infraestrutura. Tal objetivo foi acometido pela interrupção nas cadeias globais de valor e no fornecimento de produtos, além da perda de empregos e diminuição da jornada de trabalho. É preciso advertir que a participação da indústria, a maioria de pequena escala, no produto interno bruto (PIB) e no mercado de trabalho é essencial para os países em desenvolvimento, o que serve como um estímulo à tentativa de erradicação da pobreza - o ODS 1. Essas indústrias de pequeno e médio porte, no entanto, são vulneráveis à crises imprevistas, só conseguindo sobreviver por meio de acesso a serviços de crédito e de estímulo fiscal e financeiro.



Além disso, de acordo com o Relatório de ODS 2020, o setor de transporte aéreo foi altamente atingido e nos 5 primeiros meses de 2020 teve uma baixa de 51.1% no número de passageiros comparado ao mesmo período de 2019. Então, embora necessite de um esforço de coordenação global, a recuperação da indústria de aviação auxiliará na recuperação de outras áreas, tais como o comércio e o turismo, que também foram duramente atingidos.

_Gráfico 1 - Capacidade de assentos aéreos e número de passageiros de janeiro de 2019 para maio de 2020 (em milhões)_

![](https://i.imgur.com/V3Ln6il.png)

<figcaption>Fonte: The Sustainable Development Goals Report, 2020.</figcaption>



Em um contexto global deteriorado pela lentidão insistente, pela ampliação das assimetrias sociais e pelas incertezas no campo político, a crise sanitária desencadeou um choque sistêmico, com ramificações expressas diretamente no âmbito econômico. Nesse sentido, constata-se que a doutrina econômica neoliberal - a mesma que concebeu a pandemia por favorecê-la, ao perpetuar hábitos insustentáveis e ecoagressivos, e que agora é vítima de suas próprias consequências - teve que pagar por um alto custo causado pelo novo coronavírus, em que poderia ter sido evitado o seu desdobramento em tais proporções, caso houvesse um investimento maior na Agenda 2030, a qual visava justamente promover um mundo mais sustentável, com maior prosperidade e equidade, buscando mobilizar os meios necessários para garantir um ambiente seguro e saudável, com base em um espírito de solidariedade internacional. Isto posto, tanto o ODS 1 de Erradicação da Pobreza, quanto o ODS 12 de Consumo e Produção Responsáveis e o ODS 17 de Parcerias e Meios de Implementação já apresentavam um progresso mais lento no momento pré-pandemia, uma vez que os países, em grande parte desenvolvidos, não possuíam tais temáticas como prioritárias em suas agendas.



Para tanto, muitos países ainda subsidiam combustíveis fósseis, que além de serem prejudiciais por emitirem grandes quantidades de gases de efeito estufa, também são contraproducentes para os Estados em um momento que precisam captar mais recursos para financiar programas de combate a Covid-19, sendo oportuno a reforma desses subsídios para a realocação de recursos públicos. A elevação na taxa de pobreza extrema, também, é um indicador de que, apesar do influxo da pandemia, os esforços não têm sido suficientes para impedir um aumento desse número, haja vista que desde 2013 o ritmo de consecução da meta vem desacelerando, o que torna a concretização deste ODS até 2030 cada vez mais improvável.





_Gráfico 2 - Proporção de pessoas que vivem com menos de US$ 1.90 por dia, de 2010 a 2015, previsão de 2019 e previsão de antes e depois da Covid-19 (em porcentagem)_

![](https://i.imgur.com/MDlTaJi.png)

<figcaption>Fonte: The Sustainable Development Goals Report, 2020.</figcaption>



À vista disso, resta saber se a crise pandêmica deixará cicatrizes no sistema para além das que já conhecemos. Assim, embora a previsão para a recuperação econômica seja permeada por incertezas, de acordo com o Balanço Preliminar das Economias da América Latina e do Caribe 2020 feito pela Comissão Econômica para a América Latina e o Caribe (Cepal) há quatro elementos que podem influenciar na recuperação, sendo: a intensidade da segunda onda de contágios; a velocidade para a produção e distribuição de vacinas; a capacidade de cada Estado para manter os estímulos fiscais e monetários; e uma possível manifestação de tensões geopolíticas. Desse modo, faz-se necessário a discussão de uma retomada verde pós-pandemia, sobretudo através do uso da tecnologia e de parcerias, em que as empresas possam se posicionar como atores indispensáveis na instituição de uma economia verde, além de que sejam criadas mais oportunidades para que empresas de diferentes portes consigam cooperar em todos setores e esferas, visando o mesmo projeto de desenvolvimento sustentável.



### Sustentabilidade

Em decorrência da pandemia do Coronavírus, os investimentos em projetos de energia renovável e a aquisição de carros elétricos diminuíram. Isto porque, o impacto econômico advindo da pandemia afetou diretamente esses setores. E consequentemente, prejudicou a ODS 07, que diz respeito à energia limpa e acessível. A Agência Internacional de energia constatou que, de 2019 a 2020, a instalação global de energia limpa diminuiu em 13%. A Wood Mackenzie - um grupo de escala global especializado em consultoria e pesquisas em energia renovável, entre outros - já havia previsto uma queda em 18% das instalações solares pelo mundo no ano de 2020.



Além disso, a pandemia evidenciou a fragilidade em que se encontra a infraestrutura das cidades, principalmente em países mais carentes, a exemplo do Brasil. De acordo com o IBGE de 2010, cerca de 6% da população brasileira habitava loteamentos irregulares, favelas etc., e esses chamados “aglomerados subnormais” favorecem a proliferação da COVID-19, oferecendo, assim, maior risco de contaminação nas camadas mais pobres. A pandemia reforçou a importância do ODS 11, ou seja, de investir em moradia, saneamento básico e mobilidade urbana para evitar novas epidemias e pandemias que tendem a ser mais frequentes.



O vírus ajudou a reduzir drasticamente os níveis de carbono que eram emitidos para a atmosfera durante um curto período, entretanto, a pandemia pode ser considerada um desastre climático. Por isso, a ação contra a mudança climática (ODS 13) se faz tão atual e necessária, não só para evitar novas doenças, como também para garantir a vida terrestre.



A respeito do ODS 14 que dialoga sobre a vida na água, a pandemia acabou corroborando para alguns malefícios a ele. Como, por exemplo, o aumento de resíduos hospitalares tóxicos nos mares e oceanos que prejudicou enormemente a vida marinha. Sob orientação do PNUMA (Programa das Nações Unidas para o Meio-Ambiente), as nações deveriam manter um gerenciamento responsável dos químicos e de seus descartes. Tal programa estimula os países a desenvolverem uma economia azul sustentável durante a recuperação da pandemia e no mundo Pós-Covid.



O coronavírus é considerado uma zoonose, isto é, uma doença transmitida de animais para seres humanos. Nesse sentido, a destruição de florestas, selvas, entre outros, decorrentes da atividade humana faz com que o surgimento de novos vírus seja frequente; uma vez que os habitats naturais são devastados, os humanos passam a ter contato mais direto com animais selvagens, otimizando o desenvolvimento de novas doenças. Dessa forma, o ODS 15 - que se refere à vida terrestre - é de suma importância, tanto para preservação da biodiversidade quanto para prevenção de outras crises sanitárias.



### Conclusão

Por fim, evidencia-se o agravamento das desigualdades existentes nas comunidades internacionais e as dificuldades para o cumprimento da Agenda 2030. É possível, assim, pensar duas possibilidades ante o problema suscitado pela pandemia. A primeira, seria a pandemia ser empregada positivamente como um caminho para a recuperação pós-pandemia e para estreitar os laços do multilateralismo. A segunda, por sua vez, seria um desmantelamento das instituições multilaterais, o que acentuaria a regressão dos objetivos e das metas da Agenda 2030. Portanto, diante da presente pandemia, observa-se a dificuldade de fazer uma prospecção em relação aos possíveis cenários.



[^1]: Segundo o relatório Nosso Futuro Comum (1987), o termo desenvolvimento sustentável é definido como “O desenvolvimento que procura satisfazer as necessidades da geração atual, sem comprometer a capacidade das gerações futuras de satisfazerem as suas próprias necessidades.”

[^2]: SAÚDE BRASIL 2021, Summit. Quais são os desafios para a distribuição da vacina da Covid-19? Estadão. 01 dez. de 2020. Disponível em: https://summitsaude.estadao.com.br/desafios-no-brasil/quais-sao-os-desafios-para-a-distribuicao-da-vacina-da-covid-19/. Acesso em: 29 jan. de 2021.

[^3]: UNITED NATIONS. The Sustainable Development Goals Report: relatório técnico. New York, 2020.

[^4]: UNITED NATIONS. The Sustainable Development Goals Report: relatório técnico. New York, 2020.

[^5]: VALENSISI, Giovanni.COVID-19 and Global Poverty: Are LDCs Being Left Behind?. [s.l.], 21 de out. de 2020. Disponível em: <https://link.springer.com/article/10.1057/s41287-020-00314-8#Sec3>. Acesso em: 27 de jan. de 2021.

[^6]: Riqueza dos bilionários cresce durante a pandemia e atinge marca recorde de US$ 10,2 trilhões: número de bilionários passou de 2.158 em 2017 para 2.189 em 2020, segundo levantamento do banco suíço ubs e da pwc.. G1. [S. L.], p. 1-1. 07 out. 2020. Disponível em: <https://g1.globo.com/economia/noticia/2020/10/07/riqueza-dos-bilionarios-cresce-durante-a-pandemia-e-atinge-marca-recorde-de-us-102-trilhoes.ghtml>. Acesso em: 27 jan. 2021.



<article></article>

### Referências Bibliográficas

AGENDA 2030. Conheça a Agenda 2030. Disponível em: <http://www.agenda2030.com.br/sobre/>. Acesso em: 29 jan. de 2021.

ALMEIDA, Paulo; PASTERNAK, Natalia. Entenda o programa de vacinas da OMS de que o Brasil vai participar. Revista Questão de Ciência. 20 set. de 2020. Disponível em: https://www.revistaquestaodeciencia.com.br/artigo/2020/09/20/entenda-o-programa-de-vacinas-da-oms-de-que-o-brasil-vai-participar. Acesso em: 29 jan. de 2021.

CEPAL - COMISSÃO ECONÔMICA PARA A AMÉRICA LATINA E O CARIBE. Balance Preliminar de las Economías de América Latina y el Caribe: relatório técnico. Santiago, 2020. 

FARIZA, Ignacio. Diretora do FMI pede aos países latino-americanos que não retirem o apoio fiscal: “A pandemia não acabou”. El país. Disponível em: <https://brasil.elpais.com/economia/2020-12-17/diretora-do-fmi-pede-aos-paises-latino-americanos-que-nao-retirem-o-apoio-fiscal-a-pandemia-nao-acabou.html#?sma=newsletter_brasil_diaria20201218>. Acesso em: 02 jan. 2021.



FERNÁNDEZ, David. 2021: uma injeção de esperança econômica. El país. Disponível em: <https://brasil.elpais.com/economia/2020-12-27/2021-uma-injecao-de-esperanca-economica.html#?sma=newsletter_brasil_diaria20201228>. Acesso em: 02 jan. 2021.



GRUPO DE TRABALHO DA SOCIEDADE CIVIL PARA AGENDA 2030 DO DESENVOLVIMENTO SUSTENTÁVEL. Especialistas destacam relevância da Agenda 2030 para a reconstrução do mundo pós-covid-19. Disponível em: <https://gtagenda2030.org.br/2020/06/30/especialistas-destacam-relevancia-da-agenda-2030-para-a-reconstrucao-do-mundo-pos-covid-19/>. Acesso em: 27 de jan. de 2021.

MAIA, Bibiana. Cidades mais sustentáveis, menos epidemias: Epidemiologista aponta que desmatamento e mudanças climáticas aumentam a probabilidade de vírus atingirem população humana. [S. l.], 26 mar. 2020. Disponível em: <https://projetocolabora.com.br/ods11/cidades-mais-sustentaveis-menos-epidemias/>. Acesso em: 27 jan. 2021.

MENDONÇA, José Eduardo. O que a pandemia tem a ver com a mudança do clima?: Como outras crises contemporâneas, a disseminação da covid-19 exige resposta local e global, guiada pela ciência, e pensamento a longo prazo. [S. l.], 11 abr. 2020. Disponível em: <https://projetocolabora.com.br/ods13/o-que-a-pandemia-tem-a-ver-com-a-mudanca-do-clima/>. Acesso em: 27 jan. 2021.

MONTES, Rocío. Pandemia deteriora as democracias na América Latina e aumenta o descontentamento. El país. Disponível em: <https://brasil.elpais.com/internacional/2020-12-19/pandemia-deteriora-as-democracias-na-america-latina-e-aumenta-o-descontentamento.html#?sma=newsletter_brasil20201219>. Acesso em: 02 jan. 2021. 

ORLIANGE, Philippe; PINCEMIN, Carlos. IS THE COVID-19 PANDEMIC A STRESS TEST FOR THE 2030 AGENDA? Boletim de Economia e Política Internacional, Ipea, n. 27, pp. 109-124, Maio 2020/Agosto 2020.

PNUD BRASIL. Relatório da ONU sobre progresso dos ODS aponta que a COVID-19 está comprometendo avanços no campo social. Disponível em: <https://www.br.undp.org/content/brazil/pt/home/presscenter/articles/2020/relatorio-da-onu-aponta-que-a-covid-19-esta--retardando--decadas.html>. Acesso em: 27 de jan. de 2021.

QUATRO Objetivos de Desenvolvimento Sustentável (ODSs) são chave no pós-covid. Centro de estudos estratégicos da Fiocruz, [s. l.], 3 jun. 2020. Disponível em: <https://www.cee.fiocruz.br/?q=node/1192>. Acesso em: 27 jan. 2021.

RIQUEZA dos bilionários cresce durante a pandemia e atinge marca recorde de US$ 10,2 trilhões: número de bilionários passou de 2.158 em 2017 para 2.189 em 2020, segundo levantamento do banco suíço ubs e da pwc.. G1. [S. L.], p. 1-1. 07 out. 2020. Disponível em: <https://g1.globo.com/economia/noticia/2020/10/07/riqueza-dos-bilionarios-cresce-durante-a-pandemia-e-atinge-marca-recorde-de-us-102-trilhoes.ghtml>. Acesso em: 27 jan. 2021.



ROUBICEK, Marcelo. O que vem após o fim do auxílio emergencial aos brasileiros. NEXO. Disponível em:  <https://www.nexojornal.com.br/expresso/2020/12/29/O-que-vem-ap%C3%B3s-o-fim-do-aux%C3%ADlio-emergencial-aos-brasileiros>. Acesso em: 02 jan. 2021.



SAÚDE BRASIL 2021, Summit. Quais são os desafios para a distribuição da vacina da Covid-19? Estadão. 01 dez. de 2020. Disponível em: <https://summitsaude.estadao.com.br/desafios-no-brasil/quais-sao-os-desafios-para-a-distribuicao-da-vacina-da-covid-19/>. Acesso em: 29 jan. de 2021.

UNITED NATIONS. The Sustainable Development Goals Report: relatório técnico. New York, 2020. 



VALENSISI, Giovanni.COVID-19 and Global Poverty: Are LDCs Being Left Behind?. [s.l.], 21 de out. de 2020. Disponível em: <https://link.springer.com/article/10.1057/s41287-020-00314-8#Sec3>. Acesso em: 27 de jan. de 2021.

VIEIRA, Nathan. Em tempos de coronavírus, como fica a situação da energia limpa? [S. l.], 10 jul. 2020. Disponível em: <https://canaltech.com.br/meio-ambiente/em-tempos-de-coronavirus-como-fica-a-situacao-da-energia-limpa-167828/>. Acesso em: 27 jan. 2021.

</div>