---
slug: labri-n0012
tags: [COVID-19, CADERNOS]
title: "A Atuação do Conselho de Segurança das Nações Unidas em relação à COVID-19"
author: Jonas Vieira, Leonardo Landucci, Mayara Zorzo
author_title: Graduandos em Relações Internacionais
tipo_publicacao: Cadernos LabRI/UNESP - Núm. 12
image_url: https://i.imgur.com/ov3HC3g.png
descricao: No atual momento em que se encontra a crise global gerada pela pandemia do novo coronavírus, tornou-se evidente a importância de atuações políticas eficientes. Tal importância pode ser avaliada através de comparações entre diferentes reações de líderes políticos...
download_pdf: /arquivos/2020-05-14-labri-012.pdf
serie: COVID-19
---

<div align="center">

![](https://i.imgur.com/ov3HC3g.png)

</div>

>*No atual momento em que se encontra a crise global gerada pela pandemia do novo coronavírus, tornou-se evidente a importância de atuações políticas eficientes...*

<!--truncate-->

<div align="justify">

No atual momento em que se encontra a crise global gerada pela pandemia do novo coronavírus, tornou-se evidente a importância de atuações políticas eficientes. Tal importância pode ser avaliada através de comparações entre diferentes reações de líderes políticos frente à pandemia e as respectivas consequências geradas ao seu país. Como um exemplo da falta de responsabilidade governamental, pode-se citar o Estados Unidos - responsável por manter uma política negacionista no início - que atualmente é o novo epicentro da doença, possuindo o maior número de casos confirmados e de  [mortes](https://www.google.com/url?q=https%3A%2F%2Fcoronavirus.jhu.edu%2Fmap.html&sa=D&sntz=1&usg=AFQjCNGVv3ac9jvixyxUNL2XVB_UB6ebOg).


Frente a isso, a atuação de organizações internacionais (OIs) se coloca, também, como essencial. Dentre elas, destaca-se o Conselho de Segurança das Nações Unidas (CSNU), o órgão com poder decisório da Organização das Nações Unidas (ONU), o qual é formado por 15 países membros: 10 rotativos (com eleições a cada 2 anos) e 5 permanentes (Estados Unidos, Reino Unido, China, Rússia e França, ou “grupo do P5”).

De acordo com o  [artigo 24](https://www.google.com/url?q=https%3A%2F%2Flegal.un.org%2Frepertory%2Fart24.shtml&sa=D&sntz=1&usg=AFQjCNGQ7K4zd31U16wOwBaysj7CL2kHUw)  da  [Carta da ONU](https://www.google.com/url?q=https%3A%2F%2Fwww.un.org%2Fen%2Fcharter-united-nations%2F&sa=D&sntz=1&usg=AFQjCNEYXRMDLMhY7XCwiL27luLBStltDw)  (1945), o CSNU é responsável pela garantia e manutenção da paz e da segurança no mundo, e seus membros devem aceitar como também executar as ações que forem determinadas durante as reuniões do mesmo. Dentre as principais formas de atuação do órgão está a organização de reuniões entre as lideranças políticas e as missões de paz da ONU, objetivando o cessar-fogo de zonas conflituosas, com o objetivo de levar à cabo esforços políticos que possam resolver as hostilidades entre as partes envolvidas por via diplomática.

Durante o presente cenário de incertezas que a comunidade internacional se depara, a atuação e posicionamento do CSNU são de extrema importância, visto que, atualmente, algumas regiões são vítimas de conflitos armados, gerando danos irreparáveis à população local, o que tende a ser potencializado pela disseminação do novo coronavírus, a COVID-19, já que estas pessoas em sua grande maioria vivem na extrema pobreza e sem acesso a tratamento médico básico. Deste modo, faz-se necessária uma cooperação entre os vários órgãos da ONU, sobretudo, entre a Organização Mundial da Saúde (OMS) e o CSNU.

Desde o início da pandemia, críticas negativas emergiram direcionadas ao CSNU por parte de especialistas e diplomatas. Sua ineficiência e indisposição para tratar do assunto demonstravam a forma como o mundo viria a lidar com a questão: sem cooperação e com “troca de farpas”. De um lado, a China defendia que o tema estaria fora do escopo do CSNU, não cabendo, portanto, a discussão em seu âmbito, por outro lado, os Estados Unidos sinalizaram que era necessário discutir o surgimento do novo vírus, pois acusam o mesmo de ter sido criado em laboratórios estatais chineses. Este impasse, como exposto pela Reuters (2020) dificultou qualquer tipo de ação por parte do órgão da ONU.

Essa discussão levantou a necessidade de um  [posicionamento oficial](https://www.google.com/url?q=https%3A%2F%2Fwww.reuters.com%2Farticle%2Fus-health-coronavirus-un%2Fu-n-security-council-meets-over-coronavirus-as-it-struggles-to-act-idUSKCN21R3PB&sa=D&sntz=1&usg=AFQjCNHBaU12ipEer7rIg1cwxZVWXqiRJg)  do embaixador chinês na ONU, Zhang Jun, frente ao CSNU. Visto que ele reforçou a importância da cooperação frente à COVID-19 e do fim da tentativa estadunidense de rotular a doença como “vírus chinês”, como exposto pelo presidente dos EUA, Donald Trump, em entrevista. Outro importante e conflitivo parecer dado perante à ONU foi o de António Guterres, secretário-geral das Nações Unidas, cujas palavras reforçaram a necessidade de uma ação do CSNU frente ao coronavírus, justamente no sentido de aliviar as ansiedades e tensões causadas pela falta de iniciativa e cooperação.

Após a crise diplomática entre Estados Unidos e China ter sido reduzida em prol de uma ação do CSNU e, por insistência alemã, foi marcada a reunião e dois textos estariam entrando em discussão. O primeiro foi movimentado pelos países com cadeiras não-permanentes no CSNU, seu conteúdo havia sido mobilizado pela Tunísia e consistia em um cessar-fogo imediato dos conflitos mundiais em prol de uma ação coordenada, visando a contenção da COVID-19. O segundo surgiu por iniciativa da França, com intuito de cessar hostilidades temporariamente ao redor do globo. Esse texto fomentou controvérsia, precisamente por privilegiar o grupo P5, dividindo a união, já fragilizada, entre os países com cadeiras permanentes e rotativas.

Em  [ligação privada](https://www.google.com/url?q=https%3A%2F%2Fwww.interfax.com%2Fnewsroom%2Ftop-stories%2F68466%2F&sa=D&sntz=1&usg=AFQjCNHHVN87FnZ7kLdnlmqGowzLTnyYxQ), posteriormente divulgada à mídia, os presidentes russo e francês, respectivamente Putin e Macron, anunciaram o desejo pela realização efetiva de uma reunião com os membros do P5, acerca da COVID-19, e também do próprio Conselho de Segurança como um todo. Em meio aos posicionamentos sobre a necessidade do encontro e a emergência da pauta, tanto o documento da Tunísia quanto o da França deram forma a um novo rascunho que conteria um plano de ação formal para a minimização dos danos, dentro dos limites de escopo do órgão.

Portanto, na última quinzena de abril, o CSNU se reuniu por videoconferência para discutir, principalmente, o impacto que a pandemia da COVID-19 pode trazer às sociedades de países com os sistemas de saúde pulverizados em decorrência de longos anos de guerras civis, como, por exemplo, Líbia, Afeganistão, Iraque, Síria, Sudão e Iêmen. Dessa forma, o CSNU objetiva pedir por meio de um projeto de resolução, que os beligerantes em conflitos imponham um cessar-fogo, ou “pausa humanitária”, de 90 dias. A medida visa facilitar a entrega de ajuda humanitária , sem que ela encontre obstáculos e seja feita de forma segura.

António Guterres em  [fala](https://www.google.com/url?q=https%3A%2F%2Fnews.un.org%2Fpt%2Fstory%2F2020%2F04%2F1712152&sa=D&sntz=1&usg=AFQjCNG3fI1UABZ5pc8le1hCOTMvY8LrQQ)  dada aos jornalistas, no dia 30 de abril, afirmou ter recebido o apoio sobre a proposta de cessar-fogo de 114 governos, organizações regionais e líderes religiosos, além de 16 grupos armados. Segundo ele, os esforços para o fim das hostilidades dependem de apoio político e afirma ter “esperança que o Conselho de Segurança será capaz de encontrar unidade e adotar decisões que ajudem a tornar o cessar-fogo uma realidade”.

Todavia, a resolução não implica o cessar-fogo às  [operações militares](https://www.google.com/url?q=https%3A%2F%2Fwww.thejakartapost.com%2Fnews%2F2020%2F04%2F23%2Fdraft-virus-resolution-submitted-to-the-un-security-council.html&sa=D&sntz=1&usg=AFQjCNFU4gu_-xqPYbKO0WTRX5jUJnN4-A) contra o Estado Islâmico, Al-Qaeda, Al-Nusra e qualquer outro grupo listado como terrorista pelo CSNU. Um outro ponto-chave, é que o chefe das Forças de Paz da ONU, Jean Pierre-Lacroix, defendeu que os “capacetes azuis” sejam  [mantidos](https://www.google.com/url?q=https%3A%2F%2Fnews.un.org%2Fpt%2Fstory%2F2020%2F04%2F1709552&sa=D&sntz=1&usg=AFQjCNHLGAmgycEjraVYXIycMdLTA1X8LA)  em solo para trabalhar coordenadamente com os governos dos países em conflito na luta contra o coronavírus. Reiterou, também, que o CSNU está dando suporte multifacetado aos Estados:



<span>Estamos facilitando comunicações remotas graças a nossos meios tecnológicos, estamos ajudando a garantir que cadeias de suprimento críticas sejam mantidas e nosso pessoal está alertando as comunidades sobre o coronavírus, através de plataformas de rádio local e mídia digital, assim como durante o patrulhamento.</span>



O Iêmen é um dos países que mais geram preocupações à ONU. Martin Griffiths, enviado especial da ONU, disse ao Conselho de Segurança, que o Estado não pode sobreviver à uma guerra travada simultaneamente em dois frontes: uma bélica e outra pandêmica. Pois, a guerra civil no país que já dura mais de 5 anos, pulverizou a infraestrutura de saúde, o sistema imunológico da população e contribuiu para a proliferação de diversas outras doenças, como, por exemplo, a cólera.

A Guerra Civil Iemenita (2015-presente) teve suas origens na Primavera Árabe, quando o presidente autocrático do país, Ali Abdullah Salleh, foi deposto após quase 32 anos à frente do governo, delegando o poder ao vice-presidente, Abdrabbuh Mansour Hadi. O conflito está centrado em torno de dois grandes grupos: a coalizão formada pelos apoiadores de Hadi (EUA, Reino Unido, Arábia Saudita e outros 8 países árabes) e os hutis, grupo apoiado pelo Irã de minoria xiita zaidi, que aproveitou da debilidade do novo presidente para tomar o controle da Província de Saada e de suas zonas limítrofes. Segundo  [dados](https://www.google.com/url?q=https%3A%2F%2Fwww.bbc.com%2Fportuguese%2Finternacional-46322964&sa=D&sntz=1&usg=AFQjCNHMUvFlwdvgpf2LZfjnw4gUfBMMCg)  do Conselho de Direitos Humanos da ONU, o conflito iemenita levou o país à extrema pobreza, na qual 22,2 milhões de pessoas (75% da população) necessitam de ajuda humanitária imediata para sobreviver, já que sofrem de insegurança alimentar e desnutrição. Outro grande problema, é que somente 50% das instalações sanitárias do país funcionam completamente - mais de 16,4 milhões de pessoas não têm acesso à assistência médica básica.

Devido a esse cenário caótico, a OMS teme que o novo coronavírus esteja circulando ativamente no país, podendo contaminar mais de 50% da população, o que seria catastrófico para o precário sistema de saúde. Duas mortes já foram  [registradas](https://www.google.com/url?q=https%3A%2F%2Fnews.un.org%2Fpt%2Fstory%2F2020%2F05%2F1712422&sa=D&sntz=1&usg=AFQjCNHXztvgU2cG_-rHLnmAZeQpv6xJxA)  no sul do Iêmen, região mais afetada pela guerra. Na mesma entrevista dada por Guterres, ele disse que “há uma oportunidade de paz” no Iêmen, visto que a Arábia Saudita declarou um cessar-fogo unilateral e finalizou dizendo que “é hora de reconhecer que o povo iemenita já sofreu demais”.

Porém a resolução de cessar-fogo ainda carece de votação no CSNU, e encontra  [barreiras](https://www.google.com/url?q=https%3A%2F%2Fwww.theguardian.com%2Fus-news%2F2020%2Fapr%2F19%2Fus-and-russia-blocking-un-plans-for-a-global-ceasefire-amid-crisis&sa=D&sntz=1&usg=AFQjCNFuQ-83e1KYVxN7YBueps_MUwxE4Q)  nos EUA e na Rússia, pois ambos temem que ela possa prejudicar o andamento das operações militares anti-terroristas de seus governos. O primeiro, quer manter o direito de ataque das posições pró-Irã no Iraque, e por sua vez, a Rússia, a manutenção de sua presença militar na Síria e apoio dado às milícias não estatais líbias.

Desse modo, é possível concluir que a presente pandemia tem gerado impactos aos diferentes setores, tanto dos Estados, quanto das Organizações Internacionais e, portanto, a coordenação eficiente e detalhada de ações políticas, econômicas, sociais e sanitárias se torna essencial e é, atualmente, a melhor forma de encontrar soluções para que menores danos sejam causados a populações de todo o mundo e para que, também, vidas possam ser poupadas.



<article></article>

### Referências Bibliográficas

BRASIL, Nações Unidas. **A ONU, a paz e a segurança**. Disponível em: https://nacoesunidas.org/acao/paz-e-seguranca/. Acesso em: 05 maio 2020.

INTERFAX. **Putin, Macron discuss possibility of virtual working meeting among heads of UNSC permanent member states**. 2020. Disponível em: https://www.interfax.com/newsroom/top-stories/68466/. Acesso em: 04 maio 2020.

ISRAEL, Times Of. **UN Security Council to hold first ‘virtual’ debate on coronavirus crisis**. 2020. Disponível em: https://www.timesofisrael.com/un-security-council-to-hold-first-virtual-debate-on-coronavirus-crisis/. Acesso em: 04 maio 2020.

MEDICINE, Johns Hopkins University &. **Coronavirus Resource Center**. Disponível em: https://coronavirus.jhu.edu/map.html. Acesso em: 05 maio 2020.

NEWS, Voa. **Draft COVID-19 Resolution Submitted to UN Security Council**. 2020. Disponível em: https://www.voanews.com/covid-19-pandemic/draft-covid-19-resolution-submitted-un-security-council. Acesso em: 04 maio 2020.

PEACEKEEPING, United Nations. **Role of the Security Council**. Disponível em: https://peacekeeping.un.org/en/role-of-security-council. Acesso em: 05 maio 2020.

REUTERS. **U.N. Security Council meets over coronavirus as it struggles to act**. 2020. Disponível em: https://www.reuters.com/article/us-health-coronavirus-un/u-n-security-council-meets-over-coronavirus-as-it-struggles-to-act-idUSKCN21R3PB. Acesso em: 04 maio 2020.

</div>