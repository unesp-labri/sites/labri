---
slug: labri-n0023
tags: [COVID-19, CADERNOS]
title: "O home office é o futuro do mercado de trabalho?"
author: Matheus Valencia
author_title: Graduando em Relações Internacionais
tipo_publicacao: Cadernos LabRI/UNESP - Núm. 23
image_url: https://i.imgur.com/bMYYwgc.jpg
descricao: A quarentena promovida pela pandemia do novo coronavírus isolou, ao redor do mundo, mais de 4 bilhões de pessoas. Muitos desses indivíduos, trabalhadores formais e informais, adotaram mecanismos que permitem a realização de suas tarefas de seus lares...
download_pdf: /arquivos/2020-06-19-labri-023.pdf
serie: COVID-19
---

<div align="center">

![](https://i.imgur.com/bMYYwgc.jpg)

</div>

>*A quarentena promovida pela pandemia do novo coronavírus isolou, ao redor do mundo, mais de 4 bilhões de pessoas. Muitos desses indivíduos, trabalhadores formais e informais, adotaram mecanismos que permitem a realização de suas tarefas de seus lares...*

<!--truncate-->

<div align="justify">

A quarentena promovida pela pandemia do novo coronavírus isolou, ao redor do mundo, mais de 4 bilhões de pessoas. Muitos desses indivíduos, trabalhadores formais e informais, adotaram mecanismos que permitem a realização de suas tarefas de seus lares. A radical medida do isolamento social intensificou uma tendência no mercado de trabalho: o  _home office_. Conhecido por idealizar um escritório caseiro e destinado a uma pequena porcentagem de funcionários com funções específicas, o trabalho domiciliar tornou-se uma tendência global adotada por empresas como forma de combater os danos econômicos gerados pelo atual recesso.


Com uma aprovação, por gestores de empresas, de quase 80%, o  _home office_  possibilitou a integração de diversas tecnologias. Skype, Google Meet e Zoom são utilizados diariamente, por milhões de usuários, como veículo para a realização de reuniões. O pacote Office e ferramentas disponibilizadas pelo Google permitem que funcionários escrevam, planejem e apresentem projetos requisitados por seus chefes. A evolução da internet, nas últimas duas décadas, pareceu miraculosamente articulada para resguardar as necessidades de pequenas, médias e grandes empresas em tempos de crise. A impossibilidade de aglomerações pode, hoje, ser solucionada por uma ou duas plataformas com suporte irrestrito.

O recrutamento para o trabalho  _home office_  cresce exponencialmente a cada dia, acompanhando as necessidades dos mais diversos setores da economia. Na educação, aulas, palestras e cursos são ministrados a distância com professores capacitados e avaliações adaptadas ao meio. Escolas e faculdades particulares - e algumas das públicas - utilizam esse método como forma de evitar o cancelamento de semestres e o atraso de matérias que colocariam em jogo o futuro do aluno. A medida, entretanto, instaura questionamentos relacionados à desigualdade social; no Brasil cerca de um entre quatros brasileiros não possuem acesso à internet, e em áreas rurais e periféricas o número revela-se ainda maior.

Entretanto, o sonho de um escritório ao lado da cama, representando o conforto e a comodidade e isentando o indivíduo do  _stress_  diário proporcionado pelo trânsito e filas, parece sobrepujar questões socioeconômicas divisivas no futuro do mercado de trabalho. Uma pesquisa realizada pela consultoria de recursos humanos Randstad revela que 7 entre 10 brasileiros optaram por um futuro onde o trabalho não necessite de atuação presencial. Para essas pessoas, o modelo clássico de trabalho, realizado em amplos escritórios com inúmeros outros parceiros está obsoleto e não acompanha a constante evolução da tecnologia.

Para Álvaro de Mello, professor da Business School São Paulo (BSP), “o home office será a realidade de milhões brasileiros nos próximos anos, sobretudo nas grandes cidades sufocadas pelo trânsito”. A previsão pode parecer utópica, mas não intangível; para especialistas o trabalho em casa pode significar uma queda nos custos de empresas que gerenciam enormes instalações responsáveis por abrigar diariamente seus funcionários. Além disso, um estudo realizado pela Dell, denominado “_Global Evolving Workforce_”, salientou que a adoção desse método resultaria em uma maior produtividade dos funcionários e suas equipes, o bem estar propiciado fortaleceria o profissionalismo evitando hábitos desfavoráveis e conflitos internos.

Mas não só de benefícios o  _home office_  é constituído, como já brevemente mencionado pela questão socioeconômica. Nem todo profissional está preparado para orbitar no mesmo cômodo diariamente e realizar suas tarefas com excelência, além de empresas também encontrarem obstáculos ao ter que, essencialmente, fornecer meios para que seus funcionários tenham acesso às plataformas onde o serviço será realizado. A possível falta de organização somada ao excesso de distrações facilitaria a dispersão do indivíduo frente às suas funções, um autogerenciamento, se mal executado, poderia acarretar no comodismo e consecutivamente num quadro de procrastinação. A inexistência de um chefe, em sua forma física, possibilitaria o relaxamento da percepção de “pressão” durante a entrega de, por exemplo, um relatório. O histórico cultural criado no ambiente de trabalho que, de uma forma ou outra, abarca os malefícios do trabalho presencial, também proporciona a pontualidade e o respeito.

A volta à rotina de trabalho se mostrará muito diferente do que era antes da quarentena. A adoção de medidas sanitárias, como o espaçamento entre os postos de trabalho, representa uma elevação nos custos de empresas que, paulatinamente, se reerguerão perante a súbita crise econômica a qual enfrentamos. Um estudo da empresa Cushman&Wakefield revelou que 45% dos empresários entrevistados decidiram por uma redução do espaço físico após a crise e 30% entre eles adotarão essas medidas em virtude do sucesso encontrado pela implementação do  _home office_. O mesmo estudo evidenciou um futuro homogêneo onde 40,2% das empresas que não realizavam o trabalho a distância o adotarão de forma definitiva após o fim da pandemia.

No exterior, grandes empresas como Facebook, Google e Twitter anunciaram oficialmente um prolongamento do regime de  _home office_ para além da dissolução da quarentena. Segundo Conrado Leisler, diretor geral do Facebook e do Instagram no Brasil, qualquer funcionário que opte pelo trabalho residencial até o fim do ano poderá executá-lo sem penalidades da empresa. O objetivo é garantir a segurança e a saúde dos trabalhadores que, numa situação de trabalho presencial, adoeceriam e facilitariam a disseminação do novo coronavírus. O Facebook, entretanto, sendo uma companhia digital, forneceu uma plataforma própria para o uso de seus funcionários, o  _Workplace, nome da plataforma,_  representa uma evolução na facilitação das demandas requeridas pela empresa.

Outras plataformas de gestão, como a Omie, resulta num gasto de aproximadamente R$ 900 por mês para cada funcionário, o valor pode parecer exorbitante para pequenas e médias empresas, mas para grandes companhias representam uma queda dos custos na manutenção presencial de seus trabalhadores. Empreendedores com menor poder aquisitivo podem aderir a ferramentas gratuitas de organização e logística, como o Trello (um espaço virtual baseado na mecânica de to-do list), o Google Drive (voltado para o armazenamento e compartilhamento de arquivos, o Slack (plataforma de envio de mensagens entre equipes) e também o Toggl (responsável por registrar suas horas trabalhadas). As opções são variadas e suas funcionalidades capazes de promover uma melhor experiência do trabalho doméstico sem imprevistos e obstáculos.

Cercado de benefícios, o _home office_  representa então, uma forte tendência para o futuro do mercado, como já comprovado. Ao alinhar os propósitos dos trabalhadores ao constante caminhar evolutivo das tecnologias que nos cerceiam, o trabalho doméstico traz consigo projeções promissoras após o encerramento da pandemia. Entretanto, como citado antes, o mesmo não é atribuído a todos os indivíduos de forma similar, questões socioeconômicas como a ausência de dispositivos com acesso a internet e a capacitação de funcionários adeptos ao mundo virtual estampam as maiores dificuldades que a transição do trabalho pode encontrar nos próximos anos.

Além disso, ofícios necessariamente presenciais ligados à serviços básicos e de manutenção não podem ser realizados a distância. Por mais que uma grande porcentagem do todo migre para o ambiente digital, funcionários com responsabilidades internas deverão atuar presencialmente e a mudança não os afetará. Cabe, dessa forma, às empresas, beneficiarem tais indivíduos evitando uma segregação do ambiente de trabalho e a obsolência de suas funções. O _home office_  por fim, encabeça o futuro de uma economia pungente, dinâmica e virtual, onde as lições aprendidas durante quatro Revoluções Industriais serão postas em prática e encaminharão o mundo para experimentação da quinta.



<article></article>

### Referências Bibliográficas

ESTADÃO CONTEÚDO. **Coronavírus: como será o futuro dos escritórios após a pandemia**. 2020. Disponível em: https://epocanegocios.globo.com/Carreira/noticia/2020/05/coronavirus-como-sera-o-futuro-dos-escritorios-apos-pandemia.html. Acesso em: 24 maio 2020.

EXAME. **Trabalho do futuro: como o home office pode transformar uma empresa?**. 2020. Disponível em: https://exame.com/negocios/dino_old/trabalho-do-futuro-como-o-home-office-pode-transformar-uma-empresa/. Acesso em: 24 maio 2020.

HEIMERDINGER, Marcelo. **Empresas Comentam Sobre Futuro do Home Office Após Pandemia**. 2020. Disponível em: https://comoinvestir.thecap.com.br/empresas-cogitam-sobre-o-futuro-do-home-office-apos-pandemia/. Acesso em: 24 maio 2020.

INN, Office. **Os maiores problemas do home office**. 2017. Disponível em: https://officeinn.com.br/os-maiores-problemas-do-home-office/. Acesso em: 24 maio 2020.

PORTAL DRAFT. **O ‘home office’ vai ser o futuro do mercado de trabalho?**. 2020. Disponível em: https://www.napratica.org.br/o-home-office-vai-ser-o-futuro-do-mercado-de-trabalho/. Acesso em: 24 maio 2020.

SUTTO, Giovanna. **Home office permanente e escritório do futuro: a cara da volta ao trabalho pós-quarentena**. 2020. Disponível em: https://www.infomoney.com.br/negocios/home-office-permanente-e-escritorio-do-futuro-a-cara-da-volta-ao-trabalho-pos-quarentena/. Acesso em: 24 maio 2020.

</div>