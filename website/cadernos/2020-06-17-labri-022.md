---
slug: labri-n0022
tags: [COVID-19, CADERNOS]
title: "A Importância da Testagem em Massa em meio à Pandemia do Novo Coronavírus e uma Análise sobre a Eficiência dos Testes Empregados"
author: Bianca Morales, Gabriela Alencar, Pedro Henrique Moura e Rodrigo Toloi
author_title: Graduandos em Relações Internacionais
tipo_publicacao: Cadernos LabRI/UNESP - Núm. 22
image_url: https://i.imgur.com/JC37xbE.jpg
descricao: A crise na saúde pública, causada pela pandemia do novo coronavírus (COVID-19), tem como principais medidas de prevenção e controle o isolamento social, a constante higienização das mãos e uso de máscaras, além da testagem de possíveis infectados...
download_pdf: /arquivos/2020-06-17-labri-022.pdf
serie: COVID-19
---

<div align="center">

![](https://i.imgur.com/JC37xbE.jpg)

</div>

>*A crise na saúde pública, causada pela pandemia do novo coronavírus (COVID-19), tem como principais medidas de prevenção e controle o isolamento social, a constante higienização das mãos e uso de máscaras, além da testagem de possíveis infectados...*

<!--truncate-->

<div align="justify">

### Por que testar?

A crise na saúde pública, causada pela pandemia do novo coronavírus (COVID-19), tem como principais medidas de prevenção e controle o isolamento social, a constante higienização das mãos e uso de máscaras, além da testagem de possíveis infectados. Diferente das duas primeiras, os testes não são uma medida exclusivamente preventiva, embora sejam cruciais para o combate da epidemia. Segundo Sharfstein, Becker e Mello (2020), “O teste diagnóstico é crítico para uma resposta efetiva ao novo coronavírus”. Tal importância se dá porque, com a aplicação de testes generalizados, é possível o mapeamento e monitoramento da enfermidade no país, fatores fundamentais para os passos seguintes durante uma epidemia.


E, com a testagem em grande quantidade da população sendo aplicada efetivamente, é possível também progredir do processo de isolamento horizontal para o isolamento vertical, seletivo. Priorizando o isolamento de infectados e pessoas que tiveram contato com eles. Nesse caminho, gradualmente seria viável a retomada de atividades econômicas de maneira controlada e o início da recuperação financeira do país e população.

No contexto norte-americano, a grande diferença temporal que houve entre pessoas adentrando o país com o vírus e o início da testagem foi um dos principais motivos para tantos focos em diferentes lugares dos Estados Unidos. (SHARFSTEIN, BECKER, MELLO. p. 1 2020).

Entretanto, o aspecto global dessa crise criou, como conta Daniela Fernandes em entrevista com Antoine Bondaz (2020), uma forte competição entre os países para obter os insumos para os testes, além de ampliar a capacidade dos laboratórios que podem produzi-los, o que gera dois efeitos no sistema internacional: uma alta demanda desses equipamentos e conflitos comerciais com os seus principais produtores, em especial a China. E, por último, a pequena oferta obriga os países a decidirem quais serão as pessoas testadas, uma vez que há mais suspeitos do que testes disponíveis na maior parte do globo, causando, por sua vez, uma subnotificação e sub entendimento das situações em alguns países, como é o caso brasileiro, de acordo com o apontado por Nogueira et. al. (p. 3. 2020)



### Quais são e como funcionam os testes utilizados?

Existem, até o momento, dois principais tipos de testes para identificação do novo coronavírus. O primeiro deles foi o mais utilizado até março de 2020: o RT-PCR, cujo nome é uma sigla das palavras americanas utilizadas para designar testes de reação em cadeia da enzima polimerase com transcriptase reversa. Ele consiste na coleta de amostras de muco e saliva da garganta do possível contaminado com uma haste flexível própria para o exame. A partir disso, é possível decodificar o material genético (RNA) do vírus, esteja ele fragmentado ou não.

O teste rápido, sorológico, é bem mais simples. Com uma gota de sangue do paciente e alguns reagentes, é possível identificar a presença de anticorpos produzidos para defender o organismo do novo coronavírus (Sars-CoV-2) em até 15 minutos. Há, também, outros testes fisiológicos em desenvolvimento, além do mencionado. No Brasil, por exemplo, a Agência Nacional de Vigilância Sanitária (ANVISA) permitiu a triagem de três novos testes rápidos. (DINIZ et al., 2020).

É válido ressaltar, ainda, que testes relacionados à Inteligência Artificial (IA) estão sendo desenvolvidos.  Um exemplo é o computador Tianhe-1, no Centro Nacional de Supercomputadores na China, em Tianjin. Essa máquina atua, até o momento, em 30 hospitais chineses, e sua função é avaliar imagens do tórax de diferentes pacientes contaminados com o novo coronavírus. O resultado é obtido com 80% de eficiência em aproximadamente 10 segundos, contrapondo os 15 minutos de um teste rápido tradicional (WANG et al., 2020).



### Quais os benefícios do uso do sistema de drive-thru para a testagem em massa

Ao se tratar de testagem em massa, um método bastante eficaz e de fácil implementação é a realização de testes por meio do sistema de  _drive thru_. O processo consiste em armar estruturas, compostas por diversas tendas, em estacionamentos e atuar da mesma forma que  _fast-foods_, com os pacientes passando por uma instância diferente da triagem a cada tenda, sem sair de seus carros.

Dessa forma, a ocorrência de aglomerações de pessoas é extremamente reduzida, tanto de enfermeiros e médicos quanto de outros; propiciando, também, que se um indivíduo infectado pelo novo coronavírus vá realizar o teste por  _drive-thru_, o risco de transmissão do vírus para os demais seja mínimo, já que o contaminado estará em seu carro, e o único contato será com o profissional da saúde, devidamente equipado com aparatos de proteção. Uma situação muito diferente dos hospitais, onde o risco de contágio de pessoas saudáveis é recorrente devido à existência de intenso fluxo de indivíduos.

Com tal sistema é ainda possível realizar a testagem mais rapidamente, à medida que não há filas de espera como em hospitais e os formulários podem ser preenchidos virtualmente. E, com casos suspeitos do novo coronavírus sendo atendidos em  _drive-thrus_, os departamentos dos hospitais são capazes de operar melhor, com o foco em casos graves de diversas proveniências.



### Dificuldades da testagem em formato de drive-thru no Brasil 

Ainda que o sistema de testes em  _drive thru_  seja um sucesso em alguns países, o Brasil sente dificuldade em implementar o modelo ao nível nacional. Além da enorme desigualdade social do país, que impede uma grande parcela da população de ter um carro ou acesso a ele (especialmente considerando a necessidade de evitar o transporte público em tempos de pandemia), o teste rápido não é recomendado para casos leves, uma vez que pode falhar na identificação do contágio da doença. No início de março e abril de 2020, a ANVISA começou a analisar testes seguros para serem distribuídos no Brasil; segundo seu portal oficial, oito produtos já foram aprovados e outros estão em análise desde então. Porém, a demanda é maior do que os materiais disponíveis nos laboratórios nacionais e internacionais. Além disso, os testes moleculares aplicados no Brasil tem precisão de quase 100%, mas somente se o paciente estiver em um estado avançado da contaminação.



### Observações sobre a funcionalidade dos testes

A testagem em massa pode ser muito útil no combate à COVID-19. Porém, é necessário ter cautela, porque os testes não estão isentos de problemas. O teste já mencionado RT-PCR, por exemplo, é considerado o mais eficiente, pois consegue identificar a presença do RNA do vírus pouco tempo após a exposição do paciente a ele (cerca de dois ou três dias).

Contudo, em comparação com o sorológico, é mais demorado e complexo, visto que depende de tecnologias avançadas e profissionais qualificados para ser realizado de forma correta. A partir disso, é possível concluir que o RT-PCR seria inviável para uma testagem em grande escala da população devido ao tempo para obtenção de seus resultados não compactuar com a agilidade necessária dentro deste cenário. Ademais, ele é incapaz de distinguir indivíduos que foram contaminados pelo novo coronavírus em algum momento - e, portanto, imunizados - dos ainda vulneráveis.

O teste rápido (sorológico), menos refinado, é ideal para uma testagem em formato  _drive-thru_, por não necessitar de profissionais tão instruídos quanto o RT-PCR, nem equipamentos elaborados, além, é claro, de fornecer resultados em um intervalo de tempo significativamente menor, importante para o cenário atual, como já citado. Entretanto, existe uma pequena falha em sua eficiência: ele não pode detectar pacientes contaminados pela COVID-19 se eles forem do tipo assintomático. Também há um relativo consenso científico de que a detecção irá ocorrer de forma mais efetiva apenas a partir do sétimo dias após a manifestação do primeiro sintoma. Tais informações são muito relevantes, uma vez que esse detalhe pode causar falhas na coleta de dados sobre infectados, além de permitir o trânsito destes indivíduos contaminados sem que eles possuam consciência de sua condição, podendo prejudicar vidas - essencialmente as de integrantes dos grupos de risco.



### A testagem em massa no Estado de São Paulo

Entre os dias 15 e 29 de maio de 2020, o estado de São Paulo iniciou um projeto de testagem em massa que será dividido em fases, com a aplicação sendo feita por professores e alunos de enfermagem. O foco da primeira fase de testagens será em policiais militares, civis e técnico-científicos, e suas respectivas famílias - todos residentes ou atuantes na capital -, profissionais das áreas de saúde e de segurança pública, a população privada de liberdade, os doadores de sangue e pessoas que vivem em asilos e casas de repouso. Na segunda fase de testagens serão avaliados os parentes de pessoas já internadas com o vírus, e na terceira, os assintomáticos.

O Hospital Albert Einstein desenvolveu um exame em larga escala e com alta precisão para detectar o novo coronavírus, que começará a ser aplicado em junho deste ano. A técnica levou cerca de dois meses para ficar pronta e foi patenteada pelo Sistema Internacional de Patentes nos Estados Unidos. Diferente dos exames sorológicos disponíveis atualmente, o novo teste promete identificar o vírus desde o primeiro dia de infecção, sem resultados falso-negativos.

“O feito da equipe do Einstein, executado em tão pouco tempo, é resultado do grande investimento da organização em suas áreas de pesquisa, inovação e empreendedorismo”, afirmou o engenheiro Claudio Terra, em matéria para o G1. Já que o caminho para obter sucesso na contenção do vírus tem início em sua identificação e testagem, segundo a brasileira Ana Paula Coutinho-Reyse, que lidera a área de prevenção e controle de infecções do programa de emergências da Organização Mundial da Saúde (OMS) na Europa, o progresso do novo teste se torna animador para os cidadãos de São Paulo.



### Considerações Finais 

A produção de testes para o novo coronavírus é uma tarefa árdua, que envolve diferentes atores do cenário internacional, devido à interdependência entre eles. Assim, existe uma certa dificuldade na obtenção dos materiais necessário para a produção dos testes, e isso se dá porque somente alguns países os produzem. Conforme apontado em matéria do jornal El País, o mercado internacional dos reagentes utilizados no teste RT-PCR (o mais utilizado) está saturado, dificultando a compra e prejudicando as nações mais afetadas pelo novo coronavírus, especialmente as latino-americanas, com grande número de infectados e testagem escassa.

Além disso, as diferentes marcas de teste exigem uma mistura única de reagentes químicos específicos em seus testes. Ou seja, um teste da marca imaginária “X” possui uma mistura divergente e incompatível com a utilizada pela marca “Y”, tornando o processo ainda mais complicado.

Todavia, apesar das imperfeições dos testes e dos cuidados a serem tomados, a testagem em massa permanece uma arma respeitável e essencial no combate à COVID-19, sobretudo se alinhada com outras políticas, como o isolamento social. Uma prova disso é o fato de os países que adotaram esse sistema terem sido frequentemente indicados como exemplo no combate à pandemia. É o caso da Coreia do Sul, que efetuou uma testagem generalizada de sua população, iniciada precocemente (fator importante para garantir o isolamento dos contaminados, impedindo maior difusão do vírus) em clínicas, hospitais e também em sistemas de drive-thru, além da contribuição da população para com o distanciamento social.



<article></article>

### Referências Bibliográficas

Agência Brasil com Da redação. **SP anuncia teste em massa para pacientes com sintomas leves da covid-19**. Disponível em: https://exame.com/brasil/sp-anuncia-teste-em-massa-para-pacientes-com-sintomas-leves-da-covid-19/. Acesso em: 22 maio 2020.

ANVISA/ Ascom. **Aprovados primeiros testes rápidos para Covid-19**. Disponível em: http://portal.anvisa.gov.br/noticias/-/asset_publisher/FXrpx9qY7FbU/content/aprovados-primeiros-testes-rapidos-para-covid-19/219201. Acesso em: 01 de junho de 2020.

BBC. **Coronavírus: por que o Brasil ainda não conseguiu fazer testes em massa?**. G1. Disponível em: https://g1.globo.com/bemestar/coronavirus/noticia/2020/04/03/coronavirus-por-que-o-brasil-ainda-nao-conseguiu-fazer-testes-em-massa.ghtml. Acesso dia 21 de maio de 2020.

BINNICKER, Matthew J. **Emergence of a novel coronavirus disease (covid-19) and the importance of diagnostic testing: Why partnership between clinical laboratories, public health agencies, and industry is essential to control the outbreak**. Clinical Chemistry, v. 66, n. 5, p. 664-666, 2020. Disponível em: https://academic.oup.com/clinchem/article-abstract/66/5/664/5741389. Acesso em: 21 de maio 2020.

BORGES, Beatriz. **Hospital Albert Einstein desenvolve teste para o coronavírus que une alta precisão e detecção em larga escala**. Disponível em: https://g1.globo.com/sp/sao-paulo/noticia/2020/05/21/hospital-albert-einstein-desenvolve-teste-para-o-coronavirus-que-une-alta-precisao-e-deteccao-em-larga-escala.ghtml. Acesso dia 22 de maio de 2020

CAMPBELL, Sheldon; EPSTEIN, Randi Hutter. **Why are coronavirus tests so difficult to produce?**. BBC, 22 abr. 2020. Disponível em: https://www.bbc.com/future/article/20200422-why-are-coronavirus-tests-so-difficult-to-produce. Acesso em: 18 maio 2020.

DA COVID, Força-Tarefa de Modelagem. **Uma Alternativa para o Aumento da Escala da Testagem para a Covid-19**. Disponível em: https://ufmg.br/storage/0/9/0/e/090e386563b14bec680ca07f7c9ff5c4_15892829037837_827145161.pdf.

DINIZ, Michele Correia, et al. **Crise Global Coronavírus: monitoramento e impactos**. Cadernos de Prospecção, 2020. 8 abr. 2020. 13.2 COVID-19: 359. Disponível em: https://cienciasmedicasbiologicas.ufba.br/index.php/nit/article/view/35937

ELER, Guilherme. **Quais as vantagens (e desvantagens) dos testes rápidos para Covid-19**. Superinteressante. Disponível em: https://super.abril.com.br/saude/quais-as-vantagens-e-desvantagens-dos-testes-rapidos-para-covid-19/. Acesso em 21 de maio de 2020.

FERNANDES, Daniela. **Covid-19 expõe dependência de itens de saúde fabricados na China**. BBC News Brasil. Paris. 10 de maio de 2020. Disponível em <https://www.bbc.com/portuguese/internacional-52465757>

GALLAGHER, James. **Imunidade e coronavírus: é possível pegar covid-19 mais de uma vez? Esta e outras questões ainda sem resposta**. BBC. 29 abr. 2020. Disponível em: https://www.bbc.com/portuguese/internacional-52462544. Acesso em: 21 maio 2020.

GALINDO, Jorge. **Faltam testes para mensurar o coronavírus na América Latina**. El País, 20 abr 2020. Disponível em: https://brasil.elpais.com/sociedade/2020-04-20/faltam-testes-para-mensurar-o-coronavirus-na-america-latina.html.

MAIA, Gabriel; GOMES, Lucas. **Quais são os testes para detectar o coronavírus e como funcionam**. Nexo Jornal, 8 abr 2020. Disponível em: https://www.nexojornal.com.br/grafico/2020/04/08/Quais-s%C3%A3o-os-testes-para-detectar-o-coronav%C3%ADrus-e-como-funcionam. Acesso em: 12 maio 2020.

MALLAPATY, Smriti. **Will antibody tests for the coronavirus really change everything?**. Nature. 18 abr. 2020. Disponível em: https://www.nature.com/articles/d41586-020-01115-z. Acesso em: 21 maio 2020.

Nogueira, A. L., Nogueira, C. L., Zibetti, A. W., Roqueiro, N., Bruna-Romero, O., & Carciofi, B. A. **ESTIMATIVA DA SUBNOTIFICAÇÃO DE CASOS DA COVID-19 NO ESTADO DE SANTA CATARINA**. Disponível em: https://noticias.paginas.ufsc.br/files/2020/05/aqui.pdf. Acesso em: 21 maio 2020.

SHARFSTEIN, Joshua M.; BECKER, Scott J.; MELLO, Michelle M. **Diagnostic testing for the novel coronavirus**. Jama, v. 323, n. 15, p. 1437-1438, 2020. Disponível em: https://jamanetwork.com/journals/jama/article-abstract/2762951. Acesso em: 21 maio 2020.
</div>