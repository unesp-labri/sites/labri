import React from "react";
import useBaseUrl from "@docusaurus/useBaseUrl";
import clsx from "clsx";
import styles from "./styles.module.scss";

export function Sobre({
  title,
  logo,
  imgLogo,
  logo2,
  imgLogo2,
  imageUrl,
  description,
  description2,
  link,
}) {
  const imagemInfo = useBaseUrl(imageUrl);
  return (
    <div className={clsx(styles.Infos, "cardProjects")}>
      <div className="card-container">
        <div>
          <img src={imgLogo} alt={logo} width="180" height="110" />
        </div>
        <div className="card__body">
          <h4 className={clsx(styles.h4Projetos)}>{description}</h4>
        </div>
        <div className="card__footer">
          <a className="button button--secondary" href={link}>
            <b>Acesse aqui</b>
          </a>
        </div>
      </div>
    </div>
  );
}
