import React from "react";
import clsx from "clsx";
import styles from "./styles.module.scss";
import Link from "@docusaurus/Link";

// conteúdo carousel separado por número de colunas

export function SectionSlide({
  columns,
  heroTitle,
  heroSubtitle,
  linkText,
  linkHref,
  imgHref,
  imgSrc,
}) {
  if (columns == 1) {
    // conteúdo primeiro slide
    return (
      <header
        className={clsx(styles.heroBanner, styles.slide)}
        style={{
          backgroundImage:
            "radial-gradient(circle, #3f536c, #475e7c, #50698b, #59759c, #6280ac, #6d8bb7, #7996c1, #84a1cc, #95aed3, #a6bcd9, #b8cae0, #cad7e6)",
        }}
      >
        <div className="container">
          <h1 className={clsx(styles.h1HeroTitle, "hero__title")}>
            {heroTitle}
          </h1>
          <p className={clsx(styles.subtitle, "hero__subtitle")}>
            {heroSubtitle}
          </p>
          <Link
            className={clsx(
              "button button--outline button--secondary button-lg"
            )}
            style={{ color: "white", align: "left" }}
            to={linkHref}
            targetblank="_"
          >
            {linkText}
          </Link>
        </div>
      </header>
    );
  } else {
    // conteúdo dos slides restantes (texto + imagem)
    return (
      <div
        className={clsx(styles.heroBanner, styles.slide)}
        style={{
          backgroundImage:
            "linear-gradient(to top, #3f536c, #475e7c, #50698b, #59759c, #6280ac, #6d8bb7, #7996c1, #84a1cc, #95aed3, #a6bcd9, #b8cae0, #cad7e6)",
        }}
      >
        <div className="container">
          <div className="row">
            <div className="col col--6">
              <p
                className={clsx(
                  styles.subtitle,
                  styles.subtitleCarousel,
                  "hero__subtitle"
                )}
              >
                {heroSubtitle}
              </p>
              <div>
                <Link
                  className={clsx(
                    "button button--outline button--secondary button-lg"
                  )}
                  style={{ color: "white", align: "left" }}
                  to={linkHref}
                  target="_blank"
                >
                  {linkText}
                </Link>
              </div>
            </div>
            <div className="col col--6">
              <Link to={imgHref} target="_blank">
                <img src={imgSrc} className={clsx(styles.bkgimg)} />
              </Link>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
