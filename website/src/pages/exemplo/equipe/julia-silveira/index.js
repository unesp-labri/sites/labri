import React from "react";
import clsx from "clsx";
import Layout from "@theme/Layout";
import styles from "./styles.julia.module.css";
import stylesTitle from "./styles.title.module.css"
import useBaseUrl from "@docusaurus/useBaseUrl";
import Slider from "react-slick";
import 'slick-carousel/slick/slick.css';

const sobre = [ 
  {
    projeto: "equipe - labri-unesp",
    title: (
      <>
      <span className={clsx(styles.highlight)}>Quem Sou</span>
      </>
    ),
    imgFoto: "/img/membros-pag/pag-julia.svg",
    foto: "foto do membro da equipe",
    text: (
      <>
      <b>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus dictum dignissim feugiat. Curabitur sem ipsum, iaculis a sollicitudin nec, aliquam sed dolor. Cras quis turpis erat. Phasellus pulvinar auctor convallis. Vestibulum vulputate ligula eget erat faucibus egestas. Aenean massa turpis, varius vel massa sit amet, sagittis viverra ligula. Morbi id pretium lorem.</b>
      </>
    )
  }
]  

const estudos = [
  {
    projeto: "equipe - labri-unesp",
    title: (
      <>
      <span className={clsx(styles.highlight)}>Formação Acadêmica</span>
      </>),
    text1: (
      <>
      <ul><center><b>Graduanda em Relações Internacionais</b></center></ul>
      <ul><center><b>Área de interesse dentro das Relações Internacionais:</b></center></ul>
      <ul><li>Mecanismos de Integração Regional na América Látina</li></ul>
      <ul><li>Educação no Processo de Integração Regional</li></ul>
      <ul><li>União de Nações Sul-Americanas (UNASUL)</li></ul>
      <ul><center><b>Línguas:</b></center></ul>
        <ul><ul><li><b>Português:</b> Fluente</li></ul></ul>
        <ul><ul><li><b>Inglês:</b> Avançado</li></ul></ul>
        <ul><ul><li><b>Espanhol:</b> Intermediário</li></ul></ul>
        <ul><ul><li><b>Alemão:</b> Básico</li></ul></ul>
        <ul><ul><li><b>Coreano:</b> Básico</li></ul></ul>
      </>
    ),
    imgFoto: "/img/membros-pag/estudos-julia.svg",
    text2: (
      <>
      <ul><center><i>Principal área de interesse: </i><b><span className={clsx(styles.highlightB)}>Desenvolvimento de Front-End e Design Gráfico</span></b></center></ul>
      <ul><li>HTML5</li></ul>
      <ul><li>CSS3</li></ul>
      <ul><li>JavaScript</li></ul>
      <ul><li>Adobe Photoshop</li></ul>
      </>
    )
  }
]

const conhecimentos = [
  {
    projeto: "equipe - labri-unesp",
    title: (
      <> 
      <span className={styles.highlight}>Conhecimentos e Interesses</span>
      </>),
    subtitle: (
      <>
      <center>Breve descrição dos seus principais intereses em programação e/ou Relações Internacionais. Curabitur sem ipsum, iaculis a sollicitudin nec, aliquam sed dolor. Cras quis turpis erat. Phasellus pulvinar auctor convallis. Vestibulum vulputate ligula eget erat faucibus egestas.</center>
      </>
    )
  }
]

const interesses = [
  {
    projeto: "equipe- labri-unesp",
    imgFoto: "/img/membros-pag/portfolio-09.svg",
    text: (
      <>
      <ul><h2><span className={clsx(styles.highlightCirculo)}>Lógica da programação</span></h2></ul>
      <ul>Breve introdução do conhecimento e da experiência da pessoa com relação aos conhecimentos apontados acima (Ex: explicar os principais conhecimentos de Python e há quanto tempo utiliza essa linguagem)</ul>
      </>
    ),
    text2: (
      <>
      <ul><h2>Linguagem Python</h2></ul>
      <ul>Breve introdução do conhecimento e da experiência da pessoa com relação aos conhecimentos apontados acima (Ex: explicar os principais conhecimentos de Python e há quanto tempo utiliza essa linguagem)</ul>
      </>
    ),
    imgFoto2: "/img/membros-pag/python.svg"
  },
  {
    projeto: "equipe- labri-unesp",
    imgFoto: "/img/membros-pag/portfolio-10.svg",
    text: (
      <>
      <ul><h2>SQL</h2></ul>
      <ul>Breve introdução do conhecimento e da experiência da pessoa com relação aos conhecimentos apontados acima (Ex: explicar os principais conhecimentos de Python e há quanto tempo utiliza essa linguagem)</ul>
      </>
    ),
    text2: (
      <>
      <ul><h2>Adobe Photoshop</h2></ul>
      <ul>Breve introdução do conhecimento e da experiência da pessoa com relação aos conhecimentos apontados acima (Ex: explicar os principais conhecimentos de Python e há quanto tempo utiliza essa linguagem)</ul>
      </>
    ),
    imgFoto2: "/img/membros-pag/portfolio-12.svg"
  },
  {
    projeto: "equipe- labri-unesp",
    imgFoto: "/img/membros-pag/portfolio-09.svg",
    text: (
      <>
      <ul><h2>CSS3 E HTML5</h2></ul>
      <ul>Breve introdução do conhecimento e da experiência da pessoa com relação aos conhecimentos apontados acima (Ex: explicar os principais conhecimentos de Python e há quanto tempo utiliza essa linguagem)</ul>
      </>
    ),
    text2: (
      <>
      <ul><h2>JavaScript</h2></ul>
      <ul>Breve introdução do conhecimento e da experiência da pessoa com relação aos conhecimentos apontados acima (Ex: explicar os principais conhecimentos de Python e há quanto tempo utiliza essa linguagem)</ul>
      </>
    ),
    imgFoto2: "/img/membros-pag/javascript.svg"
  },
]

const footer = [
  {
    projeto: "equipe - labri-unesp",
    text: (
      <>
      <span className={clsx(styles.highlight)}>ONDE ME ENCONTRAR</span>
      </>
    ),
    imgLogo: "/img/membros-pag/portfolio-04.svg",
    imgLogo2: "/img/membros-pag/portfolio-02.svg",
    imgLogo3: "/img/membros-pag/portfolio-03.svg",
    imgFoto: "/img/membros-pag/portfolio-01.svg",
    title: (
      <>
      <span className={clsx(styles.highlight)}>ENTRE EM CONTATO</span>
      </>
    ),
    text2: (
      <>
      <ul><u>julia.s.silveira@unesp.br</u> ou <u>juliasantossilveira@hotmail.com</u></ul>
      </>
    )
  }
]


function Footer({text, logo, imgLogo, logo2, imgLogo2, logo3, imgLogo3, foto, imgFoto, text2, title}){
  return(
    <div>
      <div className={clsx(styles.fundo, "container")}>
        <div className="row">
          <div className="col col--4">
            <h2 className={clsx(styles.tFooter)}><b>{text}</b></h2>
            <img className={clsx(styles.Logos)} src={imgLogo} alt={logo} />
            <img className={clsx(styles.Logos2)} src={imgLogo2} alt={logo2} />
            <img className={clsx(styles.Logos2)} src={imgLogo3} alt={logo3} />
          </div>
          <div className="col col--4">
            <img src={imgFoto} alt={foto} />
          </div>
          <div className="col col--4">
            <h2 className={clsx(styles.tFooter)}><b>{title}</b></h2>
            <p className={clsx(styles.pFooter)}>{text2}</p>
          </div>
        </div>
      </div>
    </div>
  )
}

function Interesses({foto, imgFoto, foto2, imgFoto2, text, text2}){
  return(
    <div>
      <div className="container">
        <div className="row">
          <div className="col col--4">
            <img src={imgFoto} alt={foto} />
          </div>
          <div className="col col--8">
            <p className={clsx(styles.tInteresses)}>{text}</p>
          </div>
            <div className="col col--8">
              <p className={clsx(styles.tInteresses)}>{text2}</p>
            </div>
            <div className="col col--4">
              <img src={imgFoto2} alt={foto2} />
            </div>
        </div>
      </div>
    </div>
  )
}

function Conhecimentos({title, subtitle}){
  return(
    <div>
      <div className={clsx(styles.titleConhecimentos)}>
            {title}
      </div>
      <div className={clsx(styles.subConhecimentos)}>
            {subtitle}
        </div>
    </div>
  )
}

function Estudos({title, foto, imgFoto, text1, text2}){
  return(
    <div>
      <div className="container">
        <div className="row">
          <div className="col col--6">
            <h2 className={clsx(styles.tEstudos)}>{title}</h2>
            <p className={clsx(styles.pEstudos)}>{text1}</p>
          </div>
          <div className="col col--6">
            <img src={imgFoto} alt={foto} />
            <p className={clsx(styles.pEstudos)}>{text2}</p>
          </div>
        </div>
      </div>
    </div>
  )
}

function Sobre({foto, imgFoto, title, text, imageUrl}){
  const ImagemInfo = useBaseUrl(imageUrl);
  return(
    <div>
    <div className="container"                                                        >
      <div className="row">
        <div className="col col--6">
          <img src={imgFoto} alt={foto}></img>
        </div>
        <div className="col col--6">
          <h2 className={clsx(styles.tSobreEquipe)}><b>{title}</b></h2>
          <p className={clsx(styles.pSobreEquipe)}>{text}</p>
        </div>
      </div>
    </div>
    </div>
  )
}

function Equipe(){
    return(
        <Layout title="Júlia dos Santos Silveira">
        <header className={clsx('hero hero--primary', styles.heroBanner)}>
          <div className={styles.container}>
            <h1 className={clsx(styles.hero__title)}><span className={clsx(stylesTitle.span)}>JÚLIA DOS SANTOS SILVEIRA</span></h1>
            <h2 className={clsx(styles.hero__subtitle)}>Estagiária Voluntária</h2>
          </div>
        </header>
        <main className={clsx(styles.pagCor)}>
          <section className="content">
          <div className="container">
            <div className="row">
              {sobre.map((props, idx) => (
                <Sobre key={idx} {...props} />
              ))}
            </div>
          </div>

          <div className="container">
            <div className="row">
              {estudos.map((props, idx) => (
                <Estudos key={idx} {...props} />
              ))}
            </div>
          </div>

          <div className="container">
            <div className="row">
              {conhecimentos.map((props, idx) => (
                <Conhecimentos key={idx} {...props} />
              ))}
            </div>
          </div>

          <div className="container">
            <div className="row">
              {interesses.map((props, idx) => (
                <Interesses key={idx} {...props} />
              ))}
            </div>
          </div>

          <div className="container">
            <div className="row">
              {footer.map((props, idx) => (
                <Footer key={idx} {...props} />
              ))}
            </div>
          </div>
          </section>
        </main>

        </Layout>
    )
}

export default Equipe;
