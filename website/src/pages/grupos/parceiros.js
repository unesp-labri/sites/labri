import React from "react";
import Layout from "@theme/Layout";
import styles from "../styles.module.scss";
import clsx from "clsx";
import useBaseUrl from "@docusaurus/useBaseUrl";

const projetos = [
  {
    title: "Conhecer Para Acolher",
    imageUrl: "/img/conhecer-para-acolher/logo-cpa.svg",
    description: (
      <>
        Mapeamento de migrantes e refugiados na cidade de Franca. Projeto
        vinculado à REDE TEMÁTICA DE EXTENSÃO: Rede de Atenção ao Migrante
        Internacional (RAMIN/UNESP). Programa Institucional da Pró-Reitoria de
        Extensão Universitária e Cultura (PROEC).
      </>
    ),
    website: "https://labriunesp.org/conhecer-para-acolher",
  },
  {
    title: "POD-RI",
    imageUrl: "/img/pod-ri/logo-pod-ri.svg",
    description: (
      <>
        O Pod-RI é um projeto feito por estudantes, no formato de podcast,
        iniciado como uma ideia entre amigos. Gravamos episódios com diversas
        temáticas das Relações Internacionais, assim como conversas com
        especialistas e profissionais. Além disso, produzimos conteúdos para
        redes sociais, onde entramos em contato mais direto com a comunidade de
        Relações Internacionais do Brasil.
      </>
    ),
    website: "https://labriunesp.org/pod-ri",
  },
  {
    title:
      "Grupo de Estudo em Política e Direito Ambiental Internacional (GEPDAI)",
    imageUrl: "/img/gepdai/arte-oficial-site.png",
    description: (
      <>
        O Grupo de Estudo em Política e Direito Ambiental Internacional (GEPDAI)
        se dedica a analisar e compreender os temas da Política e do Direito
        Ambiental em uma perspectiva das Relações Internacionais. O GEPDAI
        promove reuniões quinzenais que trazem as discussões de maneira
        horizontal entre os membros e docentes participantes resultando em
        reflexões e produções a respeito da dicotomia Sociedade - Meio Ambiente,
        tema de alta relevância no contexto global atual.
      </>
    ),
    website: "https://labriunesp.org/gepdai",
  },
  {
    title: "Cidades Saudáveis e Sustentáveis",
    imageUrl: "/img/cidades-sustentaveis/logo-ver1.svg",
    description: (
      <>
        O Projeto de Extensão Cidades Saudáveis e Sustentáveis foi criado em
        2020, vinculado ao Centro Jurídico Social da Universidade Estadual
        Paulista, campus de Franca/SP. Assim, com o objetivo de difundir o
        conhecimento sobre essa temática, fortalecer a interlocução da
        universidade e a sociedade, juntamente com a finalidade de explorar o
        campo da inovação e das políticas públicas dos entes federativos , o
        grupo de extensão é composto por 16 discentes, entre eles graduandos e
        pós graduandos em Direito, Relações Internacionais e Arquitetura e
        Urbanismo, sob coordenação do Prof. Dr. Daniel Damásio Borges.
      </>
    ),
    website: "https://labriunesp.org/cidades-sustentaveis",
  },
  {
    title: "Development, Internacional Politics and Peace (DIPP)",
    imageUrl: "/img/dipp/logo-dipp.svg",
    description: (
      <>
        A Rede DIPP – Development, International Politics and Peace – se baseia
        na construção de uma rede que seja geograficamente ampliada
        (inicialmente em três continentes, mas visando ampliar para a Ásia
        futuramente) e capaz de oferecer abordagens interdisciplinares sobre
        comportamentos políticos e institucionalidade no âmbito das relações
        internacionais. Foi instaurada a partir do edital UNESP/Capes-Print
      </>
    ),
    website: "https://labriunesp.org/dipp",
  },
  {
    title: "Rede de Pesquisa em Política Externa e Regionalismo (REPRI)",
    imageUrl: "/img/repri/repri-logo.png",
    description: (
      <>
        A Rede de Pesquisa em Política Externa e Regionalismo é formada por
        pesquisadores, professores e alunos de pós-graduação e graduação de
        diferentes Instituições de Ensino Superior e de Pesquisa, como a UFU,
        UNIFESP, UNESP, UNB, UFGD, IPEA. Os estudos e pesquisas realizados pelos
        participantes buscam, a partir de objetos e perspectivas teóricas e
        metodológicas diversas, compreender as motivações e os determinantes da
        política externa dos Estados e dos processos de regionalismo e
        multilateralismo.
      </>
    ),
    website: "https://labriunesp.org/repri",
  },
  {
    title:
      "Laboratório de Novas Tecnologias de Pesquisa em Relações Internacionais (LANTRI)",
    imageUrl: "/img/lantri/logo.svg",
    description: (
      <>
        O LANTRI busca integrar a utilização sistemática de Novas Tecnologias de
        Informação e Comunicação (TICs) com as pesquisas acadêmicas em Relações
        Internacionais, privilegiando ferramentas de pesquisa Livres (Free
        Software) ou de Código Aberto (Open Source). Esta integração é realizada
        através do desenvolvimento de pesquisas que tem como foco central as
        Relações Internacionais do Brasil ou apresentem temáticas com impactos
        significativos sobre o Brasil.
      </>
    ),
    website: "https://labriunesp.org/lantri",
  },
];

function Projeto({ imageUrl, title, description, website }) {
  const imgUrl = useBaseUrl(imageUrl);
  return (
    <div className={styles.Projeto}>
      <div className={styles.imagemProjeto}>
        {imgUrl && (
          <img className={styles.featureImage} src={imgUrl} alt={title} />
        )}
      </div>
      <div className={styles.projetoTexto}>
        <h3>{title}</h3>
        <p>
          {" "}
          <b> Descrição: </b> {description}
        </p>
        <br></br>
        <a class="button button--secondary" href={website}>
          Saiba Mais
        </a>
      </div>
    </div>
  );
}

function Pesquisa() {
  return (
    <Layout title="Grupos Parceiros">
      <header className={clsx("hero hero--primary", styles.heroBanner)}>
        <div className={styles.container}>
          <h1 className={styles.hero__title}>Grupos Parceiros</h1>
          <h2 className={styles.subtituloProjetos}>
            Conheça melhor os grupos parceiros do LabRI/UNESP
          </h2>
        </div>
      </header>
      <main className={styles.main}>
        <section className={styles.content}>
          {projetos && projetos.length > 0 && (
            <section className={styles.projetos}>
              <div className={styles.container}>
                {projetos.map((props, idx) => (
                  <Projeto key={idx} {...props} />
                ))}
              </div>
            </section>
          )}
        </section>
      </main>
    </Layout>
  );
}

export default Pesquisa;
