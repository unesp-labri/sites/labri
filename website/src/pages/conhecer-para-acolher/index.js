import React from "react";
import clsx from "clsx";
import Layout from "@theme/Layout";
import styles from "./styles.module.css";


const intro = [
    {
        projeto: "cpa - labri-unesp",
        imgIntro: "/img/conhecer-para-acolher/header-cpa.png"
    }
]
 
const menu = [
    {
        projeto: "cpa - labri-unesp",
        link1: "/conhecer-para-acolher",
        link2: "/conhecer-para-acolher/sobre",
        link3: "/conhecer-para-acolher/objetivos",
        link4: "/conhecer-para-acolher/equipe",
        link5: "/conhecer-para-acolher/contato",
    }
]

const primeiracoluna = [
    {
        projeto: "cpa - labri-unesp",
        titulo: "Conhecer Para Acolher",
        subtitulo: "Mapeamento de migrantes e refugiados na cidade de Franca",
        descricao: (
            <>
                    Projeto vinculado à REDE TEMÁTICA DE EXTENSÃO: Rede de Atenção ao Migrante Internacional
                    (RAMIN/UNESP). <br />
                    Programa Institucional da Pró-Reitoria de Extensão Universitária e Cultura (PROEC).
            </>
        ),
        logo: "/img/conhecer-para-acolher/logo-cpa.svg"
    }
]

const segundacoluna = [
    {
        projeto: "cpa - labri-unesp",
        titulo: "SOBRE",
        texto: "Conheça nosso projeto",
        imgFoto: "/img/conhecer-para-acolher/sobre.svg",
        link: "/conhecer-para-acolher/sobre"
    },
    {
        projeto: "cpa - labri-unesp",
        titulo: "OBJETIVOS",
        texto: "Saiba mais sobre os nossos objetivos",
        imgFoto: "/img/conhecer-para-acolher/objetivos.svg",
        link: "/conhecer-para-acolher/objetivos"
    },
    {
        projeto: "cpa - labri-unesp",
        titulo: "EQUIPE",
        texto: "Conheça as pessoas por traz do projeto",
        imgFoto: "/img/conhecer-para-acolher/equipe.svg",
        link: "/conhecer-para-acolher/equipe"
    },
    {
        projeto: "cpa - labri-unesp",
        titulo: "CONTATO",
        texto: "Saiba como entrar em contato",
        imgFoto: "/img/conhecer-para-acolher/contato.svg",
        link: "/conhecer-para-acolher/contato"
    },
]

function SegundaColuna({titulo, texto, imgFoto, link}){
    return(
      <div className={clsx(styles.containerCards, "col col--3")}>
        <div className={clsx(styles.Cards)}>
            <h2><a href={link} className={clsx(styles.h2)}>{titulo}</a></h2>
            <a href={link}><img src={imgFoto} alt="Logo" className={clsx(styles.SobreCards)} /></a>
            <a href={link}><p className={clsx(styles.descricao)}>{texto}</p></a>
        </div>
      </div>
    ) 
  }

function PrimeiraColuna({ titulo, subtitulo, descricao, logo }) {
    return (
        <div className="container">
            <div className={clsx(styles.row, "row")}>
                <div className="col col--8">
                    <h2 className={clsx(styles.h2CPA)}>{titulo}</h2>
                    <h4 className={clsx(styles.h4CPA)}>{subtitulo}</h4>
                    <p className={clsx(styles.paragrafo)}>{descricao}</p>
                </div>
                <div className={clsx(styles.logoCPA, "col col--4")}>
                    <img src={logo} alt="Logo do Conhecer Para Acolher" />
                </div>
            </div>
        </div>
    )
}

function Menu({ link1, link2, link3, link4, link5 }) {
    return (
        <div className={clsx(styles.Menu)}>
            <ul className={clsx(styles.ulLista)}>
                <li className={clsx(styles.liLista)}><a href={link1}>INICIO</a></li>
                <li className={clsx(styles.liLista)}><a href={link2}>SOBRE</a></li>
                <li className={clsx(styles.liLista)}><a href={link3}>OBJETIVOS</a></li>
                <li className={clsx(styles.liLista)}><a href={link4}>EQUIPE</a></li>
                <li className={clsx(styles.liLista)}><a href={link5}>CONTATO</a></li>
            </ul>
        </div>
    )
}

function Intro({ projeto, imgIntro }) {
    return (
        <div className={clsx(styles.heroBanner)}>
            <div className={styles.HeaderCPA}>
                <img src={imgIntro} alt="Header e Logo do Projeto Conhecer Para Acolher" />
            </div>
        </div>
    );
}

function Home() {
    return (
        <Layout title="Conhecer Para Acolher">
            <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
            <script src="js/bootstrap.min.js"></script>

            <header className={clsx(styles.heroBanner)}>
                <div>
                    {intro.map((props, idx) => (
                        <Intro key={idx} {...props} />
                    ))}
                </div>
            </header>

            <main className={clsx(styles.main)}>
                <section className={styles.content}>
                    <div className={clsx(styles.container)}>
                        <div className={clsx(styles.row, "row")}>
                            {menu.map((props, idx) => (
                                <Menu key={idx} {...props} />
                            ))}
                        </div>
                    </div>

                    <div className={clsx(styles.container)}>
                        <div className={clsx(styles.row, "row")}>
                            {primeiracoluna.map((props, idx) => (
                                <PrimeiraColuna key={idx} {...props} />
                            ))}
                        </div>
                    </div>

                    <div className={clsx(styles.container)}>
                        <div className={clsx(styles.row, "row")}>
                            {segundacoluna.map((props, idx) => (
                                <SegundaColuna key={idx} {...props} />
                            ))}
                        </div>
                    </div>
                </section>
            </main>
        </Layout>
    )
}



export default Home;