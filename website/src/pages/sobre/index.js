import React from "react";
import clsx from "clsx";
import Layout from "@theme/Layout";
import styles from "./styles.module.scss";

const sobre = {
  quem_somos: (
    <>
      <p>
        O Laboratório de Relações Internacionais da Universidade Estadual
        Paulista (LabRI/UNESP), criado em 2011, faz parte da estrutura do
        departamento de Relações Internacionais (DERI) da UNESP, campus Franca.
      </p>
      <p>
        Podemos pensar o LabRI/UNESP como um Hub Acadêmico. O termo Hub é
        comumente utilizado na área de rede de computadores para se referir a um
        equipamento que interliga diferentes computadores, possibilitando com
        que os mesmos troquem informações.
      </p>
      <p>
        Nos últimos anos, esse termo passou a ser utilizado por outras áreas
        além da computação para se referir a um local físico ou virtual onde
        indivíduos e grupos podem compartilhar experiências e usufruir de um
        espaço de infraestrutura comum. Termos como Hub digital ou Hub de
        inovação tem sido utilizados para se referir a iniciativas voltadas a
        estimular e fornecer apoio físico ou virtual a pequenos empreendimentos
        e a startups.
      </p>
      <p>
        O LabRI/UNESP não é um grupo de pesquisa. A partir da breve descrição
        indicada acima, pode-se considerá-lo um Hub acadêmico, pois fornece um
        espaço e infraestrutura física e virtual para que os docentes, discentes
        e grupos de pesquisa a ele vinculados possam desenvolver as suas
        atividades de pesquisa e ensino de extensão, especialmente se tais
        atividades demandam a utilização de tecnologias digitais e o suporte a
        partir de uma infraestrutura computacional.
      </p>
      <p>
        Projetos e iniciativas de docentes e discentes vinculados ao curso de
        Relações Internacionais da UNESP/Franca e ao Programa de Pós-Graduação
        em Relações Internacionais San Tiago Dantas (UNESP, UNICAMP, PUC-SP) são
        desenvolvidos no espaço físico e virtual do LabRI/UNESP.
      </p>
    </>
  ),

  objetivos: (
    <>
      <p>
        O Laboratório de Relações Internacionais da Universidade Estadual
        Paulista (LabRI/UNESP), criado em 2011, faz parte da estrutura do
        departamento de Relações Internacionais (DERI) da UNESP, campus Franca.
        O principal objetivo que o DERI atribui ao LabRI/UNESP é fornecer apoio,
        por meio de tecnologias digitais, às atividades de ensino, pesquisa e
        extensão dos docentes, discentes e grupos de pesquisa vinculados ao
        curso de Relações Internacionais. Deste modo, o LabRI/UNESP é um espaço
        voltado a disseminar e prover auxílio na utilização de novas tecnologias
        da informação e comunicação. Dentre as atividades desenvolvidas no
        LabRI/UNESP se destacam:
      </p>
      <ul>
        {" "}
        1. Manutenção da infraestrutura computacional disponível no LabRI/UNESP;{" "}
      </ul>
      <ul> 2. Digitalização e pós-processamento de documentação textual </ul>
      <ul>
        {" "}
        3. Implementação de software e serviços de código aberto utilizados para
        fins acadêmicos;{" "}
      </ul>
      <ul>
        {" "}
        4. Auxílio na gestão de dados gerados e coletados pelos docentes,
        discentes e grupos de pesquisa;{" "}
      </ul>
      <ul>
        {" "}
        5. Apoio para a utilização das bases de dados disponíveis no LabRI;{" "}
      </ul>
      <ul> 6. Apoio ao trabalho experimental. </ul>
    </>
  ),

  historico: (
    <>
      <p>
        O Laboratório de Relações Internacionais (LabRI/UNESP) foi criado em
        2011 e faz parte da estrutura do departamento de Relações Internacionais
        (DERI) da Unesp, campus Franca.
      </p>
      <p>
        Para iniciar seus trabalhos, em 2012, o LabRI/UNESP adquiriu um conjunto
        de 10 computadores de mesa e a diretoria da Unesp, campus Franca,
        concedeu uma sala dentro da Diretoria Técnica de Informática (DTI) para
        que o LabRI/UNESP pudesse utilizar os equipamentos, foi feito upgrade de
        10 computadores e foi mantido 10 computadores que já estavam instalados
        no espaço do laboratório.
      </p>
      <p>
        No primeiro semestre de 2014, o LabRI/UNESP conseguiu junto a diretoria
        da Unesp, campus Franca, uma bolsa de estágio. Desde então o LabRI/UNESP
        tem o suporte de um estagiário remunerado.
      </p>
      <p>
        A partir de 2016, o LabRI/UNESP passou a utilizar o sistema Proxmox para
        virtualizar a infraestrutura computacional disponível, buscando utilizar
        e ampliar o suporte tecnológico aos discentes e docentes.
      </p>
      <p>
        A partir de 2017, o LabRI/UNESP começou a receber estagiários
        voluntários.
      </p>
      <p>
        A partir de 2020, passou a incentivar o maior uso de linguagem de
        programação no cotidiano de suas atividades. Além disso, iniciou um
        processo de reestruturação das bases de dados nas quais auxilia a
        manutenção e expansão.
      </p>
    </>
  ),

  suporte: (
    <>
      <p>
        Como já apontado acima, o LabRI/UNESP é um espaço que visa incentivar e
        dar suporte às atividades que demandam a utilização de tecnologias
        digitais. Devido a isso, podemos destacar duas principais frentes de
        apoio que o LabRI/UNESP oferece:
      </p>
      <p>
        <h4>1. Infraestrutura computacional</h4>
      </p>
      <p>
        Desde 2016 temos um cluster computacional, baseado no Proxmox - sistema
        open source de virtualização, no espaço do LabRI/UNESP com o objetivo de
        fornecer um apoio computacional mais adequado às atividades de pesquisa.
        A partir do Proxmox provemos:
      </p>
      <p>
        Alguns serviços de apoio a pesquisa com destaque a indexação integral de
        documentos, feita a partir do software livre Recoll, e a realização de
        Reconhecimento Óptico de Caracteres (OCR) automático em documentos de
        imagem e PDFs não pesquisáveis;
      </p>
      <p>
        Armazenamento das bases de dados. Grande parte destas bases são
        consultadas através da indexação realizada no Recoll e estão disponíveis
        para os pesquisadores via acesso remoto (x2go e/ou Chrome Remote
        Desktop).
      </p>
      <p>
        Acesso a um desktop virtual completo através da virtualização de
        sistemas operacional também conhecida como Virtual Desktop
        Infrastructure (VDI) que a utilização conjunta do Proxmox e de software
        de acesso remoto, como o x2go e o Chrome Remote Desktop, proporcionam.
        Os estudantes e professores com recursos computacionais pessoais não
        adequados podem se beneficiar bastante deste recurso.
      </p>
      <p>
        As máquinas em que o Proxmox está instalado se transformam em estações
        multifuncionais de trabalho fornecendo suporte virtual e real a uma gama
        de diversas atividades tanto presenciais como remotas.
      </p>
      <p>
        OBSERVAÇÃO: caso você necessite de informações mais detalhadas sobre os
        equipamentos que compõem a infraestrutura do LabRI/UNESP, entre em
        contato por email.
      </p>
      <h4>2. Projetos integrados e bases de dados</h4>
      <p>
        Uma importante iniciativa que o LabRI/UNESP tem promovido é transformar
        algumas das bases de dados em “Projetos de Dados” para estruturarmos
        melhor a gestão e expansão das mesmas. Esta iniciativa tem aperfeiçoado
        nossa estratégia de coleta, tratamento, armazenamento e disponibilização
        de dados. Além disso, reflete um acúmulo de anos de trabalho voltado
        para a sistematização de dados relevantes para as pesquisas de Relações
        Internacionais.
      </p>
      <p>
        Deste modo, passamos a estruturar essas bases de dados em um projeto
        integrado, conhecido também como projeto guarda-chuva, que abarca
        subprojetos responsáveis na gestão das várias bases de dados
        disponíveis. Para mais detalhes, <a href="/projetos">clique aqui</a>.
      </p>
    </>
  ),

  Equipe: (
    <>img className="img-equipe-foto" src="/img/social/equipe.png.svg"/</>
  ),
};

function SobreLabri() {
  return (
    <Layout title="SobreLabRI">
      <main className={styles.main}>
        <header className={clsx("hero hero--primary", styles.heroBanner)}>
          <div className={styles.container}>
            <h1 className={clsx(styles.hero__title, "hero__title")}>Sobre</h1>
            <h2 className={clsx(styles.hero__subtitle, "hero__subtitle")}>
              Conheça melhor a história do LabRI/UNESP
            </h2>
          </div>
        </header>
        <section className={styles.content}>
          <div className={styles.container}>
            <div className={styles.sobre}>
              <div className={styles.sobre_box}>
                <div className={clsx(styles.h1A)}> QUEM SOMOS </div>
                <p>{sobre.quem_somos}</p>
              </div>
              <div className={styles.sobre_box}>
                <h3>Objetivos</h3>
                <p>{sobre.objetivos}</p>
              </div>
              <div className={styles.sobre_box}>
                <h3>Histórico</h3>
                <p>{sobre.historico}</p>
              </div>
              <div className={styles.sobre_box}>
                <h3>Suporte aos docentes, discentes e demais colaboradores</h3>
                <p>{sobre.suporte}</p>
              </div>
              <div className={styles.sobre_box}>
                <h3>Equipe LabRI/UNESP</h3>
                <p>
                  {" "}
                  <div style={{ textAlign: "center" }}>
                    {" "}
                    {sobre.grupos_pesquisa}
                    Conheça os membros da nossa equipe
                  </div>{" "}
                </p>
                <img className="img-equipe-foto" src="/img/social/equipe.svg" />
                <br />
                <a class="button button--secondary" href="/docs/equipe">
                  Saiba Mais
                </a>
                <p>
                  {" "}
                  <div style={{ textAlign: "center" }}>
                    {" "}
                    {sobre.grupos_pesquisa}
                    <br></br>Faça parte da nossa equipe, conheça as{" "}
                    <a href="/docs/geral/info/estagio">
                      oportunidades de estágio
                    </a>{" "}
                    disponíveis.
                  </div>{" "}
                </p>
              </div>
            </div>
          </div>
        </section>
      </main>
    </Layout>
  );
}

export default SobreLabri;
