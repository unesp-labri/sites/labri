import React from "react";
import Layout from "@theme/Layout";
import styles from "./styles.module.scss";
import clsx from "clsx";
import { Projeto } from "../../components/projetos/Projeto";
import { extensao } from "../../data/projetos/projeto";

function Projetos_extensao() {
  return (
    <Layout title="Projeto De Extensao">
      <header className={clsx("hero hero--primary", styles.heroBanner)}>
        <div className={styles.container}>
          <h1 className="hero__title">Projetos</h1>
          <h2 className="hero__subtitle">
            Conheça e contribua com os projetos do LabRI/UNESP
          </h2>
        </div>
      </header>
      <div className={clsx(styles.apresentacao, styles.container)}>
        <h2> Apresentação </h2>
        <p>
          Abaixo estarão expostos os projetos nos quais o LabRI participa, assim
          como informações essenciais sobre esses projeto. Para acessar a lista
          simplificada dos projetos de extensão{" "}
          <a href="https://labriunesp.org/docs/projetos/extensao/lista">
            clique aqui
          </a>
          .
        </p>

        <div className="row tipos_projeto">
          <div className="col col--4">
            <h3>
              <a href="/projetos/dados/">Projetos de Dados</a>
            </h3>
          </div>
          <div className="col col--4">
            <h3>
              <a href="/projetos/ensino/">Projetos de Ensino</a>
            </h3>
          </div>
          <div className="col col--4">
            <h3>
              <a href="/projetos/sistemas/">Projetos de Sistemas</a>
            </h3>
          </div>
        </div>
      </div>

      <main className={styles.main}>
        <section className={styles.content}>
          {extensao && extensao.length > 0 && (
            <section className={styles.projetos}>
              <div className="container">
                {extensao.map((props, idx) => (
                  <Projeto key={idx} {...props} />
                ))}
              </div>
            </section>
          )}
        </section>
      </main>
    </Layout>
  );
}

export default Projetos_extensao;
