import React from "react";
import clsx from "clsx";
import Layout from "@theme/Layout";
import styles from "./styles.module.css";



const intro = [
    {
        projeto: "repri - labri-unesp",
        logo: "/img/repri/repri-logo.png",
        link: "/repri",
        link1: "/repri",
        link2: "/repri/institucional",
        link3: "/repri/publicacoes",
        link4: "/repri/projetos",
        link5: "/repri/noticias",
        link6: "/repri/equipe"
    }
]

const primeiracoluna = [
  {
    projeto: "repri - labri-unesp",
    img: "/img/repri/institucional.png",
    titulo: "Institucional"
  }
]

const segundacoluna = [
  {
    projeto: "repri - labri-unesp",
    texto: (
      <>
      A Rede de Pesquisa em Política Externa e Regionalismo é formada por pesquisadores, professores e alunos 
      de pós-graduação e graduação de diferentes Instituições de Ensino Superior e de Pesquisa, como a <b>UFU, UNIFESP, UNESP, UNB, UFGD, IPEA. </b>
      Os estudos e pesquisas realizados pelos participantes buscam, a partir 
      de objetos e perspectivas teóricas e metodológicas diversas, compreender as motivações e os 
      determinantes da política externa dos Estados e dos processos de regionalismo e multilateralismo. Alguns dos temas de 
      pesquisa são relativos à análise de instituições internacionais específicas, tanto do ponto de vista da sua inter-relação 
      com a política externa dos Estados, quanto do ponto de vista das suas particularidades, o processo de formulação de 
      política externa e a atuação dos atores domésticos, os processos específicos de regionalismo e o regionalismo comparado, 
      assim como as motivações e estratégias da política externa do Brasil e de outros países.
      </>
    )
  }
]

const vinculos = [
  {
    projeto: "repri - labri-unesp",
    img: "/img/repri/network-repri.svg"
  }
]

function Vinculos({img}){
  return(
    <div className={clsx(styles.colunaSemFundo, styles.center)}>
      <div className="row">
        <div className="col col--12">
          <img className={clsx(styles.modelo)} src={img} alt="Instituições vinculadas ao REPRI" />
        </div>
      </div>
    </div>
  )
}

function SegundaColuna({texto}){
  return(
    <div className={clsx(styles.colunaSemFundo, "container")}>
      <div className="row">
        <div className="col col--12">
          <p className={clsx(styles.paragrafo)}>{texto}</p>
        </div>
      </div>
    </div>
  )
}

function PrimeiraColuna({img, titulo}){
  return(
    <div className={clsx(styles.colunaSemFundo, styles.center, "container")}>
      <div className={clsx(styles.row, "row")}>

        <div className={clsx(styles.ilustracao, "col col--4")} >
          <img src={img} alt="Logo REPRI centralizado" />
        </div>

        <div className="col col--8">
          <h2 className={clsx(styles.h2Repri)}>{titulo}</h2>
        </div>

      </div>
    </div>
  )
}

function Intro({ logo, link, link1, link2, link3, link4, link5, link6 }) {
  return (
    <div className={clsx(styles.heroBanner, "hero hero--primary")}>
      <div className="row">
        <div className="col col--4">
          <div className={clsx(styles.LogoRepri)}>
            <a href={link}>
              <img src={logo} alt="Logo do REPRI" />
            </a>
          </div>
        </div>
        <div className={clsx(styles.Menu, "col col--8")}>
          <ul className={clsx(styles.ulLista)}>
            <li className={clsx(styles.liLista)}>
              <a href={link1}>home</a>
            </li>
            <li className={clsx(styles.liLista)}>
              <a href={link2}>institucional</a>
            </li>
            <li className={clsx(styles.liLista)}>
              <a href={link3}>publicações</a>
            </li>
            <li className={clsx(styles.liLista)}>
              <a href={link4}>projetos</a>
            </li>
            <li className={clsx(styles.liLista)}>
              <a href={link5}>notícias</a>
            </li>
            <li className={clsx(styles.liLista)}>
              <a href={link6}>equipe</a>
            </li>
          </ul>
        </div>
      </div>
    </div>
  );
}

function Institucional() {
  return (
    <Layout title=" Institucional | REPRI - Rede de Pesquisa em Política Externa e Regionalismo">
      <meta
        name="viewport"
        content="width=device-width, initial-scale=1"
      ></meta>
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
      <script src="js/bootstrap.min.js"></script>

      <header>
        <div>
          {intro.map((props, idx) => (
            <Intro key={idx} {...props} />
          ))}
        </div>
      </header>

      <main>
        <section className="content">

        <div className="container">
            <div className={clsx(styles.row, "row")}>
              {primeiracoluna.map((props, idx) => (
                <PrimeiraColuna key={idx} {...props} />
              ))}
            </div>
          </div>

          <div className="container">
            <div className={clsx(styles.row, "row")}>
              {segundacoluna.map((props, idx) => (
                <SegundaColuna key={idx} {...props} />
              ))}
            </div>
          </div>

          <div className="container">
            <div className={clsx(styles.row, "row")}>
              {vinculos.map((props, idx) => (
                <Vinculos key={idx} {...props} />
              ))}
            </div>
          </div>

        </section>
      </main>

    </Layout>
  );
}

export default Institucional;
