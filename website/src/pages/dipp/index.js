import React from "react";
import clsx from "clsx";
import Layout from "@theme/Layout";
import styles from "./styles.module.css";


const intro = [
  {
    projeto: "dipp - labri-unesp",
    logo: "/img/dipp/logo-dipp-pagina-inicial.svg",
    link: "/dipp",
    link1: "/dipp",
    link2: "/dipp/institucional",
    link3: "/dipp/noticias",
    link4: "/dipp/publicacoes",
    link5: "/dipp/projetos",
    link6: "/dipp/equipe",
    link7: "/dipp/cursos"
  },
];

const primeiracoluna = [
  {
    projeto: "dipp - labri-unesp",
    logo: "/img/dipp/logo-dipp.svg",
  },
];

const sobredipp = [
  {
    projeto: "dipp - labri-unesp",
    texto: (
      <>
        A <b>Rede DIPP – Development, International Politics and Peace –</b> se
        baseia na construção de uma rede que seja geograficamente ampliada
        (inicialmente em três continentes, mas visando ampliar para a Ásia
        futuramente) e capaz de oferecer abordagens interdisciplinares sobre
        comportamentos políticos e institucionalidade no âmbito das relações
        internacionais. Foi instaurada a partir do edital UNESP/Capes-Print
      </>
    ),
    link: "/dipp/institucional",
  },
];

const cards = [
  {
    projeto: "dipp - labri-unesp",
    img: "/img/dipp/pesquisas.jpg",
    titulo: "Projetos e Pesquisas",
    texto: <>Projetos e pesquisas envolvendo a DIPP</>,
    link: "/dipp/projetos",
  },
  {
    projeto: "dipp - labri-unesp",
    img: "/img/dipp/artigos.jpg",
    titulo: "Publicações",
    texto: <>Leia as publicações dos integrantes da DIPP</>,
    link: "/dipp/publicacoes",
  },
  {
    projeto: "dipp - labri-unesp",
    img: "/img/dipp/equipe.png",
    titulo: "Equipe",
    texto: <>Conheça os integrantes da DIPP</>,
    link: "/dipp/equipe",
  },
];

const instituicoes = [
  {
    projeto: "dipp - labri-unesp",
    img: "/img/dipp/network-dipp.svg",
    titulo: "Instituições Envolvidas"
  }
]

const logos = [
  {
    projeto: "dipp - labri-unesp",
    img: "/img/dipp/unesp-logo.gif",
    link: "https://www2.unesp.br/"
  },
  {
    projeto: "dipp - labri-unesp",
    img: "/img/dipp/unicamp-logo.png",
    link: "https://www.unicamp.br/"
  },
  {
    projeto: "dipp - labri-unesp",
    img: "/img/dipp/pucsp-logo.png",
    link: "https://www5.pucsp.br/"
  },
  {
    projeto: "dipp - labri-unesp",
    img: "/img/dipp/std-logo.png",
    link: "https://www.santiagodantas-ppgri.org/"
  },
  {
    projeto: "dipp - labri-unesp",
    img: "/img/dipp/capes-logo.jpg",
    link: "https://www.gov.br/capes/"
  },
  {
    projeto: "dipp - labri-unesp",
    img: "/img/dipp/udelar-logo.png",
    link: "https://udelar.edu.uy/"
  },
  {
    projeto: "dipp - labri-unesp",
    img: "/img/dipp/unpl-logo.jpeg",
    link: "https://unlp.edu.ar/"
  },
  {
    projeto: "dipp - labri-unesp",
    img: "/img/dipp/ucolombia-logo.jpeg",
    link: "https://unal.edu.co/"
  },
  {
    projeto: "dipp - labri-unesp",
    img: "/img/dipp/unam-logo.jpeg",
    link: "https://www.unam.mx/"
  },
  {
    projeto: "dipp - labri-unesp",
    img: "/img/dipp/ucoimbra-logo.png",
    link: "https://www.uc.pt/"
  },
  {
    projeto: "dipp - labri-unesp",
    img: "/img/dipp/uab-logo.png",
    link: "https://www.uab.cat/"
  },
  {
    projeto: "dipp - labri-unesp",
    img: "/img/dipp/ucm-logo.jpg",
    link: "https://www.ucm.es/"
  },
  {
    projeto: "dipp - labri-unesp",
    img: "/img/dipp/neoma-logo.jpg",
    link: "https://neoma-bs.com/"
  },
  {
    projeto: "dipp - labri-unesp",
    img: "/img/dipp/kuleuven-logo.png",
    link: "https://www.kuleuven.be/"
  },
  {
    projeto: "dipp - labri-unesp",
    img: "/img/dipp/giga-logo.png",
    link: "https://www.giga-hamburg.de/"
  },
  {
    projeto: "dipp - labri-unesp",
    img: "/img/dipp/columbiau-logo.png",
    link: "https://www.columbia.edu/"
  },
  {
    projeto: "dipp - labri-unesp",
    img: "/img/dipp/georgetown-logo.jpg",
    link: "https://www.georgetown.edu/"
  },
  {
    projeto: "dipp - labri-unesp",
    img: "/img/dipp/queensland-logo.png",
    link: "https://www.uq.edu.au/"
  },
  {
    projeto: "dipp - labri-unesp",
    img: "/img/dipp/calcary-logo.jpeg",
    link: "https://www.ucalgary.ca/"
  },
]


function Logos({img, link}){
  return(
    <div className={clsx(styles.colunaGaleria, "col col--2")}>
      <div className="row">
          <a href={link} target="_blank">
            <img className={clsx(styles.logoIcon)} src={img} alt="Logo" />
          </a>
      </div>
    </div>
  )
}

function Instituicoes({img, titulo}){
  return(
    <div className={clsx(styles.colunaSemFundo, styles.center)}>
      <div className="row">

        <div className="col col--12">
          <h2 className={clsx(styles.h2Dipp)}>{titulo}</h2>
        </div>

        <div className="col col--12">
          <img className={clsx(styles.modelo)} src={img} alt="Rede de Instituições Vinculadas ao DIPP" />
        </div>
      </div>
    </div>
  )
}

function Cards({ img, titulo, texto, link }) {
  return (
    <div className={clsx(styles.containerCards, " card col col--4")}>
      <div className={clsx(styles.Cards, "card-container")}>
        <div className="card__header">
          <img
            className={clsx(styles.imgCards)}
            src={img}
            alt="Ilustração da página"
          />
        </div>
        <div className="card__body">
          <h2 className={clsx(styles.h2Cards)}>{titulo}</h2>
          <p className={clsx(styles.pCards)}>{texto}</p>
        </div>
        <div className={clsx(styles.center, "card__footer")}>
          <a
            className={clsx(styles.botaoInfo, "button button--secondary")}
            href={link}
            target="_blank"
          >
            Saiba Mais
          </a>
        </div>
      </div>
    </div>
  );
}

function SobreDipp({ texto, link }) {
  return (
    <div className={clsx(styles.colunaComFundo)}>
      <div ClassName={clsx(styles.row)}>
        <div className={clsx(styles.center, "col col--12")}>
          <p className={clsx(styles.pDipp)}>{texto}</p>
          <a
            className={clsx(styles.botaoInfo, "button button--secondary")}
            href={link}
            target="_blank"
          >
            Saiba Mais
          </a>
        </div>
      </div>
    </div>
  );
}

function PrimeiraColuna({ logo }) {
  return (
    <div className={clsx(styles.colunaSemFundo, "container")}>
      <div className={clsx(styles.row)}>
        <div className="col col--12">
          <img
            className={clsx(styles.logoMaior)}
            src={logo}
            alt="Logo do Dipp"
          />
        </div>
      </div>
    </div>
  );
}

function Intro({ logo, link, link1, link2, link3, link4, link5, link6, link7 }) {
  return (
    <div className={clsx(styles.heroBanner, "hero hero--primary")}>
      <div className="row">
        <div className="col col--4">
          <div className={clsx(styles.LogoDipp)}>
            <a href={link}>
              <img src={logo} alt="Logo do DIPP" />
            </a>
          </div>
        </div>
        <div className={clsx(styles.Menu, "col col--8")}>
          <ul className={clsx(styles.ulLista)}>
            <li className={clsx(styles.liLista)}>
              <a href={link1}>home</a>
            </li>
            <li className={clsx(styles.liLista)}>
              <a href={link2}>institucional</a>
            </li>
            <li className={clsx(styles.liLista)}>
              <a href={link3}>notícias</a>
            </li>
            <li className={clsx(styles.liLista)}>
              <a href={link4}>publicações</a>
            </li>
            <li className={clsx(styles.liLista)}>
              <a href={link5}>projetos</a>
            </li>
            <li className={clsx(styles.liLista)}>
              <a href={link6}>equipe</a>
            </li>
            <li className={clsx(styles.liLista)}>
              <a href={link7}>cursos</a>
            </li>
          </ul>
        </div>
      </div>
    </div>
  );
}

function Home() {
  return (
    <Layout title="DIPP – Development, International Politics and Peace">
      <meta
        name="viewport"
        content="width=device-width, initial-scale=1"
      ></meta>
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
      <script src="js/bootstrap.min.js"></script>

      <header>
        <div>
          {intro.map((props, idx) => (
            <Intro key={idx} {...props} />
          ))}
        </div>
      </header>

      <main>
        <section className="content">
          <div className="container">
            <div className={clsx(styles.row, "row")}>
              {primeiracoluna.map((props, idx) => (
                <PrimeiraColuna key={idx} {...props} />
              ))}
            </div>
          </div>

          <div>
            <div className={clsx(styles.row, "row")}>
              {sobredipp.map((props, idx) => (
                <SobreDipp key={idx} {...props} />
              ))}
            </div>
          </div>

          <div className="container">
            <div className={clsx(styles.row, "row")}>
              {instituicoes.map((props, idx) => (
                <Instituicoes key={idx} {...props} />
              ))}
            </div>
          </div>

          <div className="container">
            <div className={clsx(styles.row, "row")}>
              {cards.map((props, idx) => (
                <Cards key={idx} {...props} />
              ))}
            </div>
          </div>

        </section>
      </main>
    </Layout>
  );
}

export default Home;
