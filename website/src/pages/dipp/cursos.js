import React from "react";
import clsx from "clsx";
import Layout from "@theme/Layout";
import styles from "./styles.module.css";


const intro = [
    {
        projeto: "dipp - labri-unesp",
        logo: "/img/dipp/logo-dipp-pagina-inicial.svg",
        link: "/dipp",
        link1: "/dipp",
        link2: "/dipp/institucional",
        link3: "/dipp/noticias",
        link4: "/dipp/publicacoes",
        link5: "/dipp/projetos",
        link6: "/dipp/equipe",
        link7: "/dipp/cursos"
    },
];

const primeiracoluna = [
    {
        projeto: "dipp - labri-unesp",
        titulo: "Cursos"
    }
]

const cursosdipp = [
    {
        projeto: "dipp - labri-unesp",
        img1: "/img/dipp/cursos/exemplo1.png",
        titulo1: "NOME DO CURSO",
        descricao1: (
            <>
                <b>Descrição</b>. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec in enim purus. Donec porttitor massa vel porta hendrerit.
            </>
        ),
        link1: "#",
        img2: "/img/dipp/cursos/exemplo2.png",
        titulo2: "NOME DO CURSO",
        descricao2: (
            <>
                <b>Descrição</b>. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec in enim purus. Donec porttitor massa vel porta hendrerit.
            </>
        ),
        link2: "#"
    },
]

function CursosDipp({ img1, img2, titulo1, titulo2, descricao1, descricao2, link1, link2 }) {
    return (
        <div className={clsx(styles.containerCards, styles.center)}>
            <div className="row">
                <div className={clsx(styles.cardCursos, "col col--4")}>
                    <a href={link1} target="_blank"><img className={clsx(styles.imgCursos)} src={img1} alt="Imagem do Curso" /></a>
                    <h2 className={clsx(styles.h2Cursos)}>{titulo1}</h2>
                    <p className={clsx(styles.pCursos)}>{descricao1}</p>
                    <a className={clsx(styles.botao, "button button--primary")} href={link1} target="_blank">Saiba mais</a>
                </div>
                <div className={clsx(styles.cardCursos, "col col--4")}>
                    <a href={link2} target="_blank"><img className={clsx(styles.imgCursos)} src={img2} alt="Imagem do Curso" /></a>
                    <h2 className={clsx(styles.h2Cursos)}>{titulo2}</h2>
                    <p className={clsx(styles.pCursos)}>{descricao2}</p>
                    <a className={clsx(styles.botao, "button button--primary")} href={link2} target="_blank">Saiba mais</a>
                </div>
            </div>
        </div>
    )
}

function PrimeiraColuna({ titulo }) {
    return (
        <div className={clsx(styles.colunaSemFundo, "container")}>
            <div className="row">
                <div className="col col--12">
                    <h2 className={clsx(styles.h2Dipp)}>
                        {titulo}
                    </h2>
                </div>
            </div>
        </div>
    )
}

function Intro({ logo, link, link1, link2, link3, link4, link5, link6, link7 }) {
    return (
        <div className={clsx(styles.heroBanner, "hero hero--primary")}>
            <div className="row">
                <div className="col col--4">
                    <div className={clsx(styles.LogoDipp)}>
                        <a href={link}>
                            <img src={logo} alt="Logo do DIPP" />
                        </a>
                    </div>
                </div>
                <div className={clsx(styles.Menu, "col col--8")}>
                    <ul className={clsx(styles.ulLista)}>
                        <li className={clsx(styles.liLista)}>
                            <a href={link1}>home</a>
                        </li>
                        <li className={clsx(styles.liLista)}>
                            <a href={link2}>institucional</a>
                        </li>
                        <li className={clsx(styles.liLista)}>
                            <a href={link3}>notícias</a>
                        </li>
                        <li className={clsx(styles.liLista)}>
                            <a href={link4}>publicações</a>
                        </li>
                        <li className={clsx(styles.liLista)}>
                            <a href={link5}>projetos</a>
                        </li>
                        <li className={clsx(styles.liLista)}>
                            <a href={link6}>equipe</a>
                        </li>
                        <li className={clsx(styles.liLista)}>
                            <a href={link7}>cursos</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    );
}

function Cursos() {
    return (
        <Layout title="Cursos – DIPP">
            <meta
                name="viewport"
                content="width=device-width, initial-scale=1"
            ></meta>
            <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
            <script src="js/bootstrap.min.js"></script>

            <header>
                <div>
                    {intro.map((props, idx) => (
                        <Intro key={idx} {...props} />
                    ))}
                </div>
            </header>

            <main>
                <section className="content">

                    <div className="container">
                        <div className={clsx(styles.row, "row")}>
                            {primeiracoluna.map((props, idx) => (
                                <PrimeiraColuna key={idx} {...props} />
                            ))}
                        </div>
                    </div>

                    <div className="container">
                        <div className={clsx(styles.row, "row")}>
                            {cursosdipp.map((props, idx) => (
                                <CursosDipp key={idx} {...props} />
                            ))}
                        </div>
                    </div>

                </section>
            </main>
        </Layout>
    );
}

export default Cursos;
