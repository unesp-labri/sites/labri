import React from "react";
import clsx from "clsx";
import Helmet from 'react-helmet'
import Layout from "@theme/Layout";
import stylesGlobal from "./styles.global.module.css";
import stylesSobre from "./sobre/styles.sobre.module.css";
import styles from "./styles.home.module.css";
import useBaseUrl from "@docusaurus/useBaseUrl";

import BannerSuperiorMenu, {Menu} from "./geral";
import {banner_superior_menu, menu} from "./variaveis";


/*
CONTEÚDO
*/

const faixa_01 = [
  {
    projeto: "gepdai - labri-unesp",
    titulo: "Conheça o GEPDAI",
    path_imgFoto: "/img/gepdai/home/conhecer.svg",
    link:"gepdai/sobre",
    texto: (
      <>
        O Grupo de Estudo em Política e Direito Ambiental Internacional (GEPDAI) se dedica a analisar e 
        compreender os temas da Política e do Direito Ambiental em uma perspectiva das Relações Internacionais.
      </>
    ), 
  }
]


const faixa_02 = [
  {
    projeto: "gepdai - labri-unesp",
    titulo: "Conheça nossos membros",
    path_imgFoto: "/img/gepdai/home/membros.svg",
    link: "/gepdai/membros",
    texto: (
      <>
        Nossa equipe é formada por professores, estudantes de graduação, pós-graduação, graduados.
      </>
    )
  },
  {
    projeto: "gepdai - labri-unesp",
    titulo: "Veja nossos contatos",
    path_imgFoto: "/img/gepdai/home/contatos.svg",
    link: "/gepdai/contatos",
    texto: (
      <>
      Entre em cotato e nos siga nas redes sociais.
      </>
    )
  }
]


/*
ESTRUTURA DA PAGINA
*/

function Faixa_02({titulo, texto, path_imgFoto, link}) {
  return (
    <div className={clsx(styles.containerColuna2, "col col--6")}>
      <div className={clsx(stylesGlobal.row, "row")}>
        <div className={clsx(styles.titulo_imagem)}>
          <h1 className={clsx(stylesGlobal.h1Geral)}>{titulo}</h1>
          <div className={clsx(styles.imgFoto)}>
            <img src={path_imgFoto} alt="Ilustração do card" />
          </div>
        </div>
        <div className={clsx(styles.conteudo)}>
          <p className={clsx(stylesGlobal.p1Geral)}>{texto}</p>
          <a className={clsx(stylesGlobal.saibaMais, "button button--secondary")} href={link}>Saiba mais</a>
        </div>
      </div>
    </div>
  )
}

function Faixa_01({titulo, texto, path_imgFoto, link}) {
  return (
    <div className={clsx(styles.containerColuna2)}>
      <div className={clsx(stylesGlobal.row, "row")}>
        <div className="col col--12">
          <h2 className={clsx(stylesGlobal.h1Geral)}>{titulo}</h2>
        </div>
        <div className={clsx(styles.imgFoto)}>
          <img src={path_imgFoto} alt="Ilustração do sobre o GEPDAI" className={clsx(styles.imgFoto)} />
        </div>
        <div className="col col--8">
          <p className={clsx(stylesGlobal.p1Geral)}>{texto}</p>
          <a className={clsx(stylesGlobal.saibaMais, "button button--secondary")} href={link}>Saiba mais</a>
        </div>
      </div>
    </div>
  )
}



function Home() {
  return (
    <Layout title="GEPDAI">
      <header className="App-header">
      <link href="https://cdn.jsdelivr.net/npm/bootstrap-icons/font/bootstrap-icons.css" rel="stylesheet"></link>
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
      <script src="js/bootstrap.min.js"></script>
  
      <div>
          {banner_superior_menu.map((props, idx) => (
            <BannerSuperiorMenu key={idx} {...props} />
          ))}
        </div>
      </header>
      <main className={clsx(stylesGlobal.main)}>
        <section className={styles.content}>
          <div>
            <div className={clsx(stylesGlobal.row, "row")}>
              {menu.map((props, idx) => (
                <Menu key={idx} {...props} />
              ))}
            </div>
          </div>

          <div className={clsx(styles.containerColuna1)}>
            <div className={clsx(stylesGlobal.row, "row")}>
              {faixa_01?.map((props, idx) => (
                <Faixa_01 key={idx} {...props} />
              ))}
            </div>
          </div>

          <div>
            <div className={clsx(stylesGlobal.row, "row")}>
              {faixa_02?.map((props, idx) => (
                <Faixa_02 key={idx} {...props} />
              ))}
            </div>
          </div>

        </section>
      </main>
    </Layout>
  );
}

export default Home;

