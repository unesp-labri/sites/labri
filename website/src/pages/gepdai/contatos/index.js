import React from "react";
import clsx from "clsx";
import Layout from "@theme/Layout";

import stylesGlobal from "../styles.global.module.css";
import styles from "./styles.contatos.module.css";
import useBaseUrl from "@docusaurus/useBaseUrl";

import BannerSuperiorMenu, { Menu } from "../geral";
import {banner_superior_menu, menu} from "../variaveis";




/*
CONTEÚDO
*/

const introcontatos = [
    {
        projeto: "gepdai - labri-unesp",
        titulo: "Contatos",
        texto: (
            <>
            Entre em contato e nos siga nas redes sociais.
            </>
        ),
        imgEquipe: "/img/gepdai/equipe2.png",
        title2: "Membros Atuais"
    }
]

const contatos = [
    {
      path_imgContatos:  "img/gepdai/contatos.svg",
      linkRedeSocial1: "https://www.linkedin.com/company/grupo-de-estudo-em-política-e-direito-ambiental-internacional-gepdai/posts/?feedView=all",
      linkRedeSocial2: "https://www.instagram.com/gepdai/",
      linkRedeSocial3: "unesp.gepdai@gmail.com",
      path_imgLogoRedeSocial1: "/img/social/linkedin.png",
      path_imgLogoRedeSocial2: "/img/social/instagram.png",
      path_imgLogoRedeSocial3: "/img/social/email-message.svg",
    },
    
    
]

/*
ESTRUTURA DA PÁGINA
*/

function IntroContatos({titulo, texto}) {
  return (
    <div className={styles.containerA}>
      <div className={clsx(stylesGlobal.row, "row")}>
        <div className="col col--12">
          <h2 className={stylesGlobal.h1Geral}>{titulo}</h2>
        </div>
        <div className="col col-6">
          <p className={stylesGlobal.p1Geral}>{texto}</p>
        </div>
      </div>
    </div>
  );
}

function InfoContatos({
  path_imgContatos,
  linkRedeSocial1,
  linkRedeSocial2,
  linkRedeSocial3,
  path_imgLogoRedeSocial1,
  path_imgLogoRedeSocial2,
  path_imgLogoRedeSocial3,

}) {
  return (
    <div className="col col--12">
      <div>
        <div>
          <div className={clsx(styles.fotoContatos, "text__center")}>
            <img
              src={useBaseUrl(path_imgContatos)}
              alt="Foto do membro da equipe GEPDAI"
            />
          </div>
        </div>
       
        <div className={clsx(styles.Icons)}>
          {linkRedeSocial1 && (
            <a target="_blank" href={linkRedeSocial1}>
              <img
                src={useBaseUrl(path_imgLogoRedeSocial1)}
                alt="Logo da Rede Social"
                width="180"
                height="110"
              />
            </a>
          )}
          {linkRedeSocial2 && path_imgLogoRedeSocial2 && (
            <a target="_blank" href={linkRedeSocial2}>
              <img src={useBaseUrl(path_imgLogoRedeSocial2)} alt="Logo da Rede Social" />
            </a>
          )}
          {linkRedeSocial3 && path_imgLogoRedeSocial3 && (
            <a target="_blank" href={`mailto:${linkRedeSocial3}`}>
              <img src={useBaseUrl(path_imgLogoRedeSocial3)} alt="Logo da Rede Social" />
            </a>
          )}
        </div>
      </div>
    </div>
  );
}

function Contatos(){
    return(
      <Layout title="GEPDAI">
        <header className="App-header">
        <link href="https://cdn.jsdelivr.net/npm/bootstrap-icons/font/bootstrap-icons.css" rel="stylesheet"></link>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
    
        <div>
            {banner_superior_menu.map((props, idx) => (
              <BannerSuperiorMenu key={idx} {...props} />
            ))}
          </div>
        </header>
          <main className={clsx(stylesGlobal.main)}>
              <section className={styles.content}>
                <div>
                    <div className={clsx(stylesGlobal.row, "row")}>
                    {menu.map((props, idx) => (
                    <Menu key={idx} {...props} />
                    ))}
                    </div>
                </div>

                <div>
                    <div className={clsx(stylesGlobal.row, "row")}>
                    {introcontatos.map((props, idx) => (
                    <IntroContatos key={idx} {...props} />
                    ))}
                    </div>
                </div>
                <div>
                    <div className={clsx(stylesGlobal.row, "row")}>
                    {contatos.map((props, idx) => (
                    <InfoContatos key={idx} {...props} />
                    ))}
                    </div>
                </div>

              </section>
          </main>
        </Layout>
    )
}

export default Contatos;