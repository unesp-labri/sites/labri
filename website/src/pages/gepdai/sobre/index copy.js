import React, { useState, useEffect } from "react";
import clsx from "clsx";
import Layout from "@theme/Layout";
import stylesGlobal from "../styles.global.module.css";
import styles from "./styles.sobre.module.css";
import useBaseUrl from "@docusaurus/useBaseUrl";


import BannerSuperiorMenu, { Menu } from "../geral";
import {banner_superior_menu, menu} from "../variaveis";




/*
CONTEÚDO
*/


const conhecer = [
  {
    projeto: "gepdai - labri-unesp",
    path_imgSobre: "/img/gepdai/logos/araucarias.svg",
    titulo: "Conheça o GEPDAI",
    texto: (
      <>
      O Grupo de Estudo em Política e Direito Ambiental Internacional (GEPDAI) se dedica a analisar e compreender os temas da Política e do Direito Ambiental em uma perspectiva das Relações Internacionais. O GEPDAI promove reuniões quinzenais que trazem as discussões de maneira horizontal entre os membros e docentes participantes resultando em reflexões e produções a respeito da dicotomia Sociedade - Meio Ambiente, tema de alta relevância no contexto global atual.
      </>
    )
  }
]

const atividades = [
  {
    projeto: "gepdai - labri-unesp",
    invertido: true,
    titulo: "Projeto",
    texto: (
      <>
      Ruptura metabólica e crise estrutural do capital: o complexo mineroindustrial em Minas Gerais e a tendência à eliminação das 
      condições elementares da reprodução social
      </>
    ),
    path_imgSobre: "/img/gepdai/logos/lencois-maranhenses.svg",
  },
  {
    projeto: "gepdai - labri-unesp",
    titulo: "Projeto",
    texto: (
      <>
      Educação Ambiental e Educação do campo para transformação social
      </>
    ),
    path_imgSobre: "/img/gepdai/logos/serra-dos-orgaos.svg",
  },
]


/*
ESTRUTURA DA PAGINA
*/


function Conhecer({path_imgSobre, titulo, texto}){
  return(
    <div className={clsx(styles.containerA)}>
      <div className={clsx(stylesGlobal.row, "row")}>
        <div className="col col--12">
        <h2 className={clsx(stylesGlobal.h1Geral)}>{titulo}</h2>
        </div>
        <div className="col col--4">
          <img src={path_imgSobre} alt="Ilustração do sobre o GEPDAI" className={clsx(styles.image)} />
        </div>
        <div className="col col--8">
          <p className={clsx(stylesGlobal.p1Geral)}>{texto}</p>
        </div>
      </div>
    </div>
  )
}

function Atividades({ 
  titulo, 
  texto, 
  path_imgSobre, 
  invertido, 
  titleAlign = "center", 
  textAlign = "center"
}) {
  return (
    <div className={clsx(
      styles.section,
      invertido ? styles.inverted : styles.light
    )}>
      <div className={clsx(
        styles.imageContainer,
        invertido ? styles.order2 : styles.order1
      )}>
        <img 
          src={path_imgSobre} 
          alt="Ilustração" 
          className={styles.image} 
        />
      </div>
      <div className={clsx(
        styles.textContainer,
        invertido ? styles.order1 : styles.order2
      )}>
        <h2 className={styles.title} style={{ textAlign: titleAlign }}>
          {titulo}
        </h2>
        <p className={styles.text} style={{ textAlign: textAlign }}>
          {texto}
        </p>
      </div>
    </div>
  );
}


function Sobre(){ 
  return(
    <Layout title="GEPDAI">
    <header className="App-header">
      <link href="https://cdn.jsdelivr.net/npm/bootstrap-icons/font/bootstrap-icons.css" rel="stylesheet"></link>
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
      <script src="js/bootstrap.min.js"></script>

      <div>
          {banner_superior_menu.map((props, idx) => (
            <BannerSuperiorMenu key={idx} {...props} />
          ))}
        </div>
    </header>
      <main className={clsx(styles.main)}>
      <section className={styles.content}>
          <div className={clsx(styles.container)}>
          <div className={clsx(stylesGlobal.row, "row")}>
              {menu.map((props, idx) => (
                <Menu key={idx} {...props} />
              ))}
            </div>
          </div>
          <div className={clsx(styles.container)}>
          <div className={clsx(stylesGlobal.row, "row")}>
              {conhecer.map((props, idx) => (
                <Conhecer key={idx} {...props} />
              ))}
            </div>
          </div>

          <div className={clsx(styles.container)}>
            <div>
              {atividades.map((props, idx) => (
                <Atividades key={idx} {...props} />
              ))}
            </div>
          </div>
        </section>
      </main>
    </Layout>
  )
}


export default Sobre;


/*
function Atividades({ titulo, texto, path_imgSobre, invertido, titleAlign = "center", textAlign = "center"}) {
  const sectionStyle = {
    backgroundColor: invertido ? "#ffffff" : "#f6f6f6",
    padding: "2rem",
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
  };

  const textContainerStyle = {
    order: invertido ? 1 : 2,
    flex: "1 1 60%",
    display: "flex",
    flexDirection: "column",
    justifyContent: "center",
  };

  const titleStyle = {
    color: "#33722e",
    fontSize: "3.2vw",
    marginBottom: "0.5rem",
    textAlign: titleAlign,
  };

  const textStyle = {
    fontSize: "2.1vw",
    fontWeight: "",
    textAlign: textAlign,
    marginBottom: "0.5rem",
  };

  
  return (
    <div style={sectionStyle}>
      <div style={{ order: invertido ? 2 : 1, flex: "1 1 40%" }}>
        <img src={path_imgSobre} alt="Ilustração" style={{ width: "100%", height: "auto" }} />
      </div>
      <div style={textContainerStyle}>
        <h2 style={titleStyle}>{titulo}</h2>
        <p style={textStyle}>{texto}</p>
      </div>
    </div>
  );
}
*/