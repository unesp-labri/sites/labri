import React, { useState, useEffect } from "react";
import clsx from "clsx";
import Layout from "@theme/Layout";
import stylesGlobal from "../styles.global.module.css";
import styles from "./styles.sobre.module.css";
import useBaseUrl from "@docusaurus/useBaseUrl";


import BannerSuperiorMenu, { Menu } from "../geral";
import {banner_superior_menu, menu} from "../variaveis";




/*
CONTEÚDO
*/


const conhecer = [
  {
    projeto: "gepdai - labri-unesp",
    path_imgSobre: "/img/gepdai/logos/Chapada-dos-Veadeiros.svg",
    titulo: "Conheça o GEPDAI",
    texto: (
      <>
      </>
    )
  }
]

const atividades = [
  {
    projeto: "gepdai - labri-unesp",
    invertido: true,
    titulo: "",
    texto: (
      <>
      O Grupo de Estudo em Política e Direito Ambiental Internacional (GEPDAI) foi criado em 2015 a partir da iniciativa da profa. dra. Fernanda Mello Sant’Anna em conjunto com alunos de graduação e pós-graduação da Faculdade de Ciências Humanas e Sociais (FCHS) da Universidade Estadual Paulista (UNESP), campus de Franca-SP. É grupo de pesquisa que busca trabalhar com a inter e transdisciplinariedade para estudar a política e o direito ambiental em suas múltiplas escalas e interconexões, a partir de perspectivas críticas das ciências humanas e sociais, e também a partir do diálogo de saberes e interculturalidade. Neste sentido, priorizamos perspectivas decoloniais e sobre a luta política dos povos originários, bem como o pluriverso de suas cosmovisões
      </>
    ),
    path_imgSobre: "/img/gepdai/logos/Chapada-dos-Veadeiros.svg",
    smallImagePath: "/img/gepdai/logos/Icone-Vermelho.svg", // Pequena imagem
    smallImagePosition: "left", // Segue a posição da imagem de fundo
    sideImagePath: "/img/gepdai/logos/Chapada-dos-Veadeiros.svg",
  },
  {
    projeto: "gepdai - labri-unesp",
    titulo: "",
    texto: (
      <>
        As pesquisas produzidas pelos membros do grupo podem ser acessadas pelas publicações de artigos, livros, trabalhos de conclusão de curso, dissertações de mestrado, teses de doutorado e demais materiais publicados para a divulgação dos resultados. Dessa forma, o GEPDAI promove reuniões quinzenais as quais trazem discussões de maneira horizontal entre os membros e docentes participantes, resultando em reflexões e produções a respeito da dicotomia Sociedade-Meio Ambiente, conflitos, justiça e políticas ambientais, entre outros temas de alta relevância no contexto global atual. Além disso, o grupo busca atuar de modo que o conhecimento produzido coletivamente possa ser difundido para toda a sociedade e traga contribuições internas e externas à universidade. Portanto, procuramos também realizar atividades de extensão ligadas às nossas pesquisas e eventos de divulgação.
      </>
    ),
    path_imgSobre: "/img/gepdai/logos/Araucarias.svg",
    smallImagePath: "/img/gepdai/logos/Icone-Vermelho.svg", // Pequena imagem
    smallImagePosition: "right", // Segue a posição da imagem de fundo
    sideImagePath: "/img/gepdai/logos/Chapada-dos-Veadeiros.svg",
  },
];

/*
ESTRUTURA DA PAGINA
*/


function Conhecer({titulo, texto}) {
  return (
          <div className={styles.h1Geral}>{titulo}</div>
  
  );
}

function Atividades({
  titulo,
  texto,
  path_imgSobre,
  sideImagePath,
  smallImagePath, // Caminho da pequena imagem
  smallImagePosition = "center", // Posição da pequena imagem (default: centralizada)
  invertido,
  titleAlign = "justify",
  textAlign = "justify",
  index
}) {
  const isEven = index % 2 === 0; // Verifica se o índice é par
  const imagePosition = isEven ? styles.left : styles.right; // Posição do texto
  const invertedImagePosition = clsx(
    isEven ? [styles.right, styles.rightsideImage] : [styles.left, styles.leftsideImage]
  ); // Posição invertida para a imagem lateral

  return (
    <div className={clsx(styles.section, invertido ? styles.inverted : styles.light)}>
      {/* Imagem de fundo */}
      <div className={clsx(styles.backgroundImage, imagePosition)}>
        <img src={path_imgSobre} alt="Ilustração" className={styles.image} />
      </div>
      {/* Nova imagem lateral */}
      
        <div className={clsx(styles.sideImageContainer,invertedImagePosition)}>
          <img src={sideImagePath} alt="Lateral" className={styles.sideImage} />
        </div>
      
      {/* Texto */}
      <div className={clsx(styles.textContainer, imagePosition)}>
        {/* Pequena imagem antes do texto */}
        {smallImagePath && (
          <img
            src={smallImagePath}
            alt="Ícone"
            className={clsx(
              styles.smallImage,
              smallImagePosition === "left" && styles.smallImageLeft,
              smallImagePosition === "right" && styles.smallImageRight,
              smallImagePosition === "center" && styles.smallImageCenter
            )}
          />
        )}
        <h2 className={styles.title} style={{ textAlign: titleAlign }}>{titulo}</h2>
        <p className={stylesGlobal.p1Geral} style={{ textAlign: textAlign }}>{texto}</p>
      </div>
    </div>
  );
}

function Sobre(){ 
  return(
    <Layout title="GEPDAI">
    <header className="App-header">
      <link href="https://cdn.jsdelivr.net/npm/bootstrap-icons/font/bootstrap-icons.css" rel="stylesheet"></link>
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
      <script src="js/bootstrap.min.js"></script>

      <div>
          {banner_superior_menu.map((props, idx) => (
            <BannerSuperiorMenu key={idx} {...props} />
          ))}
        </div>
    </header>
      <main className={clsx(styles.main)}>
      <section className={styles.content}>
          <div className={clsx(styles.container)}>
          <div className={clsx(stylesGlobal.row, "row")}>
              {menu.map((props, idx) => (
                <Menu key={idx} {...props} />
              ))}
            </div>
          </div>
          <div className={clsx(styles.container)}>
              {conhecer.map((props, idx) => (
                <Conhecer key={idx} {...props} />
              ))}
          </div>

          <div className={clsx(styles.container)}>
            <div>
              {atividades.map((props, idx) => (
                <Atividades key={idx} {...props} index={idx}/>
              ))}
            </div>
          </div>
        </section>
      </main>
    </Layout>
  )
}


export default Sobre;


/*
function Atividades({ titulo, texto, path_imgSobre, invertido, titleAlign = "center", textAlign = "center"}) {
  const sectionStyle = {
    backgroundColor: invertido ? "#ffffff" : "#f6f6f6",
    padding: "2rem",
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
  };

  const textContainerStyle = {
    order: invertido ? 1 : 2,
    flex: "1 1 60%",
    display: "flex",
    flexDirection: "column",
    justifyContent: "center",
  };

  const titleStyle = {
    color: "#33722e",
    fontSize: "3.2vw",
    marginBottom: "0.5rem",
    textAlign: titleAlign,
  };

  const textStyle = {
    fontSize: "2.1vw",
    fontWeight: "",
    textAlign: textAlign,
    marginBottom: "0.5rem",
  };

  
  return (
    <div style={sectionStyle}>
      <div style={{ order: invertido ? 2 : 1, flex: "1 1 40%" }}>
        <img src={path_imgSobre} alt="Ilustração" style={{ width: "100%", height: "auto" }} />
      </div>
      <div style={textContainerStyle}>
        <h2 style={titleStyle}>{titulo}</h2>
        <p style={textStyle}>{texto}</p>
      </div>
    </div>
  );
}
*/