import React from "react";
import clsx from "clsx";
import Layout from "@theme/Layout";

import stylesGlobal from "../styles.global.module.css";
import styles from "./styles.publicacoes.module.css";


import BannerSuperiorMenu, { Menu } from "../geral";
import {banner_superior_menu, menu} from "../variaveis";


/*
CONTEÚDO
*/


const primeiracoluna = [
    {
        projeto: "publicacoes - labri-unesp",
        title: "Publicações"
    }
]

const publicacoes = [
{
        projeto: "publicacoes - labri-unesp",
        titulo: "Transição energética justa na Amazônia e as hidrelétricas (2023)",
        autoria: "Autoria: Fernanda Mello Santanna, Pedro Henrique Casalecchi Bortoletto, Arianne Caus Donda",
        path_imgIcon: "/img/gepdai/icon-download.svg",
        link: "http://dx.doi.org/10.38116/rtm32art5"
    }, 
{
        projeto: "publicacoes - labri-unesp",
        titulo: "Indonésia: um panorama pós-Pandêmico (2023)",
        autoria: "Autoria: Amauri Marcelo Fernandes Junior",
        path_imgIcon: "/img/gepdai/icon-download.svg",
        link: "https://boletim-broriente.medium.com/indon%C3%A9sia-um-panorama-p%C3%B3s-pand%C3%AAmico-cd14a6f63f39"
    }, 

]

const voltar = [
    {
        projeto: "publicacoes - labri-unesp",
        link: "/gepdai/publicacoes"
    }
] 

/*
ESTRUTURA DA PAGINA
*/


function Artigos({titulo, autoria, path_imgIcon, link}){
    return(
        <div className={clsx(styles.containerArtigos)}>
            <div className={clsx(stylesGlobal.row, "row")}>
                <div className="col col--8">
                    <h2 className={clsx(styles.h2Artigos)}>{titulo}</h2>
                    <p className={clsx(styles.pArtigos)}>{autoria}</p>
                </div>
                <div className={clsx(styles.IconPubli, "col col--4")}>
                    <a target="_blank" href={link}><img src={path_imgIcon} style={{ filter: '#4A8F29' }} alt="acesso ao item" /></a>
                </div>
            </div>
        </div>
    )
}

function PrimeiraColuna({title}){
    return(
        <div className={clsx(stylesGlobal.row, "row")}>
                <div className="col col--12">
                    <h2 className={clsx(stylesGlobal.h1Geral)}>
                        {title}
                    </h2>
                </div>
        </div>
    )
}


function Publicacoes(){
    return(
        <Layout title="GEPDAI">
        <header className="App-header">
        <link href="https://cdn.jsdelivr.net/npm/bootstrap-icons/font/bootstrap-icons.css" rel="stylesheet"></link>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
    
        <div>
            {banner_superior_menu.map((props, idx) => (
              <BannerSuperiorMenu key={idx} {...props} />
            ))}
          </div>
        </header>
        <main className={clsx(styles.main)}>
        <section className={styles.content}>
          <div className={clsx(styles.container)}>
          <div className={clsx(stylesGlobal.row, "row")}>
              {menu.map((props, idx) => (
                <Menu key={idx} {...props} />
              ))}
          </div>
          </div>

          <div className={clsx(styles.container)}>
          <div className={clsx(stylesGlobal.row, "row")}>
              {primeiracoluna.map((props, idx) => (
                <PrimeiraColuna key={idx} {...props} />
              ))}
          </div>
          </div>

          <div className={clsx(styles.container)}>
          <div className={clsx(stylesGlobal.row, "row")}>
              {publicacoes.map((props, idx) => (
                <Artigos key={idx} {...props} />
              ))}
          </div>
          </div>
        </section>
    </main>
    </Layout>
    )
}

export default Publicacoes;
