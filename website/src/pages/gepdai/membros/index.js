import React from "react";
import clsx from "clsx";
import Layout from "@theme/Layout";

import stylesGlobal from "../styles.global.module.css";
import styles from "./styles.membros.module.css";
import useBaseUrl from "@docusaurus/useBaseUrl";

import BannerSuperiorMenu, { Menu } from "../geral";
import {banner_superior_menu, menu} from "../variaveis";




/*
CONTEÚDO
*/

const sobreequipe = [
    {
        projeto: "gepdai - labri-unesp",
        titulo: "Conheça nossos Membros",
        texto: (
            <>
            Nossa equipe é formada por professores, estudantes de graduação, pós-graduação, graduados.
            </>
        ),
        imgEquipe: "/img/gepdai/equipe2.png",
        title2: "Membros Atuais"
    }
]

const membrosAtuais = [
  {
    projeto: "gepdai - labri-unesp",
    nome: "Fernanda Mello Santanna",
    descricao: "Professora de Relações Internacionais (UNESP/Franca)",
    path_imgFoto: "img/gepdai/membros/fernanda-santanna.gif",
    linkRedeSocial: "http://lattes.cnpq.br/6759610800922658",
    path_imgLogoRedeSocial: "/img/social/lattes.png",
    linkRedeSocial2: "https://www.linkedin.com/in/fernanda-mello-sant-anna-79b8882b3/",
    path_imgLogoRedeSocial2: "/img/social/linkedin.png"
},  
  {
    projeto: "gepdai - labri-unesp",
    nome: "Amauri Marcelo Fernandes Junior",
    descricao: "Mestrando no Programa de Pós-Graduação San Tiago Dantas (UNESP/UNICAMP/PUC-SP)",
    linkRedeSocial: "http://lattes.cnpq.br/8868478037885754",
    linkRedeSocial2: "https://www.linkedin.com/in/amauri-marcelo-fernandes-junior-31b429232/",
    path_imgFoto: "/img/gepdai/membros/amauri-fernandes-junior.jpg",
    path_imgLogoRedeSocial: "/img/social/lattes.png",
    path_imgLogoRedeSocial2: "/img/social/linkedin.png",
    },

    {
      projeto: "gepdai - labri-unesp",
      nome: "Felipe Ramos Cabral Sá",
      descricao: "Mestrando no Programa de Pós-Graduação San Tiago Dantas (UNESP/UNICAMP/PUC-SP)",
      path_imgFoto: "img/gepdai/membros/felipe-sa.jpeg",
      linkRedeSocial: "http://lattes.cnpq.br/2409344335621112",
      path_imgLogoRedeSocial: "/img/social/lattes.png",
      linkRedeSocial2: "https://www.linkedin.com/in/safelipe/",
      path_imgLogoRedeSocial2: "/img/social/linkedin.png"
    },
    {
        projeto: "gepdai - labri-unesp",
        nome: "Milena Gonçalves Ludwig",
        descricao: "Doutoranda no Programa de Pós-Graduação San Tiago Dantas (UNESP/UNICAMP/PUC-SP)",
        path_imgFoto: "img/gepdai/membros/milena-ludwig.gif",
        linkRedeSocial: "http://lattes.cnpq.br/0621563929751912",
        path_imgLogoRedeSocial: "/img/social/lattes.png",
        linkRedeSocial2: "https://www.linkedin.com/in/milena-ludwig-11361414b/",
        path_imgLogoRedeSocial2: "/img/social/linkedin.png"
    },
    {
        projeto: "gepdai - labri-unesp",
        nome: "Alice Tomazetti da Silveira ",
        descricao: "Mestranda no Programa de Pós-Graduação San Tiago Dantas (UNESP/UNICAMP/PUC-SP)",
        path_imgFoto: "img/gepdai/membros/alice-silveira.gif",
        linkRedeSocial: "http://lattes.cnpq.br/0379638633272616",
        path_imgLogoRedeSocial: "/img/social/lattes.png",
        linkRedeSocial2: "/gepdai",
        path_imgLogoRedeSocial2: "/img/social/linkedin.png"
    },
    {
        projeto: "gepdai - labri-unesp",
        nome: "Arianne Caus Donda",
        descricao: "Mestra em Planejamento e Análise de Políticas Públicas pela UNESP",
        path_imgFoto: "/img/gepdai/membros/donda.jpeg",
        linkRedeSocial: "https://lattes.cnpq.br/4355375319185867",
        path_imgLogoRedeSocial: "/img/social/lattes.png",
        linkRedeSocial2: "https://www.linkedin.com/in/arianedonda/",
        path_imgLogoRedeSocial2: "/img/social/linkedin.png"
    },
    {
      projeto: "gepdai - labri-unesp",
      nome: "Guilherme de Lima Souza",
      descricao: "Mestrando no Programa de Pós-Graduação San Tiago Dantas (UNESP/UNICAMP/PUC-SP)",
      path_imgFoto: "/img/gepdai/membros/guilherme.gif",
      linkRedeSocial: " http://lattes.cnpq.br/7967633682024023",
      path_imgLogoRedeSocial: "/img/social/lattes.png",
      linkRedeSocial2: "https://www.linkedin.com/in/guilherme-limasouza",
      path_imgLogoRedeSocial2: "/img/social/linkedin.png"
  },
  {
    projeto: "gepdai - labri-unesp",
    nome: "Otávio Henrique da Silva Lemes",
    descricao: "Graduando em Relações Internacionais pela UNESP/Franca",
    path_imgFoto: "img/gepdai/membros/otavio.jpeg",
    linkRedeSocial: "http://lattes.cnpq.br/5108922386231228",
    path_imgLogoRedeSocial: "/img/social/lattes.png",
    linkRedeSocial2: "https://www.linkedin.com/in/otaviohslemes",
    path_imgLogoRedeSocial2: "/img/social/linkedin.png"
},
{
  projeto: "gepdai - labri-unesp",
  nome: "Taynara Martins Batista",
  descricao: "Mestranda no Programa de Pós-Graduação San Tiago Dantas (UNESP/UNICAMP/PUC-SP)",
  path_imgFoto: "/img/gepdai/membros/taynara.gif",
  linkRedeSocial: "https://lattes.cnpq.br/3386979544441878",
  path_imgLogoRedeSocial: "/img/social/lattes.png",
  linkRedeSocial2: "https://www.linkedin.com/in/taynara-martins-batista-a3230713b/",
  path_imgLogoRedeSocial2: "/img/social/linkedin.png"
},
  {
      projeto: "gepdai - labri-unesp",
      nome: "Rodrigo Santos Mendonça",
      descricao: "Mestrando em Alimentos e Nutrição (PPGAN) - Faculdade de Engenharia de Alimentos (FEA) - Universidade Estadual de Campinas (UNICAMP)",
      path_imgFoto: "img/gepdai/membros/rodrigo.jpg",
      linkRedeSocial: "http://lattes.cnpq.br/0306148140138311",
      path_imgLogoRedeSocial: "/img/social/lattes.png",
      linkRedeSocial2: "/gepdai",
      path_imgLogoRedeSocial2: "/img/social/linkedin.png"
  },

  {
    projeto: "gepdai - labri-unesp",
    nome: "Dante Gabriel Nunes Zanoti",
    descricao: "Doutorando no Programa de Pós-Graduação San Tiago Dantas (UNESP/UNICAMP/PUC-SP)",
    path_imgFoto: "img/gepdai/membros/dante.jpeg",
    linkRedeSocial: "http://lattes.cnpq.br/8931755297840460",
    path_imgLogoRedeSocial: "/img/social/lattes.png",
    linkRedeSocial2: "https://www.linkedin.com/in/dante-gabriel-nunes-zanoti-4885658b/",
    path_imgLogoRedeSocial2: "/img/social/linkedin.png"
},

{
  projeto: "gepdai - labri-unesp",
  nome: "Julia Ribeiro dos Santos",
  descricao: "Graduanda em Relações Internacionais pela UNESP/Franca",
  path_imgFoto: "img/gepdai/membros/julia-santos.jpeg",
  linkRedeSocial: " http://lattes.cnpq.br/1462877791273004",
  path_imgLogoRedeSocial: "/img/social/lattes.png",
  linkRedeSocial2: "https://www.linkedin.com/in/julia-ribeiro-dos-santos",
  path_imgLogoRedeSocial2: "/img/social/linkedin.png"
},
  
]

/*
ESTRUTURA DA PÁGINA
*/

function SobreEquipe({titulo, texto}) {
  return (
    <div className={styles.containerA}>
      <div className={clsx(stylesGlobal.row, "row")}>
        <div className="col col--12">
          <h2 className={stylesGlobal.h1Geral}>{titulo}</h2>
        </div>
        <div className="col col-6">
          <p className={stylesGlobal.p1Geral}>{texto}</p>
        </div>
      </div>
    </div>
  );
}

function MembrosAtuais({
  nome,
  path_imgFoto,
  linkRedeSocial,
  linkRedeSocial2,
  path_imgLogoRedeSocial,
  path_imgLogoRedeSocial2,
  descricao
}) {
  return (
    <div className="col col--4">
      <div>
        <div>
          <div className={clsx(styles.fotoMembro, "text__center")}>
            <img
              src={useBaseUrl(path_imgFoto)}
              alt="Foto do membro da equipe GEPDAI"
            />
          </div>
        </div>
        <div>
          <h2 className={clsx(stylesGlobal.h3Geral)}>{nome}</h2>
          {descricao && <p className={clsx(styles.descricaoMembro)}>{descricao}</p>}
        </div>
        <div className={clsx(styles.Icons)}>
          {linkRedeSocial && (
            <a target="_blank" href={linkRedeSocial}>
              <img
                src={useBaseUrl(path_imgLogoRedeSocial)}
                alt="Logo da Rede Social"
                width="180"
                height="110"
              />
            </a>
          )}
          {linkRedeSocial2 && path_imgLogoRedeSocial2 && (
            <a target="_blank" href={linkRedeSocial2}>
              <img src={useBaseUrl(path_imgLogoRedeSocial2)} alt="Logo da Rede Social" />
            </a>
          )}
        </div>
      </div>
    </div>
  );
}


function Equipe(){
    return(
      <Layout title="GEPDAI">
        <header className="App-header">
        <link href="https://cdn.jsdelivr.net/npm/bootstrap-icons/font/bootstrap-icons.css" rel="stylesheet"></link>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
    
        <div>
            {banner_superior_menu.map((props, idx) => (
              <BannerSuperiorMenu key={idx} {...props} />
            ))}
          </div>
        </header>
          <main className={clsx(stylesGlobal.main)}>
              <section className={styles.content}>
                <div>
                    <div className={clsx(stylesGlobal.row, "row")}>
                    {menu.map((props, idx) => (
                    <Menu key={idx} {...props} />
                    ))}
                    </div>
                </div>

                <div>
                    <div className={clsx(stylesGlobal.row, "row")}>
                    {sobreequipe.map((props, idx) => (
                    <SobreEquipe key={idx} {...props} />
                    ))}
                    </div>
                </div>
                <div className="col col--12">
                  <h2 className={stylesGlobal.h2Geral}>Membros</h2>
                </div>
                <div>
                    <div className={clsx(stylesGlobal.row, "row")}>
                    {membrosAtuais.map((props, idx) => (
                    <MembrosAtuais key={idx} {...props} />
                    ))}
                    </div>
                </div>

              </section>
          </main>
        </Layout>
    )
}

export default Equipe;