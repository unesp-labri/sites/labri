import React from "react";
import clsx from "clsx";
import Layout from "@theme/Layout";
import styles from "./styles.module.css";

const intro = [
    {
        projeto: "sobre - labri-unesp",
        imgIntro: "/img/cidades-sustentaveis/logo-ver1.svg"
    }
]

const menu = [
    {
        projeto: "css - labri-unesp",
        link1: "/cidades-sustentaveis",
        link2: "/cidades-sustentaveis/sobre",
        link3: "/cidades-sustentaveis/projeto",
        link4: "/cidades-sustentaveis/publicacoes",
        link5: "/cidades-sustentaveis/contato"
    }
]

const primeiracoluna = [
    {
        projeto: "publicacoes - labri-unesp",
        title: "Artigos Jornalísticos"
    }
]

const artigos = [
{
        projeto: "publicacoes - labri-unesp",
        titulo: "Da Comemoração ao Resíduo: um recorte sobre o desmoderado consumo e o descarte inadequado no período de fim de ano",
        autor: "Autor: Larissa Vitoria Moreira",
        imgFoto: "/img/cidades-sustentaveis/pdf-icon.svg",
        link: "https://drive.google.com/file/d/1cxGphMHbRqiY5e3CRiz0pIJgg4hHvIeM/view?usp=sharing"
    }, 
{
        projeto: "publicacoes - labri-unesp",
        titulo: "O Estatuto da Cidade e a construção de cidades saudáveis e sustentáveis no Brasil",
        autor: "Autor: Isabela Maria Valente Capato",
        imgFoto: "/img/cidades-sustentaveis/pdf-icon.svg",
        link: "https://drive.google.com/file/d/1NqcPPbec5W5ok7qi_rhg0bfSmC4E-_md/view?usp=drive_link"
    }, 

{
        projeto: "publicacoes - labri-unesp",
        titulo: "Desenvolvimentismo predatório na ditadura militar: breve panorama além para do desmatamento",
        autor: "Autor: Maisa Martins Alves",
        imgFoto: "/img/cidades-sustentaveis/pdf-icon.svg",
        link: "https://drive.google.com/file/d/1xwlee5tpJRxlqGNV5UfgCPuJnpDHZX8Q/view?usp=drive_link"
    }, 
{
        projeto: "publicacoes - labri-unesp",
        titulo: "A questão do carbono na União Europeia e a descarbonização no Brasil",
        autor: "Autor: Sarah de Jesus Silva dos Santos",
        imgFoto: "/img/cidades-sustentaveis/pdf-icon.svg",
        link: "https://drive.google.com/file/d/1DHgw_dhLCSLJ4xH2y1P8vFgHDJKwA55C/view?usp=drive_link"
    }, 
    
{
        projeto: "publicacoes - labri-unesp",
        titulo: "A exploração e a dificuldade para a criação de projetos de preservação na Caatinga",
        autor: "Autor: Luiz Henrique Lopes Mariano",
        imgFoto: "/img/cidades-sustentaveis/pdf-icon.svg",
        link: "https://drive.google.com/file/d/1cL3_tNHHcSMRxuJhy7f7JF81TLYS6VQe/view?usp=drive_link"
    }, 
    
{
        projeto: "publicacoes - labri-unesp",
        titulo: "Turismo Sustentável: Práticas e Desafios na Preservação de Destinos Naturais",
        autor: "Autor: Luis Fernando de Jesus Ribeiro",
        imgFoto: "/img/cidades-sustentaveis/pdf-icon.svg",
        link: "https://drive.google.com/file/d/19NtuIxXabcqolMwSc7Ewg8WJozorQZIM/view?usp=drive_link"
    }, 
    
{
        projeto: "publicacoes - labri-unesp",
        titulo: "Mulheres Campesinas e a busca por direitos para ampliação de práticas agroecológicas",
        autor: "Autora: Larissa Vitoria Moreira",
        imgFoto: "/img/cidades-sustentaveis/pdf-icon.svg",
        link: "https://drive.google.com/file/d/19Rha4PzIh92oAcQeU7ASjyCMrx7Fv78j/view?usp=drive_link"
    },
     {
        projeto: "publicacoes - labri-unesp",
        titulo: "Racismo Ambiental: Análise das Disparidades Ambientais e Sociais na Distribuição de Ônus e Benefícios",
        autor: "Autora: Bianca Lopes de Sousa",
        imgFoto: "/img/cidades-sustentaveis/pdf-icon.svg",
        link: "https://drive.google.com/file/d/1CHzQ4ZO7hr5PGJG9m18Edn57ji9F2UkN/view?usp=drive_link"
    },
    {
        projeto: "publicacoes - labri-unesp",
        titulo: "Biopirataria no Brasil e sua deficiência legislativa",
        autor: "Autora: Heloísa Calixto da Silva",
        imgFoto: "/img/cidades-sustentaveis/pdf-icon.svg",
        link: "https://drive.google.com/file/d/1WNwpiRC4jHy8tMc7_PDxVjN5-zS1kcVt/view?usp=drive_link"
    },
    {
        projeto: "publicacoes - labri-unesp",
        titulo: "Mercado de carbono: o que é e como funciona no Brasil.",
        autor: "Autora: Ana Laura Murari Silva",
        imgFoto: "/img/cidades-sustentaveis/pdf-icon.svg",
        link: "https://drive.google.com/file/d/1Gy47RKcW237RJ985uUS-6Z4cMLAeRDjg/view?usp=share_link"
    },
    {
        projeto: "publicacoes - labri-unesp",
        titulo: "Legislação migratória para Refugiados Climáticos na União Europeia",
        autor: "Autora: Sarah Santos",
        imgFoto: "/img/cidades-sustentaveis/pdf-icon.svg",
        link: "https://drive.google.com/file/d/1SzfZaDqozqHfI6YhB4iICcMhnW4HHIb_/view?usp=drive_link"
    },
     {
        projeto: "publicacoes - labri-unesp",
        titulo: "Cooperativas de catadores de lixo: o que são e como elas atuam?",
        autor: "Autora: Luiza Polo Rosario",
        imgFoto: "/img/cidades-sustentaveis/pdf-icon.svg",
        link: "https://drive.google.com/file/d/1RmrgTiaV0zQLMCeGOQPuov9ExTLRdDgV/view?usp=drive_link"
    },
    {
        projeto: "publicacoes - labri-unesp",
        titulo: "A mobilidade urbana como instrumento de equidade e direito à cidade",
        autor: "Autora: Raquel Rebequi de Sousa",
        imgFoto: "/img/cidades-sustentaveis/pdf-icon.svg",
        link: "https://drive.google.com/file/d/1whasOQ8rmQJ9yzvmoqQyyuchH1TGEucy/view?usp=drive_link"
    },
    {
        projeto: "publicacoes - labri-unesp",
        titulo: "Desigualdade de gênero e proteção ambiental: os caminhos do Ecofeminismo",
        autor: "Autora: Isabela Maria Valente Capato",
        imgFoto: "/img/cidades-sustentaveis/pdf-icon.svg",
        link: "https://drive.google.com/file/d/1KEBeeJAtNKsdIHxcE1xzJTbktTap-hFh/view"
    },
    {
        projeto: "publicacoes - labri-unesp",
        titulo: "Agroecologia: caminho sustentável para asseguração ao direito à alimentação saudável",
        autor: "Autora: Larissa Vitória Moreira",
        imgFoto: "/img/cidades-sustentaveis/pdf-icon.svg",
        link: "https://drive.google.com/file/d/1-wApateKwrpUFWHZnr8rNC-OwG5eSOqJ/view"
    },
    {
        projeto: "publicacoes - labri-unesp",
        titulo: "Moda Sustentável: qual caminho devemos seguir?",
        autor: "Autora: Manuela Martins Kassab",
        imgFoto: "/img/cidades-sustentaveis/pdf-icon.svg",
        link: "https://drive.google.com/file/d/1xJDoJRfr8Nu3Awc87LbMtEQXA1NTG_l3/view?usp=drive_link"
    },
    {
        projeto: "publicacoes - labri-unesp",
        titulo: "O abandono das praças e o impedimento da construção da cidadania coletiva nas cidades brasileiras",
        autor: "Autora: Taís Aimee Vasconcellos Prudêncio",
        imgFoto: "/img/cidades-sustentaveis/pdf-icon.svg",
        link: "https://drive.google.com/file/d/1ok76_Oi-B9NctgD8V7WGjmgUFvtYbg9I/view?usp=sharing"
    },
    {
        projeto: "publicacoes - labri-unesp",
        titulo: "Estados falidos enquanto geradores de refugiados climáticos",
        autor: "Autor: Douglas Sábio Franco Vivo",
        imgFoto: "/img/cidades-sustentaveis/pdf-icon.svg",
        link: "https://drive.google.com/file/d/149GeE1hmbU_RtDu9nczN3cEXeJuw7YrF/view?usp=sharing"
    },
    {
        projeto: "publicacoes - labri-unesp",
        titulo: "Uma breve análise de práticas ESG no setor agro brasileiro",
        autor: "Autor: André Camargo Ziviani Filho",
        imgFoto: "/img/cidades-sustentaveis/pdf-icon.svg",
        link: "https://drive.google.com/file/d/1z7bytb0RRDvJmPXgDL7fM070YUi-s29A/view"
    },
    {
        projeto: "publicacoes - labri-unesp",
        titulo: "Justiça ambiental e as lutas sociais pelo direito humano a um meio ambiente saudável",
        autor: "Autora: Isabela Maria Valente Capato",
        imgFoto: "/img/cidades-sustentaveis/pdf-icon.svg",
        link: "https://drive.google.com/file/d/1kVF2-JG1aqrrx7tDK-ZRV5JWse3e9M2r/view"
    },
    {
        projeto: "publicacoes - labri-unesp",
        titulo: "Infraestrutura urbana e justiça ambiental",
        autor: "Autora: Sarah de Jesus Silva dos Santos",
        imgFoto: "/img/cidades-sustentaveis/pdf-icon.svg",
        link: "https://drive.google.com/file/d/1zl99DAOVEvA1pCZt9ut3xDnJkEjY3W_6/view"
    },
    {
        projeto: "publicacoes - labri-unesp",
        titulo: "A deterioração do solo agravada pela agricultura e consequências",
        autor: "Autora: Desconhecida",
        imgFoto: "/img/cidades-sustentaveis/pdf-icon.svg",
        link: "https://drive.google.com/file/d/1pJptDshS_5455JUkXj8LFm-H9_LC6t_D/view?usp=sharing"
    },
    {
        projeto: "publicacoes - labri-unesp",
        titulo: "A degradação ambiental e seus riscos para a população",
        autor: "Autora: Gabriela Fideles Silva",
        imgFoto: "/img/cidades-sustentaveis/pdf-icon.svg",
        link: "https://85e3e4b9-f154-4e78-947f-441e7131ec28.filesusr.com/ugd/401d97_3bdf53b7b84a4bdf933734483ece4b43.pdf"
    },
    {
        projeto: "publicacoes - labri-unesp",
        titulo: "Cidades saudáveis e sustentáveis: acessibilidade urbana como instrumento para efetivação dos direitos humanos das pessoas com deficiência",
        autor: "Autores: André Luiz Pereira Spinieli e Letícia de Paula Souza",
        imgFoto: "/img/cidades-sustentaveis/pdf-icon.svg",
        link: "https://85e3e4b9-f154-4e78-947f-441e7131ec28.filesusr.com/ugd/401d97_4386bf27a56a4d588aa0158cd9cfc357.pdf"
    },
    {
        projeto: "publicacoes - labri-unesp",
        titulo: "O princípio da precaução como garantia para a equidade intergeracional",
        autor: "Autores: Angélica Miguel Cardoso e Beatriz Bernardino Buccioli",
        imgFoto: "/img/cidades-sustentaveis/pdf-icon.svg",
        link: "https://85e3e4b9-f154-4e78-947f-441e7131ec28.filesusr.com/ugd/401d97_9fa0cfb7db344642b5989346220d754a.pdf"
    },
    {
        projeto: "publicacoes - labri-unesp",
        titulo: "Obesididade e planejamento das cidades: uma discussão necessária",
        autor: "Autores: Gustavo Alarcon Rodrigues e Leonardo Eiji Kawamoto",
        imgFoto: "/img/cidades-sustentaveis/pdf-icon.svg",
        link: "https://85e3e4b9-f154-4e78-947f-441e7131ec28.filesusr.com/ugd/401d97_1ea23a17aac14245af2bd5e5bf9839ca.pdf"
    },
    {
        projeto: "publicacoes - labri-unesp",
        titulo: "A relação do aquecimento global com as cidades pelo mundo e seus reflexos sociais",
        autor: "Autor: Murillo Ravagnani",
        imgFoto: "/img/cidades-sustentaveis/pdf-icon.svg",
        link: "https://85e3e4b9-f154-4e78-947f-441e7131ec28.filesusr.com/ugd/401d97_fe9622ab12774cabb139fa2bad7e2e45.pdf"
    },
    {
        projeto: "publicacoes - labri-unesp",
        titulo: "A importância de projetos sustentáveis para as cidades: Programa Município Verde Azul-SP",
        autor: "Autores: Julia Nogueira e Natália Yoshida",
        imgFoto: "/img/cidades-sustentaveis/pdf-icon.svg",
        link: "https://85e3e4b9-f154-4e78-947f-441e7131ec28.filesusr.com/ugd/401d97_44837ef113874b1e935ecb8b25045024.pdf"
    },
    {
        projeto: "publicacoes - labri-unesp",
        titulo: "As escolas sustentáveis e as políticas públicas que fortalecem suas implantações ",
        autor: "Autora: Carla Cristina de Moraes",
        imgFoto: "/img/cidades-sustentaveis/pdf-icon.svg",
        link: "https://85e3e4b9-f154-4e78-947f-441e7131ec28.filesusr.com/ugd/401d97_4c58dd07aef04b3eb6f1e9027ef3b42f.pdf"
    },
    {
        projeto: "publicacoes - labri-unesp",
        titulo: "Daqui para frente, o desenvolvimento é para trás: digressões sobre o desafio global da Agenda 2030",
        autor: "Autora: Daniela Antunes Chierice Davanso",
        imgFoto: "/img/cidades-sustentaveis/pdf-icon.svg",
        link: "https://85e3e4b9-f154-4e78-947f-441e7131ec28.filesusr.com/ugd/401d97_8a35cf9b23e1481bb6db89dcf4fb321d.pdf"
    },
    {
        projeto: "publicacoes - labri-unesp",
        titulo: "Pobreza e exclusão na cidade",
        autor: "Autora: Isabela Menezes Franco ",
        imgFoto: "/img/cidades-sustentaveis/pdf-icon.svg",
        link: "https://85e3e4b9-f154-4e78-947f-441e7131ec28.filesusr.com/ugd/401d97_02e8f2a858ab47bc8afb97547547653e.pdf"
    },
]

const voltar = [
    {
        projeto: "publicacoes - labri-unesp",
        link: "/cidades-sustentaveis/publicacoes"
    }
] 

function Voltar({link}){
    return(
        <div className={clsx(styles.containerSobre)}>
            <div className="row">
                <div className="col col--12">
                <a className="button button--secondary" href={link}>Voltar para o topo</a>
                </div>
            </div>
        </div>
    )
}

function Artigos({titulo, autor, imgFoto, foto, link}){
    return(
        <div className={clsx(styles.containerArtigos)}>
            <div className="row">
                <div className="col col--8">
                    <h2 className={clsx(styles.h2Artigos)}>{titulo}</h2>
                    <p className={clsx(styles.pArtigos)}>{autor}</p>
                </div>
                <div className={clsx(styles.IconPDF, "col col--4")}>
                    <a href={link}><img src={imgFoto} alt="Documento em PDF para Download" /></a>
                </div>
            </div>
        </div>
    )
}

function PrimeiraColuna({title}){
    return(
        <div className={clsx(styles.containerSobre)}>
            <div className="row">
                <div className="col col--12">
                    <h2 className={clsx(styles.h2CSS)}>
                        {title}
                    </h2>
                </div>
            </div>
        </div>
    )
}

function Menu({link1, link2, link3, link4, link5}){
    return(
        <div className={clsx(styles.Menu)}>
          <ul className={clsx(styles.ulLista)}>
            <li className={clsx(styles.liLista)}><a href={link1}>HOME</a></li>
            <li className={clsx(styles.liLista)}><a href={link2}>SOBRE</a></li>
            <li className={clsx(styles.liLista)}><a href={link3}>PROJETO</a></li>
            <li className={clsx(styles.liLista)}><a href={link4}>PUBLICAÇÕES</a></li>
            <li className={clsx(styles.liLista)}><a href={link5}>CONTATO</a></li>
          </ul>
        </div>
    )
}

function Intro({imgIntro, projeto}){
    return(
        <div className={clsx(styles.heroBanner)}>
        <div className={styles.LogoCSS}>
            <img src={imgIntro} alt="Logo Cidades Saudáveis e Sustentáveis" />
        </div>
    </div>
    )
}

function Publicacoes(){
    return(
        <Layout title="Cidades Saudáveis e Sustentáveis">
            <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
            <script src="js/bootstrap.min.js"></script>
        <header className={clsx(styles.heroBanner)}>
            <div>
            {intro.map((props, idx) => (
            <Intro key={idx} {...props} />
            ))}
            </div>
        </header>
        <main className={clsx(styles.main)}>
        <section className={styles.content}>
          <div className={clsx(styles.container)}>
          <div className="row">
              {menu.map((props, idx) => (
                <Menu key={idx} {...props} />
              ))}
          </div>
          </div>

          <div className={clsx(styles.container)}>
          <div className="row">
              {primeiracoluna.map((props, idx) => (
                <PrimeiraColuna key={idx} {...props} />
              ))}
          </div>
          </div>

          <div className={clsx(styles.container)}>
          <div className="row">
              {artigos.map((props, idx) => (
                <Artigos key={idx} {...props} />
              ))}
          </div>
          </div>

          <div className={clsx(styles.container)}>
          <div className="row">
              {voltar.map((props, idx) => (
                <Voltar key={idx} {...props} />
              ))}
          </div>
          </div>
          </section>
          </main>
        </Layout>
    )
}

export default Publicacoes;
