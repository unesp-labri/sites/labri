import React from "react";
import clsx from "clsx";
import Layout from "@theme/Layout";
import styles from "./styles.module.css";

 
const intro = [
  {
    projeto: "css - labri-unesp",
    imgIntro: "/img/cidades-sustentaveis/logo-ver1.svg"
  }
]

const menu = [ 
  {
    projeto: "css - labri-unesp",
    link1: "/cidades-sustentaveis",
    link2: "/cidades-sustentaveis/sobre",
    link3: "/cidades-sustentaveis/projeto",
    link4: "/cidades-sustentaveis/publicacoes",
    link5: "/cidades-sustentaveis/contato"
  }
]

const primeiracoluna = [
  {
    projeto: "css - labri-unesp",
    titulo1: "SOBRE FRANCA/SP",
    texto1: (
      <>
      O município de Franca está localizado a nordeste do Estado de São Paulo, distando aproximadamente 400 km da capital. Atualmente, a cidade se destaca pelo seu forte centro industrial de calçado e também como a cidade brasileira com a melhor rede de saneamento básico e tratamento de água dentre todos os municípios brasileiros.
      </>
    ),
    imgFoto1: "/img/cidades-sustentaveis/franca-sp.png"
  }
]

const segundacoluna = [
  {
    projeto: "css - labri-unesp",
    titulo: "QUEM SOMOS",
    texto: (
      <>
      Grupo de extensão formado por dezesseis  discentes, entre eles graduandos e pós graduandos em Direito, Relações Internacionais e Arquitetura e Urbanismo, vinculados a UNESP/Franca e UFU/MG sob coordenação do Prof. Dr. Daniel Damásio Borges.
      </>
    ),
    imgFoto: "/img/cidades-sustentaveis/quem-somos-home.svg",
    link: "/cidades-sustentaveis/sobre"
  },
  {
    projeto: "css - labri-unesp",
    titulo: "PROJETO",
    texto: (
      <>
      Projeto de Extensão Cidades Saudáveis e Sustentáveis foi criado em 2020, vinculado ao Centro Jurídico Social da Universidade Estadual Paulista, campus de Franca/SP.      
      </>
    ),
    imgFoto: "/img/cidades-sustentaveis/projeto-home.svg",
    link: "/cidades-sustentaveis/projeto"
  },
  {
    projeto: "css - labri-unesp",
    titulo: "PUBLICAÇÕES",
    texto: (
      <>
      Espaço destinado para compartilhar toda a produção desenvolvida pelos membros do grupo de extensão, a fim de informar a sociedade civil e demais acadêmicos sobre a temática proposta pelo projeto       
      </>
    ),
    imgFoto: "/img/cidades-sustentaveis/publicacoes-home.svg",
    link: "/cidades-sustentaveis/publicacoes"
  },
]

function SegundaColuna({titulo, texto, imgFoto, foto, link, link2}){
  return(
    <div className={clsx(styles.containerCards, "col col--4")}>
      <div className={clsx(styles.Cards)}>
          <h2 className={clsx(styles.h2CSS)}><a href={link2} className={clsx(styles.link2)}>{titulo}</a></h2>
          <img src={imgFoto} alt="Logo" className={clsx(styles.SobreCards)} />
          <p className={clsx(styles.pCSS)}>{texto}</p>
          <a href={link} className={clsx(styles.link2)}>Saiba mais</a>
      </div>
    </div>
  ) 
}

function PrimeiraColuna({titulo1, texto1, imgFoto1, foto1}){
  return(
    <div className={clsx(styles.containerColuna1)}>
      <div className={clsx(styles.row, "row")}>
        <div className="col col--4">
          <h2 className={clsx(styles.h2CSS)}>{titulo1}</h2>
          <p className={clsx(styles.pCSS)}>{texto1}</p>
        </div>
        <div className="col col--8">
          <img src={imgFoto1} alt="Cidade de Franca" className={clsx(styles.coluna1)} />
        </div>
      </div>
    </div>
  )
}

function Menu({link1, link2, link3, link4, link5}){
  return(
      <div className={clsx(styles.Menu)}>
          <ul className={clsx(styles.ulLista)}>
            <li className={clsx(styles.liLista)}><a href={link1}>HOME</a></li>
            <li className={clsx(styles.liLista)}><a href={link2}>SOBRE</a></li>
            <li className={clsx(styles.liLista)}><a href={link3}>PROJETO</a></li>
            <li className={clsx(styles.liLista)}><a href={link4}>PUBLICAÇÕES</a></li>
            <li className={clsx(styles.liLista)}><a href={link5}>CONTATO</a></li>
          </ul>
      </div>
  )
}

function Intro({projeto, imgIntro}) {
  return (
    <div className={clsx(styles.heroBanner)}>
      <div className={styles.LogoCSS}>
        <img src={imgIntro} alt="Logo Cidades Saudáveis e Sustentáveis" />
      </div>
    </div>
  );
}

function Home(){
  return(
    <Layout title="Cidades Saudáveis e Sustentáveis">
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
      <script src="js/bootstrap.min.js"></script>
      
      <header className={clsx(styles.heroBanner)}>
        <div>
          {intro.map((props, idx) => (
            <Intro key={idx} {...props} />
          ))}
        </div>
      </header>
      <main className={clsx(styles.main)}>
        <section className={styles.content}>
          <div className={clsx(styles.container)}>
          <div className={clsx(styles.row, "row")}>
              {menu.map((props, idx) => (
                <Menu key={idx} {...props} />
              ))}
            </div>
          </div>

          <div className={clsx(styles.container)}>
          <div className={clsx(styles.row, "row")}>
              {primeiracoluna.map((props, idx) => (
                <PrimeiraColuna key={idx} {...props} />
              ))}
            </div>
          </div>

          <div className={clsx(styles.container)}>
          <div className={clsx(styles.row, "row")}>
              {segundacoluna.map((props, idx) => (
                <SegundaColuna key={idx} {...props} />
              ))}
            </div>
          </div>
        </section>
      </main>
    </Layout>
  )
}



export default Home;