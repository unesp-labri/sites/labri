export const infos = [
  {
    title: "Bem vindo ao LabRI/UNESP",
    description:
      "O Laboratório de Relações Internacionais (LabRI/UNESP) é um espaço voltado a apoiar o desenvolvimento de atividades de pesquisa, ensino e extensão que demandem a utilização de tecnologias digitais.",
    link: "./sobre",
    imageUrl: "./img/home_quem_somos.svg",
  },
  {
    title: "Projetos",
    description:
      "Em conjunto com os grupos e redes de pesquisa vinculados ao LabRI/UNESP, desenvolvemos projetos tecnológicos voltados a fornecer um pelo suporte às atividades de ensino, pesquisa e extensão.",
    link: "./projetos",
    imageUrl: "./img/home_projetos.svg",
  },
  {
    title: "Oportunidade de Estágio",
    description:
      "Atualmente temos uma vaga de estágio remunerado preenchida. Há a possibilidade de qualquer estudante se inscrever no estágio voluntário (não remunerado) e fazer parte da Equipe do LabRI/UNESP!",
    link: "./docs/geral/info/estagio",
    imageUrl: "./img/home_estagio.svg",
  },
];
