export const docsAzul = [
  {
    title: "Equipe",
    text: "Conheça os membros que fazem parte da equipe LabRI/UNESP",
    link: "/docs/equipe",
    imageUrl: "./img/home_noticias.svg",
    bgColor: "#60A5E0",
  },
  {
    title: "Publicações",
    text: "Textos produzidos pela equipe do LabRI/UNESP e por participantes vinculados aos projetos.",
    link: "/cadernos",
    imageUrl: "./img/home_revista.svg",
    bgColor: "#60A5E0",
  },
];

export const docsVermelho = [
  {
    title: "Tecnologias Digitais",
    text: "Materiais sobre ferramentas tecnológicas feitos pela equipe do LabRI/UNESP ou por terceiros.",
    link: "/docs/projetos/ensino/tec-digitais/intro",
    imageUrl: "./img/home_tec.svg",
    bgColor: "#FA505B",
  },
  {
    title: "Oficinas e Cursos",
    text: "Conheça as oficinas e os cursos produzidos pelo LabRI/UNESP",
    link: "/docs/projetos/ensino/intro",
    imageUrl: "./img/home_cursos.svg",
    bgColor: "#FA505B",
  },
];
