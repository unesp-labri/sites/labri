export const sobre = [
  {
    title: "Projetos de Dados",
    imgLogo: "/img/projetos/logo-projetos-dados2.svg",
    description:
      "Os projetos de dados são iniciativas que visam coletar, tratar, analisar e, quando possível, disponibilizar dados relevantes para as pesquisas em Relações Internacionais. Deste modo, neste espaço é possível encontrar projetos voltados à incorporação de tecnologias digitais para melhorar o manuseio da crescente quantidade de dados disponíveis nas redes sociais, nos meios de comunicação, nas instâncias governamentais e organismos internacionais.",
    link: "/projetos/dados/",
  },
  {
    title: "Projetos de extensão",
    imgLogo: "/img/projetos/logo-projetos-extensao2.svg",
    description:
      "Os projetos de extensão são iniciativas voltadas para melhorar e incentivar a interação entre o meio acadêmico e a sociedade. Assim, neste espaço é possível encontrar projetos que buscam envolver docentes e discentes em atividades voltadas à divulgação científica.",
    link: "/projetos/extensao/",
  },
];
