/**
 * Copyright (c) Facebook, Inc. and its affiliates.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */
 import React from 'react';
 import Layout from '@theme/Layout';
 import BlogPostItem from '@theme-original/BlogPostItem';
 import BlogPostPaginator from '@theme/BlogPostPaginator';
 import BlogSidebar from '@theme/BlogSidebar';
 import TOC from '@theme/TOC';
 import EditThisPage from '@theme/EditThisPage';
 import {ThemeClassNames} from '@docusaurus/theme-common';
 import styles from './styles.module.css'; // add
 import Link from '@docusaurus/Link';
 
 const MONTHS = [
   'Jan',
   'Fev',
   'Mar',
   'Abr',
   'Mai',
   'Jun',
   'Jul',
   'Ago',
   'Set',
   'Out',
   'Nov',
   'Dez',
 ];
 
 
 function gera_link(tag) {
   const link= "./tags/{0}".replace("{0}", tag).toLowerCase()
   return link
 }
 
 function BlogPostPage(props) {
   const {content: BlogPostContents, sidebar} = props;
   const {frontMatter, metadata} = BlogPostContents;
   const {title, description, nextItem, prevItem, editUrl, date, readingTime} = metadata;
   const {hide_table_of_contents: hideTableOfContents, tipo_publicacao, author, subtitulo, tags, download_pdf} = frontMatter;
   const match = date.substring(0, 10).split('-');
   const year = match[0];
   const month = MONTHS[parseInt(match[1], 10) - 1];
   const day = parseInt(match[2], 10);
   return (
     <Layout
       title={title}
       description={description}
       wrapperClassName={ThemeClassNames.wrapper.blogPages}
       pageClassName={ThemeClassNames.page.blogPostPage}>
       {BlogPostContents && (
         <div className="container margin-vert--lg">
           <div className="row">
             <div className="col col--2">
               <BlogSidebar sidebar={sidebar} />
             </div>
             <main className="col col--8">
             <div className={styles.download_top}>
                   <div className={styles.download_pdf}>
                     <a href={frontMatter.download_pdf} target="_blank" type="application/pdf" rel="noopener noreferrer">
                       <img src="/img/download_pdf.svg" alt="Download PDF" ></img>
                       <p className={styles.img__description}>Baixe o arquivo em PDF</p>
                     </a>
                     <a href={frontMatter.download_pdf} target="_blank" type="application/pdf" rel="noopener noreferrer">
                       <img src="/img/download_epub.svg" alt="Download EPUB" ></img>
                       <p className={styles.img__description}>Baixe o arquivo em EPUB</p>
                    </a>
                   </div>
                 </div>
               <div className={styles.tags}>          
                   {tags.map((tag)=>(  
                     <a className={styles.tag} href={gera_link(tag)}>
                       {tag}
                     </a>
                     ))}
               </div>
               <div className={styles.premiere}>
                 <center><strong className={styles.publi}> {tipo_publicacao} · ISSN 2764-7552</strong></center>
                 <h1 className={styles.titulo}> {title} {subtitulo}</h1>                
                 <h2 className={styles.autores}> Por {author} </h2>
                 <div className={styles.tempo}>
                   <center><time dateTime={date}>
                       {day}/{month}/{year} · Tempo de leitura: {Math.ceil(readingTime)} min
                   </time></center>
                 </div>
               </div>
                 <BlogPostContents />
               <div>{editUrl && <EditThisPage editUrl={editUrl} />}</div>
               {(nextItem || prevItem) && (
                 <div className="margin-vert--xl">
                   <BlogPostPaginator nextItem={nextItem} prevItem={prevItem} />
                 </div>
               )}
             </main>
             {!hideTableOfContents && BlogPostContents.toc && (
                <div className="col col--2">
                <div className={styles.download_bottom}>
                      <div className={styles.download_pdf}>
                        <a href={frontMatter.download_pdf} target="_blank" type="application/pdf" rel="noopener noreferrer">
                      <img src="/img/download_pdf.svg" alt="Download PDF" ></img>
                      <img src="/img/download_epub.svg" alt="Download EPUB" ></img>
                      {<p className={styles.img__description}>Baixe o arquivo em PDF</p>}
                    </a>
                  </div>
                </div>
                <TOC toc={BlogPostContents.toc} />
              </div>
             )}
           </div>
         </div>
       )}
     </Layout>
   );
 }
 
 export default BlogPostPage;
 
