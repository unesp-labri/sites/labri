---
slug: producao-cientifica-031
tags: [Lantri - Produção]
title: "Democracia e as agendas reformistas na América Latina"
author: Isabella Silvério
tipo_publicacao: "Produção Cientica - 031"
image_url: https://i.imgur.com/TX5V6U2.png
descricao: Este artigo discute os programas reformistas na Bolívia, no Brasil, no Equador e na Venezuela, durante o século XXI, e as suas limitações conjunturais...
---

<div align="center">

![](https://i.imgur.com/TX5V6U2.png)

</div>



>*Este artigo discute os programas reformistas na Bolívia, no Brasil, no Equador e na Venezuela, durante o século XXI, e as suas limitações conjunturais...*

<!--truncate-->

Este artigo discute os programas reformistas na Bolívia, no Brasil, no Equador e na Venezuela, durante o século XXI, e as suas limitações conjunturais. Nesse contexto, questões econômicas, jurídicas, políticas e sociais de ordem doméstica e internacional são analisadas como fatores de contradição que explicam a manutenção do sistema capitalista e a dificuldade de realização plena do regime democrático na região. Considerando as particularidades de cada Estado analisado, nota-se que, apesar das reformas realizadas, aspectos indeléveis da estrutura institucional não foram alterados, o que, consequentemente, limita a aplicação prática do que é proposto.



Para acessar o artigo completo, [clique aqui](https://periodicos.fclar.unesp.br/semaspas/article/view/11661).