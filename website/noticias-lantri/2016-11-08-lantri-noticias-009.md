---
slug: lantri-noticias-009
tags: [Lantri - Notícias]
title: "A silenciosa ditadura do algoritmo"
author: Equipe Lantri
tipo_publicacao: "Lantri Notícias - 009"
image_url: https://i.imgur.com/pyxP0Ni.jpg
descricao: "Em sociedades digitalizadas, decisões cruciais sobre a vida são tomadas por máquinas e códigos. Isso multiplica a desigualdade e ameaça a democracia..."
---

<div align="center">

![](https://i.imgur.com/pyxP0Ni.jpg)

</div>


>*Em sociedades digitalizadas, decisões cruciais sobre a vida são tomadas por máquinas e códigos. Isso multiplica a desigualdade e ameaça a democracia...*

<!--truncate-->

Em sociedades digitalizadas, decisões cruciais sobre a vida são tomadas por máquinas e códigos. Isso multiplica a desigualdade e ameaça a democracia.


Vivemos todos na Era do Algoritmo. Aqui está uma história que não apenas resume a era, mas mostra como a obsessão pelo algoritmo pode dar terrivelmente errado. Para saber mais, [clique aqui](http://www.cartacapital.com.br/blogs/outras-palavras/a-silenciosa-ditadura-do-algoritmo).