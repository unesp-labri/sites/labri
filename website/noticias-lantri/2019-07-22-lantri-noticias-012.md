---
slug: lantri-noticias-012
tags: [Lantri - Notícias]
title: "Pesquisadora do LANTRI é contemplada com bolsa para apresentação de artigo em nível internacional"
author: Equipe Lantri
tipo_publicacao: "Lantri Notícias - 012"
image_url: https://i.imgur.com/AUW7dbT.png
descricao: "A pesquisadora do LANTRI Julia Borba foi contemplada com um financiamento para participação em evento acadêmico no segundo semestre de 2019, pela International Network for Government Science Advice (INGSA)..."
---

<div align="center">

![](https://i.imgur.com/AUW7dbT.png)

</div>


>A pesquisadora do LANTRI Julia Borba foi contemplada com um financiamento para participação em evento acadêmico no segundo semestre de 2019, pela International Network for Government Science Advice (INGSA)...*

<!--truncate-->

A pesquisadora do LANTRI Julia Borba foi contemplada com um financiamento para participação em evento acadêmico no segundo semestre de 2019, pela International Network for Government Science Advice (INGSA). 


Mestra em Relações Internacionais pelo Programa San Tiago Dantas (UNESP - UNICAMP - PUC/SP), obteve a bolsa através de participação em workshop organizado pelo Instituto de Estudos Avançados da USP, o que permitiu participação no concurso pelo qual foi contemplada com a bolsa. Julia apresentou parte de sua dissertação de mestrado acerca da Aliança do Pacífico na política externa brasileira e o debate doméstico sobre o bloco regional, mostrando a lacuna existente entre o Senado e a academia que produz análises científicas sobre o tema, sendo premiada entre 10 pesquisadores da região da América Latina (cada pesquisador recebeu um prêmio em dinheiro). Seu trabalho ainda foi novamente premiado entre os 3 selecionados com financiamento para apresentação do paper. Os trabalhos podem ser [acessados aqui](http://www.ingsa.org/chapters/ingsa-latin-america/activities/lac-essay-2019/).