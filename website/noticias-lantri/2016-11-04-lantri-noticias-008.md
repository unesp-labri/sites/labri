---
slug: lantri-noticias-008
tags: [Lantri - Notícias]
title: "Conferência de 50 anos do Science Policy Research Unit"
author: Equipe Lantri
tipo_publicacao: "Lantri Notícias - 008"
image_url: https://i.imgur.com/p4VjPmo.jpg
descricao: "Entre os dias 7 e 9 de setembro, no Reino Unido, reuniram-se cerca de 500 pesquisadores para discutir processos de inovação frente aos problemas globais..."
---

<div align="center">

![](https://i.imgur.com/p4VjPmo.jpg)

</div>


>*Entre os dias 7 e 9 de setembro, no Reino Unido, reuniram-se cerca de 500 pesquisadores para discutir processos de inovação frente aos problemas globais...*

<!--truncate-->

Entre os dias 7 e 9 de setembro, no Reino Unido, reuniram-se cerca de 500 pesquisadores para discutir processos de inovação frente aos problemas globais. Trata-se de um centro interdisciplinar, que é uma das principais referências internacionais em economia da inovação, política científica e tecnológica e estudos sociais da ciência. [Leia mais](http://revistapesquisa.fapesp.br/2016/10/20/a-hora-da-inovacao-transformadora/?cat=politica).