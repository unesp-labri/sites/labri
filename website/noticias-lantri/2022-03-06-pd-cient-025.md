---
slug: producao-cientifica-025
tags: [Lantri - Produção]
title: "A Construção do Pensamento sobre o Internacional na Rússia"
author: Isabella Silvério
tipo_publicacao: "Produção Cientica - 025"
image_url: https://i.imgur.com/3bMZSEh.png
descricao: "A compreensão sobre o internacional no século XX e o desenvolvimento do campo das Relações Internacionais ecoam particularidades do ambiente sociopolítico no qual são construídos..."
---

<div align="center">

![](https://i.imgur.com/3bMZSEh.png)

</div>



>*A compreensão sobre o internacional no século XX e o desenvolvimento do campo das Relações Internacionais ecoam particularidades do ambiente sociopolítico no qual são construídos...*

<!--truncate-->

A compreensão sobre o internacional no século XX e o desenvolvimento do campo das Relações Internacionais ecoam particularidades do ambiente sociopolítico no qual são construídos. Isso pode ser percebido no caso da Rússia, onde a construção científica do campo está atrelada a projetos de nação que buscam imitar o Outro Ocidental, ou ontrapor-se a ele. O objetivo deste artigo é compreender o quadro contemporâneo das escolas de pensamento sobre o internacional naquele país. Para tanto, serão incorporados conceitos da sociologia do conhecimento e do construtivismo aplicado às Relações Internacionais a partir da noção de epistemologias geoculturais. Análises matriciais sobre a literatura revista serão o principal recurso de comparação das classificações propostas pelas obras selecionadas, com vistas a entender o impacto dos diferentes contextos sociopolíticos sobre a Academia. Conclui-se que o caso russo é um interessante exemplo no qual é possível observar a confluência de inclinações identitárias, projetos político-pragmáticos e a constituição dessa área do saber em torno do elemento Ocidental.


Se interessa pelo tema? Para ter acesso ao artigo, basta [clicar aqui](https://www.cartainternacional.abri.org.br/Carta/article/view/1000).