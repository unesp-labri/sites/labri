---
slug: producao-cientifica-030
tags: [Lantri - Produção]
title: "Instituições políticas domésticas e cooperação internacional na díade Chile-Uruguai (1973-2000)"
author: Isabella Silvério
tipo_publicacao: "Produção Cientica - 030"
image_url: https://i.imgur.com/eQm8tJ0.png
descricao: "O objetivo deste artigo é o de analisar de forma comparativa os níveis de cooperação internacional na díade Chile-Uruguai em dois momentos distintos: entre 1973 e 1985, quando a díade era autocrática..."
---

<div align="center">

![](https://i.imgur.com/eQm8tJ0.png)

</div>



>*O objetivo deste artigo é o de analisar de forma comparativa os níveis de cooperação internacional na díade Chile-Uruguai em dois momentos distintos: entre 1973 e 1985, quando a díade era autocrática...*

<!--truncate-->

O objetivo deste artigo é o de analisar de forma comparativa os níveis de cooperação internacional na díade Chile-Uruguai em dois momentos distintos: entre 1973 e 1985, quando a díade era autocrática, e entre 1990 e 2000, quando a díade se tornou democrática. A partir do estudo de caso berfore-and-after se pretende testar a hipótese de que as Instituições Políticas Domésticas podem explicar os níveis de cooperação em uma díade. De modo a realizá-lo foram levantados os tratados assinados e ratificados por Chile e Uruguai entre 1973 e 2000, disponíveis no acervo online da Biblioteca del Congreso Nacional de Chile. Após, realizou-se pesquisa documental onde as áreas temáticas e o modelo de cooperação adotado foram destacados. Enfim, em ordem a julgar a intensidade da cooperação nos dois períodos destacados, foi comparado o número bruto de tratados firmados, as áreas temáticas onde se estabeleceram os acordos e o modelo de cooperação adotado.


Para acessar o artigo completo, [clique aqui](https://rephip.unr.edu.ar/handle/2133/15201). 