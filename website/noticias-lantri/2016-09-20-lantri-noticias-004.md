---
slug: lantri-noticias-004
tags: [Lantri - Notícias]
title: "USP abre acesso à nuvem computacional"
author: Equipe Lantri
tipo_publicacao: "Lantri Notícias - 004"
image_url: https://i.imgur.com/jFgQgov.jpg
descricao: "A Universidade de São Paulo (USP) abriu o acesso à nuvem computacional para pesquisadores vinculados a qualquer universidade ou instituição de pesquisa realizarem seus projetos..."
---

<div align="center">

![](https://i.imgur.com/jFgQgov.jpg)

</div>


>*A Universidade de São Paulo (USP) abriu o acesso à nuvem computacional para pesquisadores vinculados a qualquer universidade ou instituição de pesquisa realizarem seus projetos...*

<!--truncate-->

 A Universidade de São Paulo (USP) abriu o acesso à nuvem computacional para pesquisadores vinculados a qualquer universidade ou instituição de pesquisa realizarem seus projetos. Para acessar a noticia completa, [clique aqui](http://agencia.fapesp.br/usp_abre_acesso_a_nuvem_computacional_/23839/).