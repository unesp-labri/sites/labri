---
slug: voce-sabia-000
tags: [Lantri - Você Sabia]
title: "WTF is link rot?"
author: Equipe LANTRI
tipo_publicacao: "Você Sabia - 000"
image_url: https://i.imgur.com/Eu0TBao.png
descricao: "Você sabe o que significa ''link rot''? O texto em questão mostra a importância de coletar ou salvar em algum local apropriado paginas web que são importantes para seu trabalho..."
---

<div align="center">

![](https://i.imgur.com/Eu0TBao.png)

</div>


>Você sabe o que significa ''link rot''? O texto em questão mostra a importância de coletar ou salvar em algum local apropriado paginas web que são importantes para seu trabalho...*

<!--truncate-->

Você sabe o que significa ''link rot''? O texto em questão mostra a importância de coletar ou salvar em algum local apropriado paginas web que são importantes para seu trabalho seja no meio acadêmico, corporativo, governamental ou em ONGs.

Links na internet podem vir a ''perder a válidade'', ou seja deixarem de funcionar. Saiba mais sobre as causas e consequências disso, assim como possíveis soluções para o problema, [clicando aqui](https://digiday.com/media/wtf-link-rot/). 