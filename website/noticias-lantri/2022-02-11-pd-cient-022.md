---
slug: producao-cientifica-022
tags: [Lantri - Produção]
title: "Desintegração econômica e fragmentação da governança regional na América do Sul em tempos de Covid19"
author: Isabella Silvério
tipo_publicacao: "Produção Cientica - 022"
image_url: https://i.imgur.com/dASV6N7.png
descricao: "O objetivo deste artigo é analisar os efeitos da pandemia da Covid-19 sobre o comércio e a governança da América do Sul..."
---

<div align="center">

![](https://i.imgur.com/dASV6N7.png)

</div>



>*O objetivo deste artigo é analisar os efeitos da pandemia da Covid-19 sobre o comércio e a governança da América do Sul...*

<!--truncate-->

O objetivo deste artigo é analisar os efeitos da pandemia da Covid-19 sobre o comércio e a governança da América do Sul. A hipótese é que a crise gerada pela Covid-19 acentuou as tendências anteriores de desintegração econômica e fragmentação política. Analisa-se como organizações regionais da África, América Central, América do Sul e Europa responderam à Covid-19. No caso da América do Sul, avalia-se o papel do Foro para o Progresso e Desenvolvimento da América do Sul (Prosul) para oferecer respostas à crise, considerando as iniciativas desde o surgimento da crise, sua agenda para Saúde, as interações entre os governos e seu arcabouço institucional. Avalia-se a dinâmica comercial intrarregional, com ênfase nas exportações e importações do Brasil com os países vizinhos. Conclui-se que há uma espiral entre desintegração comercial e fragmentação da governança, que se retroalimenta e tende a prolongar a crise na América do Sul mais que em outras regiões do mundo.


Se interessa pelo tema? Para ter acesso ao artigo, basta [clicar aqui](http://repositorio.ipea.gov.br/handle/11058/10337).