---
id: equipe
title: Apresentação da equipe LabRI/UNESP
sidebar_label: Atual
slug: /equipe
---

## Marcelo Passini Mariano

<img className="img-equipe-foto" src="/img/equipe/marcelo.jpg"/>

<div className="img-equipe-redes">
<a href="http://lattes.cnpq.br/3505849964932313"> <img className="img-icon-redes" src="/img/social/lattes.png" /> </a>

<a href="https://scholar.google.com.br/citations?hl=pt-BR&user=UFsVIxQAAAAJ"> <img className="img-icon-redes" src="/img/social/google.png"/> </a>

</div>

<div style={{textAlign: 'center'}}>

**Coordenador**

*2012 - atual*

Possui graduação em Ciências Sociais pela Universidade de São Paulo (1992), mestrado em Ciência Política pela Universidade de São Paulo (1998) e doutorado em Sociologia pela Universidade Estadual Paulista (2007). Professor Assistente Doutor do curso de Relações Internacionais da Faculdade de Ciências Humanas e Sociais da Universidade Estadual Paulista - UNESP/Campus de Franca. Professor do Programa de Pós-Graduação San Tiago Dantas (UNESP/UNICAMP/PUC-SP). Coordenador do Laboratório de Novas Tecnologias de Pesquisa em Relações Internacionais (LANTRI /FCHS /UNESP), que integra a Rede de Pesquisa em Política Externa e Regionalismo (REPRI). É pesquisador do Centro de Estudos de Cultura Contemporânea (CEDEC). Tem experiência na área de Relações Internacionais com pesquisas nos seguintes temas: processos de integração regional (Mercosul, Alca, Relação Mercosul União Européia e integração na América Latina), política externa brasileira, controle democrático de política externa, processos de tomada de decisão e gestão de relações internacionais de governos não-centrais (paradiplomacia), técnicas de pesquisa em ciências humanas e o uso das tecnologias de informação e comunicação.

</div>

## Rafael de Almeida

<img className="img-equipe-foto" src="/img/equipe/rafael.png"/> 

<div className="img-equipe-redes">
<a href="http://lattes.cnpq.br/5174307461578307"> <img className="img-icon-redes" src="/img/social/lattes.png" /> </a>

<a href="https://www.linkedin.com/in/rafael-augusto-ribeiro-de-almeida-083792137/"> <img className="img-icon-redes" src="/img/social/linkedin.png"/> </a>

</div>


<div style={{textAlign: 'center'}}>

**Colaborador**

*2012 - atual*

Mestre em Relações Internacionais pela Universidade Federal de Uberlândia (UFU). Possui graduação em Relações Internacionais pela Universidade Estadual Paulista "Julio de Mesquita Filho", Campus Franca (2012). Membro do Laboratório de Novas Tecnologias de Pesquisa em Relações Internacionais (LANTRI/UNESP), que integra a Rede de Pesquisa em Política Externa e Regionalismo (REPRI). Entre 2008 e 2011 foi redator-colaborador do Observatório de Política Externa Brasileira (OPEB) vinculado ao Grupo de Estudos de Defesa e Segurança Internacional (GEDES/UNESP). Foi assistente de pesquisa no Centro de Estudos de Cultura Contemporânea (CEDEC). Na graduação foi bolsista PIBIC/CNPq e, posteriormente, bolsista FAPESP. No mestrado foi bolsista FAPEMIG.

</div>

## Breno Henry da Rocha Andreazza

<img className="img-equipe-foto" src="/img/equipe/breno-andreazza.png"/>

<div className="img-equipe-redes">
<a href="https://www.behance.net/brenohenry"> <img className="img-icon-redes" src="/img/social/behance.png"/> </a>

<a href="https://www.instagram.com/brenohda?igsh=Z3NrODVmOTJkZzF6"> <img className="img-icon-redes" src="/img/social/instagram.png"/> </a>

</div>

<div style={{textAlign: 'center'}}>

**Estagiário Remunerado**

*2024 - atual*

Graduando em Relações Internacionais pela Universidade Estadual Paulista (UNESP). Técnico em Informática para Internet pela ETEC de Santa Fé do Sul.

</div>

## Leonardo de Almeida Petrilli

<img className="img-equipe-foto" src="/img/equipe/leonardo-petrilli.jpg"/>

<div className="img-equipe-redes">
<a href="https://www.linkedin.com/in/leonardopetrilli/"> <img className="img-icon-redes" src="/img/social/linkedin.png"/> </a>

<a href="https://github.com/Hakeera"> <img className="img-icon-redes" src="/img/social/github.png" /> </a>

</div>

<div style={{textAlign: 'center'}}>

**Estagiário Remunerado**

*2024 - atual*

Graduado em Relações Internacionais pela Universidade Estadual Paulista (UNESP). Bolsista de apoio técnico (edital PROPE-UNESP Nº 16/2023). Desenvolver full-stack em projetos de coleta e análise de dados, sistemas de gestão, data entrys, BI e arquitetura de softwares. Desenvolve em VBA, Python, HTML5 e CSS.

</div>



## Henédio Bernardino Pedrosa Neto

<img className="img-equipe-foto" src="/img/equipe/henedio-neto.jpg"/>

<div className="img-equipe-redes">
<a href="https://gitlab.com/HenedioNeto"> <img className="img-icon-redes" src="/img/social/gitlab.png"/> </a>

<a href="https://github.com/HenedioNeto"> <img className="img-icon-redes" src="/img/social/github.png" /> </a>

</div>

<div style={{textAlign: 'center'}}>

**Estagiário Remunerado**

*2024 - atual*

Bolsista de apoio técnico (edital PROPE-UNESP Nº 16/2023).

</div>



## Vinicius Soares Campos

<img className="img-equipe-foto" src="/img/equipe/vinicius-campos.jpg"/>

<div className="img-equipe-redes">
<a href="https://www.linkedin.com/in/vinicius-campos-7a9379288"> <img className="img-icon-redes" src="/img/social/linkedin.png"/> </a>

<a href="https://www.instagram.com/vinicampos_?igsh=dXJ3OTM1cWZpMmRn"> <img className="img-icon-redes" src="/img/social/instagram.png"/> </a>

</div>

<div style={{textAlign: 'center'}}>

**Estagiário Voluntário**

*2024 - atual*

Graduando em Relações Internacionais pela Universidade Estadual Paulista (UNESP). Membro da Enactus - Unesp Franca. Membro do POD-RI

</div>

## Giovanna Aparecida Doriguetto Souza 

<img className="img-equipe-foto" src="/img/equipe/giovanna-doriguetto.jpeg"/>

<div className="img-equipe-redes">
<a href="https://www.linkedin.com/in/giovannadoriguetto?utm_source=share&utm_campaign=share_via&utm_content=profile&utm_medium=ios_app"> <img className="img-icon-redes" src="/img/social/linkedin.png"/> </a>

</div>

<div style={{textAlign: 'center'}}>

**Estagiário Voluntário**

*2024 - atual*

Graduanda em Relações Internacionais pela Universidade Estadual Paulista "Júlio de Mesquita Filho" (UNESP), localizada no câmpus de Franca, em São Paulo. É Representante Discente do Departamento de Relações Internacionais (DERI) da Unesp-Franca (2024-2025) e Coordenadora de Política Interna do Centro Acadêmico de Relações Internacionais "João Cabral de Melo Neto" (Chapa Jorge Amado 2024-2025). Além disso, é pesquisadora e membro do grupo de pesquisa Núcleo de Estudos em Políticas Públicas "Elza de Andrade de Oliveira" (NEPPs) e Secretária Acadêmica do Grupo de Estudos, Pesquisa e Simulações de Organizações Internacionais (GEPESOI). Durante o ano de 2023, participou da coordenadoria da Polícia Federal do grupo de extensão Rede de Apoio ao Migrante Internacional (RAMIN). 

</div>

## Lucas Buzatto dos Santos 

<img className="img-equipe-foto" src="/img/equipe/lucas-buzatto.jpg"/>

<div className="img-equipe-redes">
<a href="https://www.linkedin.com/in/lucas-buzatto-3689b8231?utm_source=share&utm_campaign=share_via&utm_content=profile&utm_medium=android_app"> <img className="img-icon-redes" src="/img/social/linkedin.png"/> </a>

</div>

<div style={{textAlign: 'center'}}>

**Estagiário Voluntário**

*2024 - atual*

Graduando em Relações Internacionais pela Unesp-FCHS  

</div>

## Laura Julião Lisboa  

<img className="img-equipe-foto" src="/img/equipe/laura-juliao.jpeg"/>

<div className="img-equipe-redes">
<a href="https://www.instagram.com/lajuliao?utm_source=ig_web_button_share_sheet&igsh=ZDNlZDc0MzIxNw=="> <img className="img-icon-redes" src="/img/social/instagram.png"/> </a>

</div>

<div style={{textAlign: 'center'}}>

**Estagiário Voluntário**

*2024 - atual*

Graduando em Relações Internacionais pela Unesp-FCHS 

</div>