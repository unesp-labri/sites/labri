---
id: grupos-trabalho
title: Equipe Pandemia e as Relações Internacionais
sidebar_label: Equipe
slug: /projetos/extensao/covid19/grupos-trabalho
---

:::tip
O projeto Pandemia e as Relações Internacionais contou com uma equipe de docentes e discentes vinculados ao curso de Relações Internacionais da UNESP/Franca. Além disso, os participantes foram divididos em três GTs (I, II e III), além de quatro equipes de apoio: Equipe de Coordenação, Equipe de Comunicação/Gestão de Conteúdo, Equipe de Editorial/Curadoria e Equipe de Divulgação Científica/Técnica. 
:::

### GT1: Questões da conjuntura do sistema internacional

O GT1 foi responsável por quatro frentes gerais:

- Cooperação internacional, o papel das potências mundiais, as instituições internacionais

- Economia internacional e relações comerciais, a pandemia e o sistema neoliberal

- Questões geopolíticas e militares, o armamentismo clássico

- Lições para o sistema internacional

Os membros se dividiram em três cargos: coordenação, representes e participantes (redatores).

- **Coordenação**: Cristiano Manhães, Vinícius Guimarães.

- **Representantes**: Matheus Eduardo, Mayara Zorzo, Sofia Navarro.

- **Participantes**: Ana Júlia Diniz Neves do Lago, Bianca Poltronieri, Isabel Gonçalves, Jonas Vieira, Júlia Coleone, Lucas Quispe, Maria Paula Morabito, Matheus Eduardo, Mayara Zorzo, Sofia Navarro,  Carlos Leandro Zanotti Guizzo, Julia Robaina Fernandes, Bruna Aveiro Santos, Joyce Victor Felix, Gabriela Guillardi,  Nicole Mourad Pereira, Rafael Menguer Bykowski dos Santos, Pedro Otávio Passos Rapace e Gabriela Ritter Brigido.

### GT2: Grupos vulneráveis, experiências autoritárias e saúde global

O GT2 foi responsável por três frentes gerais: 

- Ações de governos autoritários e a relação entre autoritarismo e democracia

- Saúde global e a pandemia

- Impactos da pandemia nos grupos mais vulneráveis, questões sociais, raciais e de gênero

Os membros se dividiram em dois cargos:representantes e participantes (redatores).

- **Representantes**: Mayara Zorzo, Matheus Valencia, Lívia Dutra, Sofia Navarro.

- **Participantes**:  Ana Júlia, André Tolino, Gabriela Alencar Araujo, Gustavo Castejon, Julia Coleone, Júlia Lourenço, Lívia Dutra, Lucas Quispe, Maria Paula Morabito,  Matheus Eduardo, Matheus Novais, Sofia Navarro, Maria Cecília Silva Santos, Cíntia Paulena Di Iório, Eduarda Campos Bormanas, Julia Robaina Fernandes, Enila dos Santos Araujo e Léa Briese Staschower.

### GT3: Tecnologias na pandemia e privacidade de dados

O GT3 foi responsável por três frentes gerais: 

- Os diversos usos da tecnologia na epidemia

- Privacidade de dados e controle de dados, relação entre democracia e tecnologias de controle social

- Direito internacional e propriedade intelectual, quebra de patente

Os membros se dividiram em três cargos: coordenação, representantes e participantes.

- **Coordenação**: Cristiano Manhães

- **Representante**: Isabela Bortoloto

- **Participantes**: Isabela Bortoloto, Bárbara Figueiredo, Diego Malvasio Bertolino, Pedro Henrique do Prado Haram Colucci, Jady Jasmin Cardoso e Rafael Menguer dos Santos. 

### Equipe de Coordenação

A Equipe de Coordenação tinha como objetivo avaliar e acompanhar o andamento das atividades de cada GT, auxiliar no cumprimento das metas estabelecidas, ajudar na comunicação interna do projeto, reportar a todos o andamento geral das atividades.

Membros: 

- Cristiano Manhães

- Marcelo Mariano

- Mayara Zorzo

- Pedro Henrique

- Rafael Almeida

- Vinícius Guimarães. 

### Equipe de Comunicação/Gestão de Conteúdo

A Equipe de Comunicação foi responsável pela página web e redes sociais (YouTube, Facebook, Twitter), produção de entrevistas com especialistas da área e de prospecção de parceria com novos projetos.

Membros: 

- Bárbara Garcia de Figueiredo

- Isabela Bortoloto

- Livia Dutra

- Matheus Eduardo

- Wuara Laiane Alves Antezana

### Equipe de Editorial/Curadoria

A Equipe de Curadoria foi responsável pela verificação de fake news, avaliação contínua dos critérios científicos, avaliação da comunicação institucional.

Membros: 

- Cristiano Manhães

- Sofia Navarro

- Amanda Victória Souza

- Bruna de Souza Luiz

- Léa Briese Staschower

### Equipe de Divulgação Científica/Técnica

A Equipe de Divulgação Científica tinha como responsabilidade ajudar os GTs no levantamento de dados, produção de gráficos, infográficos, mapas e linhas do tempo

Membros: 

- Pedro Henrique

- Rafael Almeida


