---
id: equipe
title: Equipe 
sidebar_label: Equipe
slug: /projetos/extensao/conhecer-para-acolher/equipe
---

## Membro 01

<img className="img-equipe-foto" src="/img/equipe/logo-labri.jpg"/>

<div className="img-equipe-redes">
<a href="http://lattes.cnpq.br"> <img className="img-icon-redes" src="/img/social/lattes.png" /> </a>

<a href="https://scholar.google.com.br"> <img className="img-icon-redes" src="/img/social/google.png"/> </a>

</div>

## Membro 02

<img className="img-equipe-foto" src="/img/equipe/logo-labri.jpg"/>

<div className="img-equipe-redes">
<a href="http://lattes.cnpq.br"> <img className="img-icon-redes" src="/img/social/lattes.png" /> </a>

<a href="https://scholar.google.com.br"> <img className="img-icon-redes" src="/img/social/google.png"/> </a>

</div>

## Membro 03

<img className="img-equipe-foto" src="/img/equipe/logo-labri.jpg"/>

<div className="img-equipe-redes">
<a href="http://lattes.cnpq.br"> <img className="img-icon-redes" src="/img/social/lattes.png" /> </a>

<a href="https://scholar.google.com.br"> <img className="img-icon-redes" src="/img/social/google.png"/> </a>

</div>

##  Membro 04 

<img className="img-equipe-foto" src="/img/equipe/logo-labri.jpg"/>

<div className="img-equipe-redes">
<a href="http://lattes.cnpq.br"> <img className="img-icon-redes" src="/img/social/lattes.png" /> </a>

<a href="https://scholar.google.com.br"> <img className="img-icon-redes" src="/img/social/google.png"/> </a>

</div>

##  Membro 05 

<img className="img-equipe-foto" src="/img/equipe/logo-labri.jpg"/>

<div className="img-equipe-redes">
<a href="http://lattes.cnpq.br"> <img className="img-icon-redes" src="/img/social/lattes.png" /> </a>

<a href="https://scholar.google.com.br"> <img className="img-icon-redes" src="/img/social/google.png"/> </a>

</div>

##  Membro 06

<img className="img-equipe-foto" src="/img/equipe/logo-labri.jpg"/>

<div className="img-equipe-redes">
<a href="http://lattes.cnpq.br"> <img className="img-icon-redes" src="/img/social/lattes.png" /> </a>

<a href="https://scholar.google.com.br"> <img className="img-icon-redes" src="/img/social/google.png"/> </a>

</div>

## Membro 07 

<img className="img-equipe-foto" src="/img/equipe/logo-labri.jpg"/>

<div className="img-equipe-redes">
<a href="http://lattes.cnpq.br"> <img className="img-icon-redes" src="/img/social/lattes.png" /> </a>

<a href="https://scholar.google.com.br"> <img className="img-icon-redes" src="/img/social/google.png"/> </a>

</div>

## Membro 08 

<img className="img-equipe-foto" src="/img/equipe/logo-labri.jpg"/>

<div className="img-equipe-redes">
<a href="http://lattes.cnpq.br"> <img className="img-icon-redes" src="/img/social/lattes.png" /> </a>

<a href="https://scholar.google.com.br"> <img className="img-icon-redes" src="/img/social/google.png"/> </a>

</div>