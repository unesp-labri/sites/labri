---
id: id-visual
title: Identidade Visual
sidebar_label: Identidade Visual
slug: /projetos/extensao/pod-ri/id-visual
---

### Sobre o logo

- **Fontes**: Bobby Jones Condensed
- **Cores**: #D8D8D8, #F1F1F1, #53263B
