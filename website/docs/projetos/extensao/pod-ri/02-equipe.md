---
id: equipe
title: Equipe Pod-RI
sidebar_label: Equipe Pod-RI
slug: /projetos/extensao/pod-ri/equipe
---

Atualmente a equipe do Pod-RI é composta por onze graduandos de Relações Internacionais da UNESP/Franca.


## Ailton Salvadori

<img className="img-equipe-foto" src="/img/pod-ri/equipe-ailton-salvadori.png"/>

<div className="img-equipe-redes">
<a href="https://www.instagram.com/ailtonsalva/"> <img className="img-icon-redes" src="/img/social/instagram.png" /> </a>

<a href="https://www.facebook.com/ailton.salvadori"> <img className="img-icon-redes" src="/img/pod-ri/logo-fb-cor.png"/> </a>

</div>

## Bianca Cintra da Costa Antunes

<img className="img-equipe-foto" src="/img/pod-ri/equipe-bianca-cintra.png"/>

<div className="img-equipe-redes">
<a href="https://www.instagram.com/bia_antunes92/?utm_medium=copy_link"> <img className="img-icon-redes" src="/img/social/instagram.png" /> </a>

<a href="https://www.facebook.com/bianca.antunes.758"> <img className="img-icon-redes" src="/img/pod-ri/logo-fb-cor.png"/> </a>

</div>

## Bruno Cesar David Ruy

<img className="img-equipe-foto" src="/img/pod-ri/equipe-bruno-cesar.png"/>

<div className="img-equipe-redes">
<a href="https://www.instagram.com/brunoruyyy/"> <img className="img-icon-redes" src="/img/social/instagram.png" /> </a>

<a href="https://www.facebook.com/bruno.ruy.99"> <img className="img-icon-redes" src="/img/pod-ri/logo-fb-cor.png"/> </a>

</div>

##  Danielle Elis Alves Valdivia 

<img className="img-equipe-foto" src="/img/pod-ri/equipe-danielle-valdivia.png"/>

<div className="img-equipe-redes">
<a href="https://www.linkedin.com/in/daniellevaldiviaunesp/"> <img className="img-icon-redes" src="/img/social/linkedin.png" /> </a>

<a href="https://www.facebook.com/danielle.a.valdivia/"> <img className="img-icon-redes" src="/img/pod-ri/logo-fb-cor.png"/> </a>

</div>

##  Gustavo Pasqueta

<img className="img-equipe-foto" src="/img/pod-ri/equipe-gustavo-pasqueta.png"/>

<div className="img-equipe-redes">
<a href="https://www.instagram.com/gustapasqueta/"> <img className="img-icon-redes" src="/img/social/instagram.png" /> </a>

<a href="https://www.linkedin.com/in/gustavo-pasqueta/"> <img className="img-icon-redes" src="/img/social/linkedin.png"/> </a>

</div>

## Leonardo Pagano Landucci 

<img className="img-equipe-foto" src="/img/pod-ri/equipe-leonardo-pagano.png"/>

<div className="img-equipe-redes">
<a href="https://www.instagram.com/leo_landucci28/?utm_medium=copy_link"> <img className="img-icon-redes" src="/img/social/instagram.png" /> </a>

<a href="http://lattes.cnpq.br/1338768819301424"> <img className="img-icon-redes" src="/img/social/lattes.png"/> </a>

</div>

## Samuel Davis Domingues Lima 

<img className="img-equipe-foto" src="/img/pod-ri/equipe-samuel-davis.png"/>

<div className="img-equipe-redes">
<a href="https://www.instagram.com/samueldavisd/"> <img className="img-icon-redes" src="/img/social/instagram.png" /> </a>

<a href="https://www.linkedin.com/in/samuel-davis-domingues-lima-61843b1b3/"> <img className="img-icon-redes" src="/img/social/linkedin.png"/> </a>

</div>

## Sofia Navarro Aguiar 

<img className="img-equipe-foto" src="/img/pod-ri/equipe-sofia-navarro.png"/>

<div className="img-equipe-redes">
<a href="http://lattes.cnpq.br/8344699371908069"> <img className="img-icon-redes" src="/img/social/lattes.png" /> </a>

<a href="https://www.linkedin.com/in/sofia-navarro-aguiar-23a435a3/"> <img className="img-icon-redes" src="/img/social/linkedin.png"/> </a>

</div>

## Stephany Anicezio de Souza Primo 

<img className="img-equipe-foto" src="/img/pod-ri/equipe-stephany-anicezio.png"/>

<div className="img-equipe-redes">
<a href="https://www.instagram.com/stephyanicezio/?hl=pt-br"> <img className="img-icon-redes" src="/img/social/instagram.png" /> </a>

<a href="https://www.linkedin.com/in/stephanyanicezio/"> <img className="img-icon-redes" src="/img/social/linkedin.png"/> </a>

</div>

## Victoria dos Anjos Gois

<img className="img-equipe-foto" src="/img/pod-ri/equipe-victoria-gois.png"/>

<div className="img-equipe-redes">
<a href="https://www.instagram.com/torigois/"> <img className="img-icon-redes" src="/img/social/instagram.png" /> </a>

<a href="https://www.linkedin.com/in/victoriagois/"> <img className="img-icon-redes" src="/img/social/linkedin.png"/> </a>

</div>
