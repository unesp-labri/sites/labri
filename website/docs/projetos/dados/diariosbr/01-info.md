---
id: info
title: Apresentação DiáriosBR
sidebar_label: Apresentação
slug: /projetos/dados/diariosbr/info
---

<center>
    <img src="/img/projetos/dados/diariosbr/diariosbr.svg" alt="centered image" width="200" height="300" />
</center>

Os **Diários Oficiais** são onde toda a movimentação legal dos governos federal, estadual e municipal são publicadas, sendo esse fator o que os tornam uma fonte de pesquisa importante para o acompanhamento da destinação de recursos, transferência de cargos e o embasamento legal das atividades da administração pública. 

✅ Para acessar o *repositório* do projeto, [clique aqui](https://gitlab.com/unesp-labri/projeto/diariosbr).

✅ Para acessar a pasta do *google drive*, [clique aqui](https://drive.google.com/drive/u/0/folders/1ffm8RaW8LoC1Xq12TdMqIEeikepB_pCJ).

### Informações de Coleta

|Cidade|Período|Status|Plataforma|
|------|-------|------|----------|
|[Aramina](https://www.imprensaoficialmunicipal.com.br/aramina)|27/9/2022 - atual| :heavy_check_mark: (PDF)|[Diário Oficial Eletrônico (DiOE)](https://dosp.com.br/)|
|[Buritizal](https://www.imprensaoficialmunicipal.com.br/buritizal)|26/10/2022 - atual|:heavy_check_mark: (PDF)|[Diário Oficial Eletrônico (DiOE)](https://dosp.com.br/)|
|[Guará](https://imprensaoficialmunicipal.com.br/guara)|12/12/2014 - atual|:heavy_check_mark: (PDF) |[Diário Oficial Eletrônico (DiOE)](https://dosp.com.br/)|
|[Igarapava](https://imprensaoficialmunicipal.com.br/igarapava)|10/10/2019 - atual|:heavy_check_mark: (PDF) | [Diário Oficial Eletrônico (DiOE)](https://dosp.com.br/)|
|[Ipuã](https://imprensaoficialmunicipal.com.br/ipua)|12/12/2019 - atual|:heavy_check_mark: (PDF)|[Diário Oficial Eletrônico (DiOE)](https://dosp.com.br/)|
|[Ituverava](https://imprensaoficialmunicipal.com.br/ituverava)| 08/12/2022 - atual| :heavy_check_mark: (PDF)|[Diário Oficial Eletrônico (DiOE)](https://dosp.com.br/)|
|[Miguelópolis](https://www.miguelopolis.sp.gov.br/paginas/portal/diarioOficial)*|29/09/2017 - atual| :heavy_check_mark: (PDF)|Plataforma Própria (Site da Prefeitura) |
|[Morro Agudo](https://imprensaoficialmunicipal.com.br/morro_agudo)| 24/01/2017 - atual|:heavy_check_mark: (PDF) | [Diário Oficial Eletrônico (DiOE)](https://dosp.com.br/)|
|[Orlândia](http://www.orlandia.sp.gov.br/novo/jornal-oficial-de-orlandia)*| 04/11/2014 - atual| :heavy_check_mark: (PDF)|Plataforma Própria (Site da Prefeitura) |
|[Pedregulho](http://domunicipal.com.br/ir.php?id_orgao=2)*| 08/02/2018 - atual|:heavy_check_mark: (PDF)|[Diário Oficial Municipal](https://domunicipal.com.br/)|
|[São Joaquim da Barra](https://www.imprensaoficialmunicipal.com.br/sao_joaquim_da_barra)|26/05/2017 - atual|:heavy_check_mark: (PDF) | [Diário Oficial Eletrônico (DiOE)](https://dosp.com.br/)|
|[São José da Bela Vista](https://imprensaoficialmunicipal.com.br/sjbelavista)|08/03/2021 - atual|:heavy_check_mark: (PDF) | [Diário Oficial Eletrônico (DiOE)](https://dosp.com.br/)|
|[Rifaina](https://imprensaoficialmunicipal.com.br/rifaina)| 02/04/2024 - atual |:heavy_check_mark: (PDF)|[Diário Oficial Eletrônico (DiOE)](https://dosp.com.br/)|
|[Cristais Paulista](http://domunicipal.com.br/ir.php?id_orgao=3)| 12/03/2018 - atual|:heavy_check_mark: (PDF)|[Diário Oficial Municipal](https://domunicipal.com.br/)|
|[Itirapuã](http://domunicipal.com.br/ir.php?id_orgao=5)| 01/07/2020 - atual|:heavy_check_mark: (PDF)|[Diário Oficial Municipal](https://domunicipal.com.br/)|
|[Restinga](http://domunicipal.com.br/ir.php?id_orgao=1)| 13/04/2021 - atual|:heavy_check_mark: (PDF)|[Diário Oficial Municipal](https://domunicipal.com.br/)|
|[Patrocínio Paulista](https://www.patrociniopaulista.sp.gov.br/portal/diario-oficial)|18/08/2017 - atual|:heavy_check_mark: (PDF) |Plataforma Própria (Site da Prefeitura) |
|[Sales Oliveira](https://www.salesoliveira.sp.gov.br/portal/diario-oficial)|16/01/2024 - atual|:heavy_check_mark: (PDF) |Plataforma Própria (Site da Prefeitura)|
|[Batatais](https://www.batatais.sp.gov.br/diariooficial/exibirDiario/)|03/08/2021 - atual|:heavy_check_mark: (PDF) |Plataforma Própria (Site da Prefeitura) |
|[Franca](https://www.franca.sp.gov.br/pmf-diario/)|08/04/2014 - atual| |Plataforma Própria (Site da Prefeitura) |
|[Ribeirão Corrente](https://sistemas.eddydata.com/pmribeirao-corrente/diario-oficial/#/doe/ribeiraocorrente)|09/04/2021 - atual| |Plataforma Própria (Site da Prefeitura) |
|[Jeriquara](https://jeriquara.sp.gov.br/dom/)|02/01/2018  - atual| |Plataforma Própria (Site da Prefeitura) |
|[Nuporanga](https://www.nuporanga.sp.gov.br/administracao)|---|---|Plataforma Própria (Site da Prefeitura)|


|Origem|Período|Status|Plataforma|
|------|-------|------|----------|
|Diário Oficial da União|1990-atual|:heavy_check_mark: (PDF)||
|Diário oficial da Câmara Federal||||
|Diário Oficial do Senado Federal||||
|Diário Oficial do Estado de SP||||
|Diário Oficial do Legislativo de SP||||




