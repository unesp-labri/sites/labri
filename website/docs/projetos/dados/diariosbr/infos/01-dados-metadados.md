---
id: dados-metadados
title: Documentação DiáriosBR
sidebar_label: Dados e Metadados
slug: /projetos/dados/diariosbr/infos/dados-metadados
---

### Dados e Metadados

|Váriavel|Descrição|Exemplos|
|---|---|---|
|`tipo_dado`|Indica a restrição do dado|`Público` ou `Privado`|
|`plataforma`| Indica a plataforma que disponibiliza o documento| `Diário Oficial Eletrônico (DiOE)` ou `Diário Oficial Municipal` ou `Site da Prefeitura`|
|`origem`| nome do município de origem|`São Joaquim da Barra`|
|`origem_lower`| nome do município de origem em caixa baixa |`saojoaquimdabarra`|
|`cod_ibge`| Código do Município no IBGE|`7089`|
|`categoria`|Indica se é uma edição rotineira ou excepcional |`Ordinário` ou `Extraordinário`|
|`data`|Define a data da edição|`dd/mm/yyyy`|
|`num_ed`||Número da Edição|
|`nome_arquivo` | Nome do arquivo|
|`link_pdf`|Link original do pdf|
|`coleta_pdf`|Verifica se a coleta já foi realizada|`True` ou `False`|
|`dir_bd` |dir_bd|`/media/hdvm04/`|
|`codigo_bd`|Indicação da identificação local|`bd/001/002/0xx`|
|`dir_local`|Caminho absoluto da pasta |`dir_db` + `codigo_bd`|
|`arq_local_pdf` |Caminho absoluto do arquivo .pdf |`/caminho/abosluto/arquivo.pdf`|
|`arq_local_json` | Caminho absoluto do arquivo .json|`/caminho/abosluto/arquivo.json`|


Para identificar os *diários coletados* utilizamos a [tabela de Códigos dos municípios IBGE](https://www.ibge.gov.br/explica/codigos-dos-municipios.php)

