---
id: banco-termos
title: Documentação DiáriosBR
sidebar_label: Banco de Termos
slug: /projetos/dados/diariosbr/infos/banco-termos
---

### Franca - Assistência Social

```
("Conselho Municipal de Assistência Social" OR CMAS) OR
("Lei Orgânica de Assistência Social" OR LOAS) OR
("Cadástro Único") OR
("Sistema Único de Assitência Social" OR SUAS) OR
("Família Acolhedora") OR ???? ("Proteção Social Básica") OR
("Proteção Social Especial") OR
("Secretaria de Ação Social" OR SEDAS) OR
("Centro de Referência Especializado em Assistência Social" OR CREAS) OR
("Centro-Pop") OR
("Ateliê da Família") OR ???? ("Abrigo Provisório Antônio de Carvalho") OR ????? ("Centro de Referência de Assistência Social" OR CRAS) OR
("Conselho Tutelar") OR
("Secretaria Municipal dos Direitos da Criança e do Adolescente")
```

### Franca - Educação

```
Educação basica - Ensino Fundamental >> VERIFICAR

("Ensino Infantil") OR
("Ensino Fundamental") OR
("Ensino Superior") OR
("Ensino Técnico") OR
("Ensino Musical") OR
("Creches") OR
("Secretaria Municipal de Educação") OR
("Escola Municipal de Educação Básica" OR EMEB) OR ????
("Escola Municipal de Educação Infantil" OR EMEI) OR ????? ("Educação de Jovens e Adultos" OR EJA) OR
("Alfabetização de Jovens e Adultos" OR AJA) OR ????
("Centro de Ensino Supletivo Municipal" OR CESUM) OR ??? "Educação Supletivo" ("Escola Municipal de Iniciação Musical" OR EMEI) OR
("Escolas Municipais") OR ????? ("Cardápio Merenda Escolar") OR ???? ("Cardápios Creches") OR ????? ("Secretário da Educação") OR ????? ("Uniforme Escolar") OR ????? ("Secretaria Escolar Digital" OR SED) OR ?????
("Faculdade de Direito de Franca" OR FDF) OR ????? ("Centro Universitário Municipal de Franca" OR UNIFACEF) OR ?????
("Universidade Aberta do Brasil" or UAB) OR ?????? ("Universidade Virtual do Estado de São Paulo" OR UNIVESP) OR ?????
("Índice de Desenvolvimento da Educação Básica" OR IDEB) OR
("Bolsa Universidade") OR ?????? ("Cursinho Popular") OR ????? ("Vamos Aprender") OR ????? ("Fundo Nacional de Educação Básica" OR FUNDEB)
```

### Franca - Termos Orçamentários

```
("Lei Orçamentária Anual" OR LOA) OR
("Lei de Responsabilidade Fiscal" OR LRR) OR
("Plano Plurianual" OR PPA) OR
("Lei de Diretrizes Orçamentárias" OR LDO) OR
("Fundo de Participação dos Estados" OR FPE) OR
("repasse de duodécimos" OR duodécimo) OR ????? ("Fundo de Participação dos Municipios" OR FPM) OR ??????
("Receita Tributária") OR ????? ("Balancete Mensais") OR ????? VERIFICAR SE TERMOS ORÇAMENTÁRIOS RELEVANTES ("Balancete de Fluxo de Caixa") OR ???? ("Balancete da Despesa") OR ?????? ("Balancete da Receita") OR ????? ("Balancete Financeiro") OR ???? ("Balancete Orçamentário") OR ????? ("Balanço do Exercício") OR ?????? ("Balanço Financeiro") OR ????? ("Balanço Orçamentário") OR ???? ("Previsão de Receitas") OR ????? ("Receitas Previstas") OR ????? ("Receitas Autorizadas") OR ????? ("Receitas Realizadas") OR ????? ("Despesas Autorizadas") OR ????? ("Despesas Realizadas") OR ????? ("Despesas Funções e Sub-Funções") OR ?????
("Despesas Segundo Unidades Administrativas") OR ?????
("Despesas Fontes de Recurso") OR ????? ("Despesas por Grupos") OR ????? ("Fluxo de Caixa") OR ????? ("Relatório de Gestão Fiscal" OR RGF) OR
("Relatório Resumido de Execução Orçamentária") OR
("Demonstrativo da Receita") OR ????? ("Demonstrativo das Receitas e Despesas com Ações e Serviços Públicos de Saúde") OR ????? ("Demonstrativo de Receitas de Despesas com Manutenção e Desenvolvimento do Ensino") OR
("Demonstrativo dos Riscos Fiscais e Providências") OR ????? ("Demonstrativo das Metas") OR ????? ("Demonstrativo de Fixação de Despesas da Prefeitura") ????? ("Grupo Executivo de Acompanhamento da Execução Orçamentária e Financeira")
```

### Franca - Previdência Social

```
("São Paulo Previdência" OR SPPREV) OR ??? VITORIO - VERIFICAR OS TERMOS COM DANILO ("aposentadoria") OR
("pensão") OR
("Secretarias do Estado") OR ??? ("Secretarias Estaduais") OR ??? ("autarquias") OR ??? ("inatividade militar") OR
("Regime Próprio de Previdência Social") OR ??? ("contribuição") OR
("servidor") OR
("aposentados") OR
("pensionistas") OR
("previdência complementar")
```

### Franca - Saúde

```
("Secretaria de Saúde") OR
("Conselho Municipal de Saúde") OR
("Assistência Farmacêutica") OR ???? ESCLARECER SIGNFICADO ("Farmácia de Manipulação") OR ????? ESCLARECER SIGNFICADO ("Medicamentos Não Padronizados") OR ????? ESCLARECER SIGNFICADO ("Padronização Municipal de Medicamentos") OR ????? ESCLARECER SIGNFICADO ("Relação Nacional de Medicamentos Essenciais" OR RENAME OR "RENAME 2010") ????? ESCLARECER SIGNFICADO ("Sistema Integrado de Gestão em Saúde" OR SIGS) OR
("Unidade Básica de Saúde" OR UBS) OR
("Programa Saúde da Família" OR "PSFs") OR
("Núcleo de Gestão Assistencial" OR NGA) OR ???? EM SAÚDE??
("Ambulatório de Saúde Mental Adulto e Infantil") OR
("Casa do Diabético") OR ??? ("Centro Oftalmológico") OR ??? ("Diagnoses") OR ??? ("Centro de Diagnóstico por Imagem" OR CDI) OR ??? ("Pronto Socorro Adulto e Infantil") OR ??? ("Raio-X") OR ??? ("Unidade de Auditoria e Controle" OR UAC) OR ???
("Serviço de Remoção") OR ??? ("Autorização de Procedimentos de Alta Complexidade" OR APAC) OR ???
("Vigilância Sanitária") OR ("Vigilância Epidemiológica") OR
("Farmacovigilância") OR ??? ("Vacinação") OR ??? ("Vigilância Ambiental") OR ??? ("Cirurgias Eletivas") OR ??? ("Notificação de Doenças") OR ??? ("Vigilância em Saúde") OR ??? ("Unidade de Fisioterapia") OR ??? ("Serviços de Verificação de Óbitos") OR ???
("Centro de Referência em Saúde do Trabalhador" OR CEREST) OR
("Comissão de Farmácia e Terapêutica" OR CFT) OR
("Núcleo de Educação Permanente e Humanização") OR
("Adoção de Animais") OR ("Conselho de Proteção dos Animais") OR
("Conselho Municipal de Políticas Públicas sobre Drogas OR COMAD") OR
("Colo de Mãe") OR ??? ("Dentinho de Leite") OR ??? ("Fraldas Descartáveis") OR ??? ("Leite Fluído") OR ??? ("Leve Saúde") OR ??? ("Odontomóvel") OR ??? ("Plano de Atenção ao Dependente Químico" OR PADEQ) OR
("Papai Coruja") OR ??? ("Remédio em Casa") OR ??? ("Sabor Saudável") OR ??? ("Viva Vida") OR ??? ("Vivendo Melhor") OR ???
("Suporte Respiratório Domiciliar") OR
("Rede Municipal de Saúde de Franca/SP") OR
("Clínica Radiológica Francana") OR
("Santa Casa de Franca") OR
("Hospital Psiquiátrico Allan Kardec Franca/SP") OR
("Clínica de Medicina Nuclear de Franca") OR
("Magnemed Ressonância Magnética") OR
("Clínica Médica Alcantara e Moreira") OR
("Clínica Nefrológica") OR
("Nikkei") OR
("Laboratório Patologia Dra. Maria Heloísa") OR
("Laboratório Central") OR
("Dr. Leonardo de Oliveira") OR
("Laboratório Hospital São JOaquim") OR
("Laboratório Bioanálise") OR
("Universidade de Franca" OR UNIFRAN) OR
(Laboratório Dr. Alonso") OR
("Laboratório Labcenter") OR
("Laboratório Carlos Chagas") OR
("Associação de Pais e Amigos dos Excepcionais de Franca" OR APAE)
```