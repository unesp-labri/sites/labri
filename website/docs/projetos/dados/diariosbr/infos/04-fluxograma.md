---
id: fluxograma
title: Documentação DiáriosBR
sidebar_label: Fluxograma
slug: /projetos/dados/diariosbr/infos/fluxograma
---

Para acessar o *fluxograma* do projeto DiáriosBR, [clique Aqui](https://app.diagrams.net/#G12VDoCChgsmoyYeJIqrPV2fQCid3qepdx).