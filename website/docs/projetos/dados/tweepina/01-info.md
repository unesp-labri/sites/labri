---
id: info
title: Apresentação TweePInA
sidebar_label: Apresentação
slug: /projetos/dados/tweepina/info
---

<center>
    <img src="/img/projetos/dados/tweepina/tweepina.svg" alt="centered image" width="200" height="300" />
</center>


O Twitter é uma das principais redes sociais da atualidade, sendo muito utilizado por autoridades e instituições públicas que são objetos de estudo de várias pesquisas acadêmicas. Porém, ter acesso a série histórica de tweets dessas autoridades e instituições muitas vezes é difícil. Além disso, muitos tweets acabam sendo deletados, não estando disponíveis em arquivos e/ou repositórios públicos. Devido a isso, o objetivo geral do projeto **Tweets from Public institutions and Authorities** (TweePInA) é reunir tweets de autoridades e instituições públicas com especial destaque ao Brasil e organismos internacionais. 

Mais especificamente, este projeto visa: 

- (1) auxiliar a construção de uma memória de informações de autoridades e instituições públicas divulgadas através do Twitter; 

- (2) subsidiar pesquisas acadêmicas que utilizam o Twitter como fonte de informação de seus objetos de estudo através da disponibilização das variáveis disponibilizadas pelo Twitter; 

- (3) indicar possibilidades e instrumentos que auxiliem uma análise mais detalhada dos tweets; 

- (4) fornecer um instrumento básico de análise de tweets

Para acessar o *repositório* do projeto, [clique aqui](https://gitlab.com/unesp-labri/projeto/tweepina).
