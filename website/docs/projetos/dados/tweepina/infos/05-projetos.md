---
id: projetos
title: Documentação TweePInA
sidebar_label: Projetos Semelhantes
slug: /projetos/dados/tweepina/infos/projetos
---

## Projetos semelhantes que utilizam o Twitter para análise

Abaixo listamos projetos que também analisam dados de redes sociais, especialmente, o twitter.

- [https://nucleo.jor.br/monitor](https://nucleo.jor.br/monitor)