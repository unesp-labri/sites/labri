---
id: equipe
title: Equipe Acervo Redalint
sidebar_label: Equipe
slug: /projetos/dados/acervo-redalint/equipe
---

Abaixo são indicadas as pessoas que contribuem com o projeto

## Coordenadores do projeto

### Maria Julieta Abba 

<img className="img-equipe-foto" src="/img/equipe/julieta-abba.jpg"/> 

<div className="img-equipe-redes">
<a href="http://lattes.cnpq.br/2495306840732131"> <img className="img-icon-redes" src="/img/social/lattes.png" /> </a>

<img className="img-icon-redes" src="/img/social/email.png" />

<a href="https://www.linkedin.com/in/julieta-abba-71b04762/"> <img className="img-icon-redes" src="/img/social/linkedin.png" /> </a>


</div>


### Regina Laisner

<img className="img-equipe-foto" src="/img/equipe/regina-laisner.jpg"/> 

<div className="img-equipe-redes">
<a href="http://lattes.cnpq.br/2849922787767639"> <img className="img-icon-redes" src="/img/social/lattes.png" /> </a>

<img className="img-icon-redes" src="/img/social/email.png"/>

<a href="https://www.linkedin.com/in/regina-laisner-9b751735/"> <img className="img-icon-redes" src="/img/social/linkedin.png"/> </a>


</div>


### Paula Regina Pavarina

<img className="img-equipe-foto" src="/img/equipe/paula-regina.jpg"/> 

<div className="img-equipe-redes">
<a href="http://lattes.cnpq.br/8843531675243100"> <img className="img-icon-redes" src="/img/social/lattes.png" /> </a>

<img className="img-icon-redes" src="/img/social/email.png"/>

<a href="https://orcid.org/0000-0002-7341-4795"> <img className="img-icon-redes" src="/img/social/orcid.png"/> </a>


</div>


## Colaboradores do projeto

### [Rafael de Almeida](/docs/equipe#rafael-de-almeida-2020---atual) 

<img className="img-equipe-foto" src="/img/equipe/rafael.png"/> 

<div className="img-equipe-redes">
<a href="http://lattes.cnpq.br/5174307461578307"> <img className="img-icon-redes" src="/img/social/lattes.png" /> </a>

<a href="https://www.linkedin.com/in/rafael-augusto-ribeiro-de-almeida-083792137/"> <img className="img-icon-redes" src="/img/social/linkedin.png"/> </a>

</div>


<div style={{textAlign: 'center'}}>

*2020 - atual*

</div>

## Já passaram por aqui

### [Pedro Henrique Campagna](https://labriunesp.org/docs/equipe_inativo#pedro-henrique-campagna)

<img className="img-equipe-foto" src="/img/equipe/pedro-campagna.jpg"/> 

<div className="img-equipe-redes">
<a href="https://www.linkedin.com/in/pedrohcmds/"> <img className="img-icon-redes" src="/img/social/linkedin.png"/> </a>
<a href="https://github.com/pedrohcmds"> <img className="img-icon-redes" src="/img/social/github.png"/> </a>

</div>


<div style={{textAlign: 'center'}}>

*2020 - 2021*

</div>

### Vinicius Mesel

<img className="img-equipe-foto" src="/img/equipe/vinicius-mesel.jpg"/> 

<div className="img-equipe-redes">
<a href="https://www.linkedin.com/in/vmesel/"> <img className="img-icon-redes" src="/img/social/linkedin.png"/> </a>
<a href="https://github.com/vmesel"> <img className="img-icon-redes" src="/img/social/github.png"/> </a>

</div>


<div style={{textAlign: 'center'}}>

*2020*

</div>

### Marcelo Araujo Dente

<img className="img-equipe-foto" src="/img/equipe/marcelo-araujo.jpg"/> 

<div className="img-equipe-redes">
<a href="https://www.linkedin.com/in/marcelodente/"> <img className="img-icon-redes" src="/img/social/linkedin.png"/> </a>
<a href="https://github.com/mdente"> <img className="img-icon-redes" src="/img/social/github.png"/> </a>

</div>


<div style={{textAlign: 'center'}}>

*2020*

</div>

### Lucas Vido 

<img className="img-equipe-foto" src="/img/logo-labriunesp9.png"/> 

<div className="img-equipe-redes">
<a href="https://www.linkedin.com/"> <img className="img-icon-redes" src="/img/social/linkedin.png"/> </a>
<a href="https://github.com/"> <img className="img-icon-redes" src="/img/social/github.png"/> </a>

</div>


<div style={{textAlign: 'center'}}>

*2020*

</div>