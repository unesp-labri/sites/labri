---
id: elementos-equador
title: Elementos Equador
sidebar_label: Elementos Equador
slug: /projetos/dados/newscloud/infos/elementos-equador
---

import Tabs from '@theme/Tabs';
import TabItem from '@theme/TabItem';

<Tabs>
  <TabItem value="El Universo" label="El Universo" default>

|Elemento            |Tipo  |Coleta 01         |Coleta 02          |
|--------------------|------|------------------|-------------------|
|`origem`            |`str` |:heavy_check_mark:||
|`classificado`      |`list`|:heavy_check_mark:||
|`sigla`             |`str` |:heavy_check_mark:||
|`país`              |`str` |:heavy_check_mark:|
|`categoria`         |`list`|                  |:heavy_check_mark:|
|`autoria`           |`list`|                  |:heavy_check_mark:|
|`titulo`            |`str` |:heavy_check_mark:||
|`subtitulo`         |`str` |:heavy_check_mark:|||       
|`data`              |`str` |:heavy_check_mark:||         
|`horario`           |`str` |:heavy_check_mark:||         
|`data_atualizado`   |`list`|                  |:heavy_check_mark:||                 
|`horario_atualizado`|`list`|                  |:heavy_check_mark:||
|`link`              |`str` |:heavy_check_mark:||
|`link_archive`      |`str` |                  ||
|`data_archive`      |`str` |||
|`horario_archive`   |`str` |||
|`local`             |`list`|||
|`tags`              |`list`|                  |:heavy_check_mark:|
|`paragrafos`        |`list`|                  |:heavy_check_mark:|
|`dir_bd`            |`str` |:heavy_check_mark:||
|`dir_arquivo`       |`str` |||
|`codigo_bd`         |`str` |                  |:heavy_check_mark:|
|`imagens`           |`list`|||
|`nome_arquivo`      |`str` |                  |:heavy_check_mark:|
|`extra_01`          |-     |-|-|
|`extra_02`          |-     |-|-|
|`extra_03`          |-     |-|-|

</TabItem>
  <TabItem value="El Comercio" label="El Comercio">

|Elemento            |Tipo  |Coleta 01         |Coleta 02          |
|--------------------|------|------------------|-------------------|
|`origem`            |`str` |||
|`classificado`      |`list`|||
|`sigla`             |`str` |||
|`país`              |`str` ||
|`categoria`         |`list`|                  ||
|`autoria`           |`list`|                  ||
|`titulo`            |`str` |||
|`subtitulo`         |`str` ||||       
|`data`              |`str` |||         
|`horario`           |`str` |||         
|`data_atualizado`   |`list`|                  |||                 
|`horario_atualizado`|`list`|                  |||
|`link`              |`str` |||
|`link_archive`      |`str` |                  ||
|`data_archive`      |`str` |||
|`horario_archive`   |`str` |||
|`local`             |`list`|||
|`tags`              |`list`|                  ||
|`paragrafos`        |`list`|                  ||
|`dir_bd`            |`str` |||
|`dir_arquivo`       |`str` |||
|`codigo_bd`         |`str` |                  ||
|`imagens`           |`list`|||
|`nome_arquivo`      |`str` |                  ||
|`extra_01`          |-     |-|-|
|`extra_02`          |-     |-|-|
|`extra_03`          |-     |-|-|

  </TabItem>
</Tabs>
