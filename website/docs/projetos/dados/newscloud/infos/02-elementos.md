---
id: elementos
title: Documentação NewsCloud
sidebar_label: Elementos do Banco de Dados
slug: /projetos/dados/newscloud/infos/elementos
---

Questões da coleta - autoria nem sempre aparece na página web, mas pode estar presente no código. Contudo, o formato do nome se apresenta como no exemplo: "/autor/cmgutierrez/".

Questões da coleta - Se atentar com os horários dos jornais peruanos pois apresentam fuso -5.

O nome do autor não é necessariamente de quem escreveu a notícia, pode ser quem publicou ela no site.

## Elementos do Banco de Dados

| Variáveis | Tipo de Variável | Descrição | Exemplos Newscloud | Exemplos GovLatinAmerica |
|--|--|--|--|--|
|`tipo_dado`| `list` | Disponibilidade dos dados| Aberto / Fechado | Aberto / Fechado |
|`pais`| `str` | País referente ao dado | Brasil | Brasil |
|`origem`| `list` | Nome da fonte dos dados| Estadão | Ministério da Educação |
|`sigla`| `str` | Junção do País com a Origem (Abreviado)| BRA-ESTADAO| |
|`categoria`| `list`| Categoria dos dados, subdivisão para o banco | Política , Economia| Trabalho, Emprego e Previdência|
|`autoria`| `list` | Quem é o autor dos dados | Autor da Notícias | Autor dos documentos|
| `titulo`|`str`| Título dos dados| Título da Notícia| Título do artigo |
|`subtitulo`|`str`| Subtítulo do artigo | Subtítulo da Notícia| Subtítulo do artigo|
|`data`| `str` |Data de publicação| dd/mm/aaaa | dd/mm/aaaa|
|`horario`| `str` |Horário de publicação | hh:mm | hh:mm |
|`datetime`| `datetime` |Data e Horário de publicação | dd/mm/yyyy hh:mm | dd/mm/yyyy hh:mm |
|`data_atualizado`| `list` | list |Data de Atualização| dd/mm/aaaa | dd/mm/aaaa |
|`horario_atualizado`|  `list` |Horário de Atualização | hh:mm | hh:mm |
|`link`| `str` |URLs que devem ser coletados | 'https:...'| 'https:...'|
|`link_archive`| `str` |||
|`data_archive`| `list` |||
|`horario_archive`| `list` |||
|`local`| `list` |||
|`tags`| `list` | Tags de identificação dos dados| - | "Economia popular e solidária" |
|`paragrafos`| `list` | Corpo do texto | Parágrafos das notícias| Parágrafos dos artigos |
|`nome_arquivo`| `str`|||
|`imagens`| `list` |||
|`dir_bd`| `list` | Diretório do Banco de Dados |||
|`dir_arquivo`| `list` |||
|`codigo_bd`| `str` | Caminho do banco de dados | bd/002/010/001 | bd/001/02/001 |
|`nome_bd_json`| `str` | Nome do documento | BD_JORNAL_CHILE_LA_NACION.json |
|`env_dir_bd`| `str` | Código do banco de dados que será usado na variável de ambiente | BD_JORNAL_BOLIVIA_ELDEBER |  |
|`extra_01`| `str` |Campo extra para informações que não são abrangidas nos outros campos do arquivo | - | - |
|`extra_02`| `str` |Campo extra para informações que não são abrangidas nos outros campos do arquivo | - | - |
|`extra_03`| `str` |Campo extra para informações que não são abrangidas nos outros campos do arquivo | - | - |


## Variáveis

|Variável      |Significado                                                         |Exemplo                                                                  |
|--------------|--------------------------------------------------------------------|-------------------------------------------------------------------------|
|`env_dir_bd`  |Aponta para uma variável de ambiente que leva para o diretório raiz |A variável de ambiente leva para o diretório local/raiz `BD_JORNAL_UNESP`|
|`env_dir_json`|Diretório geral dos arquivos json                                   |`env_dir_bd` + `/json`                                                   |
|`nome_bd_json`|Nome do arquivo json                                                |`env_dir_bd` + `data`                                                    |
|`dir_json`    |Caminho completo do arquivo json                                    |`env_dir_json` + `env_dir_bd` + `data.json`                              |

## Tabela Modelo

|Elemento|Significado|Tipo|Exemplo|
|--------|-----------|----|-------|
|`origem`|-|`str`|-|
|`sigla`|-|`str`|-|
|`classificado`|-|`list`|-|
|`categoria`|-|`list`|-|
|`País`|-|`str`|-|
|`autoria`|-|`list`|-|
|`titulo`|-|`str`|-|
|`subtitulo`|-|`str`|-|
|`data`|-|`str`|-|
|`horario`|-|`str`|-|
|`data_atualizado`|-|`list`|-|
|`horario_atualizado`|-|`list`|-|
|`link_archive`|-|`str`|-|
|`data_archive`|-|`str`|-|
|`horario_archive`|-|`str`|-|
|`local`|-|`list`|-|
|`tags`|-|`list`|-|
|`paragrafos`|-|`list`|-|
|`nome_arquivo`|-|`str`|-|
|`imagens`|-|`list`|-|
|`dir_bd`|-|`str`|-|
|`dir_arquivo`|-|`str`|-|
|`codigo_bd`|-|`str`|-|
|`extra_01`|-|-|-|
|`extra_02`|-|-|-|
|`extra_03`|-|-|-|






## Indexação de Dados

Os htmls gerados podem ser pesquisados através do recoll.

### Índices externos

Para gerar os índices externos é necessário indicarmos a pasta em que se encontram os dados (htmls ou pdfs) e a pasta na qual está a indexação