---
id: citar
title: Como citar
sidebar_label: Como citar
slug: /projetos/dados/gov-latin-america/citar
---


GOVLATINAMERICA. Dados públicos dos órgãos governamentais latino americanos. 2025. Disponível em: https://labriunesp.org/docs/projetos/dados/gov-latin-america/info. Acesso em: 13 jan. 2025.