---
id: identidade-visual
title: Identidade Visual
sidebar_label: Identidade Visual
slug: /projetos/dados/gov-latin-america/identidade-visual
---

### Sobre o Logo

* Fonte: Bodoni FLF, Glacial Indifference
* Cores: #999999, #962932
* [Link (Canva)](https://www.canva.com/design/DAEdhYi4uJ0/s3vDkL8vudvpqh_vMKw7yQ/view?utm_content=DAEdhYi4uJ0&utm_campaign=designshare&utm_medium=link&utm_source=sharebutton)
