---
id: info
title: Apresentação GovLatinAmerica
sidebar_label: Apresentação
slug: /projetos/dados/gov-latin-america/info
---

<center>
    <img src="/img/projetos/dados/gov-latam/gov-latam.svg" alt="centered image" width="200" height="300" />
</center>

Os dados públicos dos órgãos governamentais latino americanos disponíveis via web com frequência são retirados dos sites oficiais, especialmente, após a passagem de um mandato presidencial para outro. A partir deste diagnóstico, o objetivo do projeto **GovLatinAmerica** é coletar tais dados para que possam ser utilizados em pesquisas acadêmicas diversas.

Para acessar o *repositório*, [clique aqui](https://gitlab.com/unesp-labri/projeto/govlatinamerica)


