---
id: doc
title: Documentação Mercodocs
sidebar_label: Documentação
slug: /projetos/dados/mercodocs/doc
---

Em construção

## Repositório

- Para acessar o *repositório* do projeto IRJournalsBR, [clique aqui](https://gitlab.com/unesp-labri/projeto/mercodocs).

## Material de Apoio

- Acesse o documento no *google drive* clicando [aqui](https://docs.google.com/document/d/18oagyiBXaaYQANBNlurSLHnT8SmUNljvGEQQdaLCuPk/edit?usp=sharing).