---
id: equipe
title: Equipe MercoDocs
sidebar_label: Equipe
slug: /projetos/dados/mercodocs/equipe
---

Abaixo são indicadas as pessoas que contribuem com o projeto

## Coordenadores do projeto

### [Marcelo Mariano](/docs/equipe#marcelo-passini-mariano)

<img className="img-equipe-foto" src="/img/equipe/marcelo.jpg"/>

<div className="img-equipe-redes">
<a href="http://lattes.cnpq.br/3505849964932313"> <img className="img-icon-redes" src="/img/social/lattes.png" /> </a>

<img className="img-icon-redes" src="/img/social/email.png"/>

<a href="https://scholar.google.com.br/citations?hl=pt-BR&user=UFsVIxQAAAAJ"> <img className="img-icon-redes" src="/img/social/google.png"/> </a>

</div>

### Regiane Nitsch Bressan

<img className="img-equipe-foto" src="/img/equipe/regiane-nitsch.png"/>

<div className="img-equipe-redes">
<a href="http://lattes.cnpq.br/0123316221379741"> <img className="img-icon-redes" src="/img/social/lattes.png" /> </a>

<a href="https://scholar.google.com/citations?user=eGBVPO0AAAAJ&hl=pt-BR"> <img className="img-icon-redes" src="/img/social/google.png"/> </a>

<a href="https://orcid.org/0000-0002-7101-793X"> <img className="img-icon-redes" src="/img/social/orcid.png" /> </a>

</div>

## Colaboradores do projeto

### [Rafael de Almeida](/docs/equipe#rafael-de-almeida-2020---atual) 

<img className="img-equipe-foto" src="/img/equipe/rafael.png"/>

<div className="img-equipe-redes">
<a href="http://lattes.cnpq.br/5174307461578307"> <img className="img-icon-redes" src="/img/social/lattes.png" /> </a>

<a href="https://www.linkedin.com/in/rafael-augusto-ribeiro-de-almeida-083792137/"> <img className="img-icon-redes" src="/img/social/linkedin.png"/> </a>

</div>

<div style={{textAlign: 'center'}}>

*2022 - atual*
</div>

## Já passaram por aqui

### [Pedro Campagna Moura](/docs/equipe_inativo#pedro-henrique-campagna)

<img className="img-equipe-foto" src="/img/equipe/pedro-campagna.jpg"/> 

<div className="img-equipe-redes">
<a href="https://www.linkedin.com/in/pedrohcmds/"> <img className="img-icon-redes" src="/img/social/linkedin.png"/> </a>
<a href="https://github.com/pedrohcmds"> <img className="img-icon-redes" src="/img/social/github.png"/> </a>

</div>


<div style={{textAlign: 'center'}}>

*2020 - 2021*
</div>