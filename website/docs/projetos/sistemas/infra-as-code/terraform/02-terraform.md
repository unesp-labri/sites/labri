---
id: intro
title: Introdução
sidebar_label: Introdução
slug: /projetos/sistemas/infra-as-code/terraform/intro
---

O conteúdo desta seção reúne configurações e comandos úteis que podem auxiliar o usuário durante o uso dos computadores do LabRI/UNESP.