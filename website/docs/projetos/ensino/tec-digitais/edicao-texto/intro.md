---
id: intro
title: Tecnologias para Edição de Texto
sidebar_label: Tecnologias para Edição de Texto
slug: /projetos/ensino/tec-digitais/edicao-texto/intro
---
:::tip
O objetivo dessa página é mostrar ferramentas importantes de edição no Documentos Google, com um vídeo explicativo para aprimorar os seus textos.
:::


### Editores de Texto Online 

- Documentos Google

    - **Custo**: Gratuito e Planos Pagos

    - **[Site Oficial](https://docs.google.com/document/u/0/)**

    - **Vídeo**: **Documentos Google - Como formatar e inserir sumário automático** _(Duração: 1m38s)_
    
[![V1](https://i.imgur.com/fFKaf6x.png)](https://www.youtube.com/watch?v=0DeJgsdZtZA "V1") 


