---
id: intro
title: Áudio
sidebar_label: Áudio
slug: /projetos/ensino/tec-digitais/audio/intro
---

### Transcrição Instantânea 

- **Custo**: Gratuito 

- **Sistema**: Android 

- **[Site oficial](https://play.google.com/store/apps/details?id=com.google.audio.hearing.visualization.accessibility.scribe&hl=pt_BR)**

[![App](https://i.imgur.com/l15irwz.png)](https://youtu.be/OOdVvRTKo5M "App")
