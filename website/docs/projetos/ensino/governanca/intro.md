---
id: intro
title: Curso - Governança da Internet
sidebar_label: Curso - Governança da Internet
slug: /projetos/ensino/governanca/intro
---

## Apresentação do Curso 

:::tip
O curso visa preencher uma lacuna nas disciplinas de Relações Internacionais e Ciências Sociais, que é o estudo da chamada Governança da Internet. Sendo um campo relativamente novo, há um interesse crescente da comunidade acadêmica e uma demanda das áreas de RI e Ciências Sociais para um tema que, no Brasil, se destaca mais no Direito. 
:::

Assim, o curso tem como principal objetivo apresentar as discussões já consolidadas sobre Governança da Internet, assim como debates contemporâneos, abrangendo tópicos como:
- História da Governança da Internet, principais conceitos e autores;
- Aspectos técnicos de gerenciamento global da Internet e suas implicações geopolíticas;
- Aspectos socioeconômicos como governança de plataformas e principais regulações propostas por atores estatais e não-estatais; 
- Visão macro da governança da Internet abrangendo as principais instituições e atores que compõem esse complexo ecossistema global.
- Debates de destaque no momento, como cibersegurança, antitruste, governança de dados e moderação de conteúdo.

### Palestrantes

O curso será ministrado por: **Jaqueline Trevisan Pigatto** e **Mark William Datysgeld**.

### Programação do Curso 

#### AULA 1 - INTRODUÇAO 

- Interação com os alunos acerca dos conhecimentos sobre governança da Internet;

- Exposição sobre as três camadas da Governança da Internet;

- Apresentação de principais conceitos, como do “multissetorialismo”;

- Apresentação dos principais tópicos do campo (moderação de conteúdo, governança de dados, cibersegurança, etc.).

#### AULA 2 - CAMADAS INFRAESTRUTURA E LÓGICA

- Desmistificação da estrutura da Internet;

- O que é e o que faz a ICANN;

- Outros atores e disputas geopolíticas.

#### AULA 3 - CAMADA SOCIOECONÔMICA 

- Agenda de Túnis e o IGF;

- Retorno do Estado;

- Principais atores e marcos regulatórios (Brasil, Europa, o problema EUA e Seção 230...).

#### AULA 4 - DEBATES ATUAIS E PRINCIPAIS DESAFIOS 

- Cibersegurança, ONU, guerra da Ucrânia, China, Rússia

- Concorrência de mercados (antitruste)

- Forum shifting (OMC, OCDE, etc) 

- Complexo de regimes, instituições e atores