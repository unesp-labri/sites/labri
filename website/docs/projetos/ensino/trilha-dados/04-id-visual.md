---
id: id-visual
title: Identidade Visual
sidebar_label: Identidade Visual
slug: /projetos/ensino/trilha-dados/id-visual
---

### Sobre o logo

- **Fontes**: League Gothic
- **Cores**: #004455, #D54050
- **_[Link (Canva)](https://www.canva.com/design/DAEOn9JDEko/UkG36icGx3N2m-czVdYiRA/view?utm_content=DAEOn9JDEko&utm_campaign=designshare&utm_medium=link&utm_source=sharebutton)_**
