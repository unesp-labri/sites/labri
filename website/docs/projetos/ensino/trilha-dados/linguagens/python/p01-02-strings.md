---
id: p01-02-strings
title: Strings
sidebar_label: Strings
slug: /projetos/ensino/trilha-dados/linguagens/python/p01-02-strings
--- 

- Uma string pode ser definida como uma lista de caracteres. É uma série ordenada e imutável de caracteres (números, letras, símbolos, espaços e pontuações)
- Para se definir uma string, ela deve ser colocada entre aspas simples `'string'`

```py
palavra = 'pneumoultramicroscopicossilicovulcanoconiótico'
print(palavra, type(palavra))

frase = 'O homem é o lobo do homem' 
print(frase, type(frase))

simbolos = 'áàUÜ#@^><阿萨' 
print(simbolos, type(simbolos))

```
- Utilização de aspas dentro de aspas

```py

frase = 'O homem é o "lobo" do homem' 
print(frase, type(frase))

```
- Função built-in `input()`

```py
novo_membro = input('digite o seu nome: ')
print('olá, ' + novo_membro)

```
### Formatação de string 📜

- `f-string` 

```py
novo_membro = input('digite o seu nome: ')
print(f'olá, {novo_membro}')

```

- `format()`

```py
continente = 'ásia'
pais = 'china'
print('O país {1} está localizado no continente {0}'.format(pais, continente))

numero = 99
print('{:5d}'.format(numero))
print('olá, ' + novo_membro + '. Tudo bem com você?') 
print('Rafael ' * 10)

``` 


### Índices e manipulação de strings

- Link do Replit dessa parte [Aqui](https://replit.com/@fparon/Strings#main.py)

- Cada caracter que compõe uma string apresenta um índice. Tal índice pode ser indicado para encontrarmos determinado caracter 
- Em Pyhton, o índice `0` se refere ao primeiro elemento da string e de outros tipos de dados 

![Python](https://i.imgur.com/kQjuBOD.png)

Os números indicados na figura acima são os índices de cada caracter da palavra. Para acessá-los, podemos fazer da seguinte maneira:

```py

"""
`[início:fim:salto]`
"""
linguagem = 'Python Slice String'
#A função len() indica o tamanho do tipo de dado. No caso da string, ela indica a quantidade de caracteres
print(len(linguagem))
#O print abaixo já mostra o primeiro elemento do conjunto de caracteres
print(linguagem[0])
#O print abaixo irá mostrar o último elemento da string
print(linguagem[-1])
#O print abaixo irá mostrar o intervalo entre o segundo e o quarto elemento 
print(linguagem[2:4])
#O print abaixo irá mostrar do primeiro ao quarto elemento 
print(linguagem[:4])
#O print abaixo irá mostrar do quarto elemento até o último
print(linguagem[3:])
#O print abaixo irá inverter a string 
print(linguagem[::-1])
#O print abaixo segue essa estrutura: `[início:fim:salto]`, assim começará no índice 1 e terminará no índice 19, saltando de 2 em 2
print(linguagem[1:19:2])
string_de_numeros = '0123456'
#O print abaixo começa no elemento 1 e vai até o último elemento saltando de 2 em 2, passando por todos os índices ímpares
print(string_de_numeros[1::2])
#O print abaixo verifica todos os elementos, saltando de 2 em 2, passando por todos os índices pares
print(string_de_numeros[::2])

```

|Tipos de dados `str`||
|------------------|---|
|uso|usado para armazenar uma lista de caracteres|
|criação|caracteres colocados `"entre aspas"`|
|métodos de buscar|`find('elemento')`|
|ordem preservada?| sim. os itens podem ser acessados por índices|
|mutável?|não|
|ordenado?|sim|




### Métodos de String 🧩

- Link do Replit [Aqui](https://replit.com/@fparon/metodos-string#main.py)

- Os métodos são funções dentro de uma classe. Nesse momento, o mais importante é você saber que os métodos realizam operações em determinados tipos de dados. Por exemplo, nas strings o método `split()` divide cada caractere da string em um elemento a parte. 
- `print()`, `type()`, `len()`, `input()` também são funções embutidas no Python. 
- Todas as funções e métodos embutidos no Python visam facilitar o manuseio de determinado tipo de dado, seja ele em string, lista, dicionário, tupla ou conjunto

|Método|       operação                |
|-----------|--------------------------|
|`split()`  | Separa uma string em uma lista com substrings     |
|`join()`   | Unir uma lista de strings em uma única string  |
|`strip()`  | Remove todos os espaços extras do começo ao fim da string|
|`lower()`, `upper()`, `swapcase()`, `title()`  | Colocar em caixa alta, baixa, trocar minusculas por maiusculas e vice-versa e título (primeiras letras maiusculas, resto minusculo)  |
|`replace()`   |Substitui caracteres na string|
|`count()`   | Retorna a quantidade de caracteres repetidos |
|`startswith()`, `endswith()` |Retorna `True` ou `False` para indicar o prefixo ou sufixo da string |
|`find()`, `index()`   | Busca determinado caractere na string e retorna o índice do primeiro caractere encontrado; o `index()` gera uma excessão caso não encontre o caractere especificado|
|`isalnum()`, `isalpha()`, `isidentifier()`, `iskeyword()`, `isprintable()`, `isspace()`, `isupper()`, `islower()`  | Retorna `True` ou `False` para indicar se é alfanumérico, caixa alta ou baixa, entre outros.|
|`zfill()`   | Retorna a string preenchendo à esquerda com o caractere `0` |


#### Exemplos

```py
from keyword import iskeyword 
#método split

novo_membro = input('digite o seu nome: ')
print(f'olá, {novo_membro}'.split(' '))


#método join
europa = ['portugal', 'espanha', 'italia']
print(type(europa))
europa_2 = ', '.join(europa)
print(europa_2)
print(type(europa_2))

#método strip
frase = '     roubei uma goiaba       '
print(frase.strip())

#métodos swapcase, upper, lower e title
frase = "Zen do Python"
print(frase.swapcase(), frase.upper(), frase.title(), frase.lower())

#método replace
print(frase.replace('Zen', 'Bravo'))

#método count
print(frase.count('n'), type(frase.count('n')))

#métodos startswith, endswith, find, isalnum, isalpha, isidentifier, iskeyword, isprintable, isspace, islower e isupper
frase = "Zen do Python"
print(frase.startswith("Zen"))
print(frase.endswith("do"))
print(frase.find('en do Python'))
print(frase.isalnum())
print(frase.isalpha())
print(frase.isidentifier())
print(iskeyword('return'))
print('x\ny'.isprintable())
print(' '.isspace())
print(frase.islower())
print(frase.isupper())

#método zfill
p = 'palavra'
print(p.zfill(8))

#métodos index e find
print(p.index('pa'))
print(p.find('pa'))
```


### Caracteres de escape 

- [Link](https://replit.com/@fparon/string-escape#main.py) do Replit 

| Caractere | Descrição | 
| --- | --- |
| `\n` , `\r` ,  `\r\n`    | nova linha  |
| `\s`    |  espaço   |
| `\t`    |  tab   |
| `\v`    |  tab vertical   |
| `\e`    | escape    |
| `\b`   | backspace    |

```py
print('meu nome é \n Fábio')
print('meu nome é \s Fábio')
print('meu nome é \t Fábio')
print('meu nome é \v Fábio')
print('meu nome é \e Fábio')
print('meu nome é\b Fábio')
```

