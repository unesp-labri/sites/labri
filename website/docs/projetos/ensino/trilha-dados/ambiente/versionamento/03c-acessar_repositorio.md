---
id: acessar-repositorio
title: Acessar Repositório
sidebar_label: Acessar Repositório
slug: /projetos/ensino/trilha-dados/ambiente/versionamento/acessar-repositorio
---

:::tip
Neste tutorial, vamos aprender como criar um repositório no GitHub a partir da criação de uma conta e como aplicar a licença MIT ao projeto.
:::

## O que é o GitHub?

O GitHub é uma plataforma de hospedagem de código-fonte e colaboração para desenvolvedores. Ele fornece um serviço de gerenciamento de versão distribuído usando o sistema de controle de versão Git e oferece várias ferramentas e recursos úteis para gerenciar projetos de software. Além de hospedar repositórios de código-fonte, o GitHub também fornece recursos para rastrear bugs, gerenciar tarefas e colaborar com outros desenvolvedores em projetos de código aberto ou privados. O GitHub é uma das plataformas mais populares para o desenvolvimento de software em todo o mundo e é amplamente utilizado pela comunidade de desenvolvedores.

## Benefícios de ter uma conta no GitHub e de criar repositórios abertos(compartilhados)

Existem vários benefícios de repositórios abertos e do uso do GitHub. Alguns dos principais benefícios são:

1.  Colaboração: o GitHub permite que várias pessoas trabalhem em um projeto ao mesmo tempo. Isso significa que equipes de desenvolvimento podem colaborar e trabalhar juntas em um único projeto, independentemente da localização geográfica.
    
2.  Versionamento: o GitHub permite que você controle o versionamento do seu código. Isso significa que você pode voltar para uma versão anterior do seu projeto a qualquer momento, o que é especialmente útil se algo der errado.
    
3.  Comunidade: o GitHub é uma grande comunidade de desenvolvedores e usuários em todo o mundo. Isso significa que você pode se conectar com outras pessoas, compartilhar seus projetos e obter feedback.
    
4.  Aprendizado: o GitHub é um ótimo lugar para aprender novas habilidades e técnicas de programação. Você pode ver como outros desenvolvedores trabalham em projetos, estudar seus códigos e aprender com eles.
    
5.  Portfólio: o GitHub é um ótimo lugar para criar um portfólio online de projetos que você criou. Isso pode ajudá-lo a se destacar em entrevistas de emprego e a mostrar seu trabalho para possíveis empregadores ou clientes.
    

Além desses benefícios, o GitHub também oferece uma série de ferramentas e recursos para ajudar os desenvolvedores a trabalhar com mais eficiência, como integração contínua, controle de problemas, revisão de código e muito mais. Por esses motivos, o GitHub é amplamente utilizado por desenvolvedores em todo o mundo como uma plataforma para hospedar, gerenciar e colaborar em projetos de código aberto e privados.

## Criação de uma conta no GitHub 

Para começar, você precisa criar uma conta no GitHub. Se você já tiver uma conta, pode pular esta etapa.

1.  Acesse o site do [GitHub](https://github.com/)
2.  Clique em "Sign up" no canto superior direito.
3.  Preencha os campos solicitados com seu nome de usuário, endereço de e-mail e senha.
4.  Clique em "Create account".

## Criação do repositório 

Agora que você tem uma conta no GitHub, pode criar um repositório para o seu projeto.

1.  Faça login na sua conta do GitHub.
2.  Clique em "New" no canto superior esquerdo.
3.  Digite um nome para o seu repositório.
4.  Adicione uma descrição opcional para o seu repositório.
5.  Escolha se deseja que o repositório seja público ou privado. Se você escolher "private", apenas você e as pessoas que você convidar poderão ver e colaborar com o seu projeto.
6.  Marque a opção "Initialize this repository with a README".
7.  Clique em "Create repository".

## Adicionando a licença

Agora que o repositório foi criado, você pode adicionar a licença MIT ao seu projeto.

1.  Clique em "Add file" e selecione "Create new file".
2.  Digite "LICENSE" no campo de nome do arquivo.
3.  Clique no botão "Choose a license template".
4.  Selecione "MIT License".
5.  Clique em "Review and submit".
6.  Confirme que deseja aplicar a licença MIT ao seu projeto.
7.  Clique em "Create file".

Seu repositório agora está pronto para ser usado com a licença MIT aplicada!

Observação: é importante ler e entender os termos da licença antes de aplicá-la ao seu projeto. A licença MIT permite que outras pessoas usem, copiem, modificem e distribuam seu código, desde que atribuam a você a autoria do código original e incluam a licença em seus projetos.


## Material de apoio

- [Clone a GitHub repository using a Personal Access Token](https://gist.github.com/magickatt/07ff02068f8cd2e4e558f20358b2082c)


## Autoria

- [Rafael de Almeida](https://gitlab.com/unesp-labri/sites/labri/-/commits/main/website/docs/projetos/ensino/trilha-dados?author=rafaelrdealmeida)
- [Leonardo de Almeida Petrilli](https://gitlab.com/unesp-labri/sites/labri/-/commits/main/website/docs/projetos/ensino?author=Leonardo%20Petrilli)
- [Ver todos os autores](https://gitlab.com/unesp-labri/sites/labri/-/commits/main/website/docs/projetos/ensino/trilha-dados/ambiente)