---
id: tipos-licencas
title: Tipos de Licenças
sidebar_label: Tipos de Licenças
slug: /projetos/ensino/trilha-dados/ambiente/tipos-licencas
---


## Qual o problema?⚠️

A partir do contexto da globalização 🌐 e ampliação do uso da internet, uma vasta produção de conhecimento passou a circular no meio digital. A disseminação de conhecimento, muitas vezes, não apresenta garantia, o que pode acarretar no uso indevido ou inapropriado das informações disponibilizadas de forma aberta.

De modo geral, a concepção de ciência aberta visa ampliar o acesso da sociedade à produção de conhecimentos e tecnologias, reduzindo custos💲 e buscando garantir que este acesso se mantenha e se amplie a longo prazo. 

A partir dessas questões surgiram diversos tipos de licenças abertas que buscam garantir, juridicamente, um acesso aberto minimizando restrições impostas por licenças proprietárias.

## Tipos de licenças🛡️

A problemática da do acesso ao conhecimento não pode ser totalmente resolvida devido às dimensões tomadas pelos sistemas de distribuição dos saberes produzidos, mas há, ainda, a possibilidade de evitá-la e/ou minimizá-la por meio das Licenças de Uso, serviço de acesso e uso a certo Sistema 🖥 ️. Em linhas gerais, as licenças podem ser divididas em dois tipos: abertas ou fechadas.

No caso da primeira🔒, sistemas de código fechado podem ser liberados apenas por meio de pagamento 💰 (por assinatura ou vitalício) ou concessão de dados pessoais e limitação de uso; o detentor dos direitos pode não permitir determinadas modificações e/ou adaptações; o uso livre do software por usuário secundário requer autorização prévia e expressa do autor; tais diretrizes são definidas pela Lei dos Direitos Autorais (Lei 9.610/98). 

Já na segunda opção🔓, nas licenças abertas, os direitos patrimoniais do detentor dos direitos autorais do software são compartilhados com a sociedade . Neste, permite-se: cópia, reprodução, redistribuição e trabalhos derivados. Esta forma é garantida pela GNU, GPL, Creative Commons e pelo domínio público. 

Além da divisão entre abertas e fechadas, as licenças são apresentadas, também, por dois tipos: licenças para código e licenças para bases de dados. Nas de código, segundo o site mend.io, em 2021 as mais usadas foram Apache License 2.0, MIT License e GNU General Public License v3.0.


import Tabs from '@theme/Tabs';

import TabItem from '@theme/TabItem';

:::info Licenças

<Tabs>
  <TabItem value="apache license 2.0" label="Apache license 2.0">
  
  

🔶 Direitos autorais: licença irrevogável para reproduzir, derivar trabalhos e publicá-los.


🔶 Defende os contribuidores e suas contribuições.

✅ Pode: usar, reproduzir, distribuir, modificar, comercializar, usar patentes, usar garantias. 

❌ Não pode: usar marca registrada, suspender, responsabilizar.

❗Deve: incluir direitos autorais, apresentar relato de todas as mudanças realizadas no programa, incluir licença.

🔶 Segue as leis locais, de acordo com sua localização.

🔶 30% mais usada.
</TabItem>
  <TabItem value="MIT License" label="MIT License">
  
  

🔶 Licença permissiva.


🔶 Possui cláusula de isenção de garantias e responsabilidades.

🔶 Protege o autor de quaisquer processos judiciais.

✅ Pode: usar, reproduzir, distribuir, modificar, combinar, sublicenciar e comercializar.

❌ Não pode: responsabilizar, suspender. 

❗Deve: incluir direitos autorais, incluir licença.

🔶 Segue as leis locais, de acordo com sua localização.

🔶 26% mais usada.
</TabItem>
  <TabItem value="GNU General Public License v3.0" label="GNU General Public License v3.0">
  
  

🔶 Licença de código aberto.


🔶 Protege o usuário de certas ameaças novas: Leis que proíbem o uso de Software Livre; Acordos de patentes que não pertencem, de fato, àquela empresa; Tivoização (GPL que não pode ser alterada).

🔶 O GNU GPL não restringe o que o usuário pode ou não fazer, mas delimita as mudanças feitas para que certos usuários não limitem a outros.

✅ Pode: uso comercial; modificar; distribuir; utilizar reivindicação de patente.

❌ Não pode: sublicenciar; suspender.

❗Deve incluir: cópia original; declaração de mudanças realizadas no software; fonte; licença; direitos autorais; instruções de instalação.

🔶 9% mais usada.
</TabItem>
</Tabs>

:::

### Nas de bases de dados 🎲, há apenas duas:


:::info Bases de dados

<Tabs>
  <TabItem value="DbCL" label="DbCL">
  
  

🔶 Licença do Conteúdo do Banco de Dados.



🔶 Licença de direitos autorais mundial 🌐.

🔶 Isenta de royalties.

✅ Pode: compartilhar, criar e adaptar o conteúdo; comercializar; sublicenciar (trabalhos derivados devem, também, estar sob a DbCL).

🔶 O licenciador isenta-se de qualquer responsabilidade sobre perdas ou danos causados por qualquer uso e por 
qualquer pessoa desta licença (mesmo se a falha for causada por erro do Licenciante).
</TabItem>
  <TabItem value="ODbL" label="ODbL">
  
  

🔶 Open Database License.

✅ Pode: compartilhar; distribuir; copiar; usar o banco de dados; produzir trabalhos a partir deste banco de dados; transformar e modificar sobre o banco de dados; sublicenciar.

🔶 Manter aberto🔓: em caso de redistribuição do banco de dados ou versão adaptada dele, pode-se usar tecnologias que restrinjam os trabalhos que o usarão, mas há a obrigação simultânea do compartilhamento de uma versão sem essas medidas.

🎯 Objetivo: manter a liberdade do uso de bases de dados que usam essa licença.
</TabItem>
</Tabs>

:::

# Material de apoio:
   Há, ainda, outras licenças de código disponíveis para consulta, como: BSD (Simplificada e Modificada); Mozilla; Eclipse; GNU (Affero, v2.0 e Lesser); Boost; Creative Commons. The Unlicense.
   Abaixo indicamos alguns sites que podem auxiliar na escolha de uma licença adequada:

   🔶 [GitHub Repositories.](https://docs.github.com/pt/repositories/managing-your-repositorys-settings-and-features/customizing-your-repository/licensing-a-repository)

   🔶 Links para auxílio: 

   🔸[Comissão Europeia: assistente de licenciamento - Ache e compare lienças de softwares.](https://joinup.ec.europa.eu/collection/eupl/solution/joinup-licensing-assistant/jla-find-and-compare-software-licenses)

   🔸[Escolha uma licença.](https://choosealicense.com/)

   🔸[Licenças e exceções comumente usadas em fontes abertas ou softwares colaborativos, dados, hardware ou documentações.](https://spdx.org/licenses/)

   🔸[Licenças para software em inglês simples.](https://www.tldrlegal.com/)

   🔶 Links para aprofundamento/textos complementares:

   🔸["Um estudo sistemático de licenças de software livre." Tese de mestrado de Vanessa Cristina Sabino.](https://www.teses.usp.br/teses/disponiveis/45/45134/tde-14032012-003454/pt-br.php)

   🔸[Wikipedia: Comparação entre licenças para softwares abertos.](https://en.wikipedia.org/wiki/Comparison_of_free_and_open-source_software_licenses)

   🔸[Open Data Commons](https://opendatacommons.org/)

   🔸[Sobre as licenças Creative Commons.](https://creativecommons.org/licenses/?lang=pt_BR)