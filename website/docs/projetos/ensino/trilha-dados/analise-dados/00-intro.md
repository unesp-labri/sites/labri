---
id: intro
title: Apresentação
sidebar_label: Apresentação
slug: /projetos/ensino/trilha-dados/analise-dados/intro
---

:::tip
Esta parada da Trilha de Dados trata da analise de dados utilizando da liguagem de programação python. 

O estudante terá contato com bibliotecas[^1] como pandas, numpy, ploty
:::


## Pré requisitos

- [Treinamento de Preparação de Ambiente de Trabalho](/docs/projetos/ensino/trilha-dados/ambiente/intro)


## Tópicos do treinamento


- [Tópico 01 - Conceitos básicos](/projetos/ensino/trilha-dados/analise-dados/01-00-conceitos-basicos.md)
  - [Pandas - Series e Dataframe](/projetos/ensino/trilha-dados/analise-dados/01-01-pandas-series-e-dataframe.md)
  - [Propriedades básicas](/projetos/ensino/trilha-dados/analise-dados/01-02-propriedades-basicas.md)
  - [consulta e modificação dos dados](/projetos/ensino/trilha-dados/analise-dados/01-03-consulta-e-modificacao.md)
  - [trabalhando com arquivos (csv, json)](/projetos/ensino/trilha-dados/analise-dados/01-04-trabalhando-com-arquivos.md)
 
- [Tópico 02 - Geração de gráficos](/projetos/ensino/trilha-dados/analise-dados/02-gerar-graficos.md)

- [Tópico 03 - Combinando DataFrames](/projetos/ensino/trilha-dados/analise-dados/03-combinando-dataframes.md)
  
- [Tópico 04 - Tratamento de dados](/projetos/ensino/trilha-dados/analise-dados/04-tratamento-de-dados.md)
  

  


## Certificação 



- [Formulário Treinamento de analise de dados com python](https://forms.gle/Cm6HZfWLcNTW4GK9A)


[^1]: [EXPLICAR O QUE UMA BIBLIOTECA]

### Autoria

- [Rafael de Almeida](https://gitlab.com/unesp-labri/sites/labri/-/commits/main/website/docs/projetos/ensino/trilha-dados/analise-dados?author=rafaelrdealmeida)
- [João Lucas Passos Motta](https://gitlab.com/unesp-labri/sites/labri/-/commits/main/website/docs/projetos/ensino/trilha-dados/analise-dados?author=João%20Lucas%20Passos%20Motta)
- [Ver todos os autores](https://gitlab.com/unesp-labri/sites/labri/-/commits/main/website/docs/projetos/ensino/trilha-dados/analise-dados)


## Material de apoio

- https://github.com/quinnanya/dh-jupyter