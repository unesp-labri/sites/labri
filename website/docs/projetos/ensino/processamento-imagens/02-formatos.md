---
id: formatos
title: Formato de Arquivos
sidebar_label: Formato de Arquivos
slug: /projetos/ensino/processamento-imagens/formatos
---
:::tip
Essa página tem como função detalhar os tipos de formatos de arquivos que são mais utilizados para pesquisa e estudo da graduação e da pós-graduação. O processo de digitalização será explicado na página de OCR.
:::


### Identificar formato

- Arquivos no formato textual (doc, docx, odt, epub) são nativamente passíveis de busca por palavra chaves (basta você abrir o arquivo, aperta Ctrl+F e digitar a palavra chave que desejar) e indexáveis em software como o Recoll. Sendo assim, não são o foco deste tutorial.
- Em Arquivos no formato de imagem (png, jpeg, tiff entre outros) não é possivel nativamente buscar por palavra chaves e/ou indexar em software como o Recoll. Para saber mais sobre formatos de imagem [clique aqui](https://archive.is/82iiV).
- Arquivos no formato PDF dependem da fonte de sua origem para ser ou não nativamente passiveis de busca por palavra chaves e indexáveis em software como o Recoll.
- Se o PDF foi gerado a partir de algum arquivo no formato textual, normalmente, será nativamente passível realizar busca por palavras chaves. Este tipo de PDF é conhecido como "PDF pesquisável".
- Se o PDF foi gerado a partir de algum arquivo no formato de imagem, normalmente não será nativamente passível realizar busca por palavras chaves. Este tipo de PDF é conhecido como PDF não pesquisável. Assim, nosso foco recai sobre os arquivos no formato de imagem e nos PDF não pesquisáveis, pois são estes tipos de arquivo em que não conseguimos nativamente buscar por palavra chaves e/ou indexar em software como o Recoll. 👍

### Qual o formato do arquivo?

- Se o arquivo for um PDF não pesquisável é necessário transforma-lo em algum formato de imagem. De preferência para transforma-lo em TIFF, pois é o formato mais utilizado para este tipo de processamento.
- Se o arquivo já se encontra em formato de imagem siga pra o próximo passo. 
**ATENÇÃO:** ao digitalizar páginas de documentos, livros e afins busque definir uma resolução ao menos média (equivalente a 300 dpi) para ter um melhor resultado final ao processar as imagens.
- Se o arquivo estiver no formato de PDF é necessário transformá-lo em TIFF. Para isso basta seguir o [vídeo tutorial](https://www.youtube.com/watch?v=wmcrUGzGvkQ). Caso o arquivo já se encontre no formato TIFF é seguir para o segundo passo.

### Processar o arquivo

- Com os arquivos em formato de imagem agora vamos utilizar o software ScanTailor para otimizar as imagens. Este software irá te auxiliar a fixar a orientação, cortar e rotacionar as páginas; selecionar o conteúdo, definir as margens e DPI entre outras otimizações
- Recomendamos definir o DPI em no mínimo 300 DPI. Para saber mais sobre DPI [clique aqui](https://archive.is/ElT6P).
- Para mais instruções sofre a utilização do ScanTailor assista o [vídeo tutorial](https://www.youtube.com/watch?v=2a9f9QzasBQ). 😄

### Autoria

- [Rafael de Almeida](https://gitlab.com/unesp-labri/sites/labri/-/commits/main/website/docs/projetos/ensino/filezilla?author=rafaelrdealmeida)
- [Artur Dantas](https://gitlab.com/unesp-labri/sites/labri/-/commits/main/website/docs/projetos/ensino/filezilla?author=Artur%20Dantas)
- [Ver todas as autorias](https://gitlab.com/unesp-labri/sites/labri/-/commits/main/website/docs/projetos/ensino/processamento-imagens)