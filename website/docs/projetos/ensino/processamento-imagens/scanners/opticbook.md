---
id: opticbook
title: Plustek Opticbook 3800
sidebar_label: Plustek Opticbook 3800
slug: /projetos/ensino/processamento-imagens/scanners/opticbook
---

![opticbook1](/img/projetos/sistemas/opticbook1.jpeg)

![opticbook2](/img/projetos/sistemas/opticbook2.jpeg)

- Programa correspondente:

![opticbook3](/img/projetos/sistemas/opticbook3.png)

- Com o programa aberto algumas opções são possível. Você deve deixar a opção “ Formato do Arquivo” em Uncompressed Aldus Pasta de destino coloque a pasta para o local em que as imagens serão salvas após o processo de digitalização. 

- Em “Prefixo” o nome do arquivo e como ele será salvo. Lembre-se que a nomenclatura segue a data de publicação. Sobrenome do autor. Nome do Autor. Nome da Obra. 

- Em “ Configuração de Modo” você poderá mudar o DPI da imagem. Por padrão as scanners funcionam com 300 DPI. Entretanto para melhores resoluções você pode aumentar esse número. Lembre-se que quanto maior a resolução mais tempo a digitalização vai demorar. Por fim quando tudo estiver correto, coloque o objeto a ser digitalizado na scanner e aperte os botões b/w (preto e branco), Gray(cinza) e color (digitalização com cor) na própria scanner ou programa. 

- Por ser mais rápido geralmente se utiliza os botões da própria scanner. Após tudo ser digitalizado aperte o botão transferir e suas imagens serão transferidas para a pasta destino.