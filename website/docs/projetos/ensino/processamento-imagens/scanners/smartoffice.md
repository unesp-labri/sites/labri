---
id: smartoffice
title: Plustek  SmartOffice PS286 Plus
sidebar_label: Plustek  SmartOffice PS286 Plus
slug: /projetos/ensino/processamento-imagens/scanners/smartoffice
---

![smartoffice](/img/projetos/sistemas/smartoffice.jpeg)

- Programa correspondente:

![smartoffice2](/img/projetos/sistemas/smartoffice2.png)

- Na opção “ Pasta de Destino “ você decidirá para onde as imagens serão enviadas após a digitalização.

- No formato de arquivo sempre deixar TIFF não comprimido

- No tipo de escaneamento você escolherá que se quer digitalizar a página da frente ou frente e verso. A vantagem dessa scanner é que ela pode digitalizar frente e verso rapidamente, salvando o seu tempo. Para tanto selecionar “ADF Duplex”. Caso queira apenas a parte da frente selecione “ADF Frente”.

- Em resolução deixe 300 DPI. Aumente o DPI conforme a necessidade para uma melhor resolução do scaneamento. 

- Após isso feche o programa e aperte o botão “ Scan” na scanner.

- Após o scaneamento de uma folha ser completado você poderá colocar quantas outras folhas quiser, o scaneamento continuará de forma automática, lembre, entretanto, que você tem apenas 20 segundos para colocar outra folha.

![smartoffice3](/img/projetos/sistemas/smartoffice3.png)