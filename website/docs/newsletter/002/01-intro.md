---
id: intro
title: Newsletter LabRI/UNESP
sidebar_label: Apresentação
slug: /newsletter/002/intro
---


O Laboratório de Relações Internacionais da Universidade Estadual Paulista (LabRI/UNESP) lança agora sua primeira newsletter. A mesma tem como intuito a divulgação das atividades realizadas nas últimas semanas para os interessados, além de auxiliar na gestão interna. A equipe de gestão de projetos dá início às suas divulgações através desta newsletter, onde reuniu informações acerca das atualizações de cada projeto ligado ao LabRI/UNESP e demais informações úteis ao leitor que deseje utilizar as ferramentas oferecidas.
