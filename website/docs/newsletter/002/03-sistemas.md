---
id: sistemas
title: Projetos de Sistemas
sidebar_label: Projetos de Sistemas
slug: /newsletter/002/sistemas
---


Os projetos de sistema tem como objetivo promover uma melhor utilização de recursos computacionais, a implementação e o desenvolvimento de software e serviços voltados para as pesquisas na área de Relações Internacionais. Aqui, os projetos estão relacionados com a construção e a manutenção de estações de trabalho multifuncionais, além de softwares e serviços utilizados e/ou mantidos pelo próprio laboratório.

Os projetos de sistemas que se encontram ativos neste momento são: edição do site do LabRI/UNESP, cadernos LabRI, sistema de OCR automático e a infraestrutura computacional.


import Tabs from '@theme/Tabs';
import TabItem from '@theme/TabItem';

<Tabs>
  <TabItem value="edição site" label="Edição do site do LabRI/UNESP" default>
  </TabItem>
  <TabItem value="cadernosLabRI" label="Cadernos LabRI" default>
  </TabItem>
  <TabItem value="sistema OCR" label="Sistema de OCR Automático" default>
  </TabItem>
  <TabItem value="insfraestrutura" label="Infraestrutura Computacional" default>
  </TabItem>
</Tabs>