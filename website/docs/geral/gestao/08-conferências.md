---
id: conferências
title: Vídeo Conferências e Gravação de eventos
sidebar_label: Vídeo Conferências e Gravação de eventos
slug: geral/gestao/conferências
---

## Realização de Video-Conferência na Sala do LabRI

O estagiário é responsável pelo apoio à videoconferências realizadas no LabRI, uma vez que o espaço conta com hardware e software para fazê-lo. Dessa forma fica a cargo do usuário as seguintes tarefas:

* Indicar computadores para videoconferência:

> processadores x4 - 109, 117, 113 etiquetados em Verde

* Indicar possibilidades de software de videoconferencia: GoogleMeet, Whereby, Talky.io, jitsi:

> 1 webcams 720p
> 3 webcams 1080p
> Headsets

Inventários Visuais:
[Inventário 1](https://drive.google.com/open?id=1WAwgd-hHadE-bH1Cz1xfHH7SBWB_j486)
[Inventário 2](https://drive.google.com/open?id=1ExgJ7tc3AbgofeAaZPIRe0UyjJLwkgEH)

Os kits estão localizados no armário 7
 
* Indicar webcam, headset, caixas de som e microfone disponíveis
* Indicar utilização do projetor

## Apoio na Gravação de Eventos 

O estagiário também fornece apoio aos eventos realizados por grupos de extensão e centros acadêmicos na captação de imagem e som destes. Além disso pode também ser responsável pela posterior edição e publicação dessas gravações nas redes sociais, assim como a digladiação dessas. Isso é possível graças ao material que o LabRI dispõe, listado abaixo: 

> (1) os equipamentos disponíveis para gravação de vídeos e captura de áudios; 

  > 3 WebCams 1080p
  > 1 Webcam 720p
  > Gravador Digital Sony
  > Gravador Digital Polycom
  > Microfone tipo Lapela com fio
  > Tripé para celular
  > Tripé para Cãmera
  > Microfone tipo Lapela Sem fio

Consultar [Inventário](https://docs.google.com/spreadsheets/d/1S9yhTUhsomITqkEtqDa5_7nbhUHozYjD5k_taB-UwDs/edit#gid=1987720488)

> (2) regras para empréstimos e utilização destes equipamentos; 

> (3) instruções e dicas de como utilizar estes equipamentos; 

> (4) instruções e dicas de como editar os vídeos e áudios.

## Instruções para videochamada 

#### Para a melhor experiência de videochamadas deve-se sempre, antes de entrar na sala:

* **Verificar se o volume do computador está adequado**

Se o volume do computador estiver muito alto poderá resultar em retorno no áudio da chamada, assim como se estiver muito baixo a chamada não será bem ouvida

* **Verificar se o volume de captação do microfone está adequado**

Muitas vezes o microfone está configurado corretamente, mas o seu volume de captação não, podendo resultar em um áudio "estourado" ou baixo demais

* **Verificar se a entrada e saída de áudio estão configuradas corretamente**

A entrada e saída de áudio deve ser configurada corretamente para o melhor aproveitamento da chamada de vídeo. Além disso, em alguns aplicativos de videochamada a troca não é realizada de maneira automática pelo computador.

* **Verificar se não está mutado na chamada, quando necessidade de falar**

Lembre-se também de:

* *Manter-se mutado a menos que necessite dizer algo*

* *Desligue a câmera em caso de má conexão*