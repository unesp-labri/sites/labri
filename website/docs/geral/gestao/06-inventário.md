---
id: inventário
title: Inventário
sidebar_label: Inventário
slug: geral/gestao/inventário
---

A gestão dos equipamentos do LabRI/UNESP é feita através dos três inventários abaixo:

- [Inventário de equipamentos](https://docs.google.com/spreadsheets/d/1wN2t-nQgQ9kB5eGdHSpSfczbhlyDg1z9Ee8aeqvbmo8/edit#gid=0)  
- [Armário 1](https://docs.google.com/drawings/d/1PIrpghGP-2hh04NAfQf8PtuTNuRE0sKERT0HRBGrXWs/edit?usp=sharing) - Clicar em "abrir com diagrams.net"
- [Armário 2](https://docs.google.com/drawings/d/1D8WGZAjxLoydXTqa4U8pXgcNjw4fy-OuhOVvPDH1kHU/edit?usp=sharing) - Clicar em "abrir com diagrams.net"
- [Mapa do LabRI](https://app.diagrams.net/#G0BzDme5Ht7oi2WWRTWmpfY1g1ckU) - Clicar em "abrir com diagrams.net"

### Limpeza dos Computadores

A limpeza dos computadores e servidores é uma das funções imprescindíveis para o bom funcionamento e refrigeração.

Há dois tipos de limpeza:

1. A primeira é externa, na qual deve-se limpar os computadores por fora - realizada mensalmente

2. A segunda interna, para limpar os componentes e a poeira que fica dentro dos computadores - realizada anualmente 

Pode-se acessar por este [link](https://drive.google.com/drive/u/1/folders/0BzDme5Ht7oi2cVdnY2tnZ1Q1eGc)  O Inventário visual do armário no qual se encontram os equipamentos de limpeza e manutenção do laboratório

Há também uma tarefa que deve ser atualizada com o tempo utilizado para fazê-la e então, reagendá-la para a próxima data de execução:

### Organização dos Armários 

É função do estagiário organizar e manter a organização dos armários do laboratório. Para tal, usa-se de guia os Inventário visuais que podem ser acessados pelo "Acesso Rápido" (canto superior direito) ou pelo [link](https://drive.google.com/drive/u/1/folders/1qVHPoNjhzy71gmtBVTRJdnG1TjxDeHFo)