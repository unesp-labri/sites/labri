---
id: tutorial-openshot
title: Tutorial de utilização do Openshot
sidebar_label: Tutorial de utilização do Openshot
slug: geral/gestao/comunicacao/tutorial-openshot
---

## Tutorial: Como sincronizar áudio e imagem no OpenShot

### Baixando o aplicativo

1. [OpenShot Video Editor](https://www.openshot.org/): Página de download oficial

### Importando os arquivos

1. Separe em seu computador o arquivo de áudio e o arquivo de imagem que serão utilizados no vídeo

2. Inicialize seu OpenShot abrindo o aplicativo

3. Em seguida, clique em "Arquivo" e selecione a opção "Importar Arquivos..."

![Exemplo](https://i.imgur.com/XtevGq6.gif)


### Sincronizando as faixas

1. Arraste os arquivos importados para as faixas que aparecem no canto inferior do aplicativo

2. Solte cada arquivo em uma faixa diferente

![Exemplo](https://i.imgur.com/3d6hy5S.gif)

3. Ajuste os tamanhos para que seu arquivo de imagem contemple a extensão do arquivo de áudio

![Exemplo](https://i.imgur.com/i2dujrs.gif)

4. Ao ajustar, você pode conferir a prévia clicando na seta do canto superior direito

### Exportando o vídeo

1. Em seguida, exporte seu vídeo clicando no botão vermelho do canto superior central

2. Ajuste as definições necessárias segundo sua necessidade

3. Clique em "Exportar Vídeo" para dar início a randerização de seu vídeo

![Exemplo](https://i.imgur.com/PnIGR34.gif)