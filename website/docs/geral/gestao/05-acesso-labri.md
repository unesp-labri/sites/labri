---
id: acesso-labri
title: Acesso à Sala do LabRI
sidebar_label: Acesso à Sala do LabRI
slug: geral/gestao/acesso-labri
---

## Acesso sala do LabRI (usuários com chave da sala do LabRI)

O LabRI está aberto ao público interessado, ainda que o estagiário não esteja presente, isso é realizado graças à cópias de chaves que podem ser requisitadas pelos estudantes que o usem regularmente. O número de chaves e a relação de pessoas é um dos trabalhos realizados pelo estagiário, que deverá atualizar a planilha sempre que houver alguma alteração. Para que um aluno requisite a chave ele deve preencher o formulário abaixo e será função do estagiário atualizar a planilha. O estudante deverá então entregar uma chave ao usuário, que fará uma cópia (com a qual o mesmo deve arcar com o custo) e então devolver a original ao estagiário.

* [Planilha Acesso por chave](https://docs.google.com/spreadsheets/d/1RrmHyBXUpvFQVzrG0S0ARIGHONYFkDM1qs3tXdOX6Uw/edit#gid=0)

* [Formulário](https://forms.gle/25HBq16Ko5SBdxC37)