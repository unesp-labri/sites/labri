---
id: atendimento
title: Atendimento e Suporte aos docentes e discentes
sidebar_label: Atendimento
slug: /atendimento
---

####  Veja abaixo as maneiras de entrar em contato com a equipe do LabRI/UNESP:  

 - Preferencialmente pedimos que o contato seja realizado: 
    - através do e-mail (unesplabri@gmail.com)
    - Entrar na [sala virtual](https://meet.google.com/rsq-fhjc-cwm) do Google Meet 
    

 - Também é possível entrar em contato através das Redes Sociais do LabRI/UNESP (pode ser que a resposta demore mais para ser feita)
 
    - [Facebook](https://www.facebook.com/labriunesp)
    - [Twitter](https://twitter.com/labriunesp)
    - [Instagram](https://www.instagram.com/unesplabri/)
    - [LinkedIn](https://www.linkedin.com/company/labri-unesp-franca/)
    - [Youtube](https://www.youtube.com/channel/UCHx_m-4Cv7_ZLEDUe_wYATA/featured)
  
 
:::info

Nos horários indicados abaixo, os colaboradores estão disponíveis na sala virtual do Google Meet

:::

|                                    | Segunda       | Terça         | Quarta        | Quinta        | Sexta
| ---------------------------------- | ------------  | ------------- | ------------  | ------------- | -------
| Funcionamento                      | 09h00-21h00\* | 09h00-21h00\* | 09h00-21h00\* | 09h00-21h00\* | 09h00-21h00\*
| Atendimento (RAFAEL/BRENO)  | 08h30-12h30¹  | 08h30-12h30¹  | 08h30-12h30¹   | 08h30-12h30¹        | 08h30-12h30¹
| Atendimento (RAFAEL/LAIS)   | 08h30-12h30¹  | 13h00-17h00¹  | 08h30-12h30¹   | 08h30-12h30¹ | 17h00-21h00¹
| Atendimento (RAFAEL/VICTOR) | 13h00-17h00¹  | 13h00-17h00¹  | 08h30-12h30¹   | 08h30-12h30¹ | 17h00-21h00¹
| Atendimento eventual        | 17h-19h²     | 17h-19h²      | 17h-19h²       | 17h-19h²       | 17h-19h² 

 \* Horário disponível para aqueles que possuem acesso direto ao laboratório. Aqueles que não possuem acesso direto ao laboratório devem seguir o horário de atendimento dos estagiários (Funcionamento suspenso devido a pandemia). 
 
 ¹Atendimento: neste horário, os estagiários e colaboradores do LabRI desenvolvem as atividades em conjunto e estão disponíveis no link do meet indicado acima 

 ²Atendimento eventual: neste horário, os estagiários e colaboradores do LabRI prestam apoio de forma eventual, não estando necessariamente disponíveis no meet e presencialmente sem agendamento prévio 


### Utilização da sala do LabRI/UNESP para vídeo-conferências 

-  Abaixo estão listados os horários que determinados grupos utilizam a sala do LabRI para suas atividades 

| atividade/Projetos            | Segunda       | Terça       | Quarta      | Quinta         | Sexta
| ----------------------------- | ------------  | ----------- | ----------  | -------------- | -------
|          LANTRI               |       -       |      -      |      -      |      -         |      -     
|                               |       -       |      -      |      -      |      -         |      -     
|                               |       -       |      -      |      -      |      -         |      -
|                               |       -       |      -      |      -      |      -         |      -   

 \* Reuniões quinzenais

### Reuniões de Trabalho em Torno de Projetos Integrados

- Para saber mais sobre os Projetos Integrados, clique [Aqui](https://labriunesp.org/projetos/)

| atividade/Projetos     | Segunda     | Terça       | Quarta      | Quinta      | Sexta
| ---------------------- | ------------| ----------- | ----------  | ----------- | -------
| RI e Internet          |       -     | 08h00-10h00 |      -      |      -      |
| NewsCloud              |       -     |     -       |      -      |      -      | 14h00-16h00 
| GovLatinAmerica        |       -     |     -       | 10h00-12h00 |      -      |      -
| Tweepina               | 10h00-12h00 |     -       |      -      |      -      |      - 
| Hemeroteca  PEB        | 16h45-18h45 |     -       |      -      |      -      |      - 
| IRjornalsBR            |             | 10h00-12h00 |      -      |      -      |      -
| DiariosBr              |       -     |     -       |      -      |      -      | 08h00-09h30
| MercoDocs              |       -     |     -       |      -      |      -      | 10h00-12h00     
| Infra Computacional    |       -     |     -       |      -      | 14h00-15h30 |      -   

### Tipos de Reuniões 

- Reuniões de planejamento
    - São encontros em que são repassados informes gerais, são definidas as próximas tarefas de cada colaborador, e ocorre uma revisão da dinâmica do LabRI/UNESP. Normalmente estas Reuniões contam com a presença do coordenador do LabRI/UNESP. 

- Reuniões de trabalho 
    - São encontros em que desenvolvemos alguma atividade específica. Tal atividade pode ser relacionada a algum projeto integrado específico, ao apoio a algum grupo de pesquisa, docente ou discente, ou ao auxílio na manutenção da infraestrutura disponível no LabRI/UNESP.
    - Em grande medida, tais reuniões têm um forte vínculo com algum subprojeto dentre os projetos integrados.

- Reuniões de treinamento 
    - São encontros em que os colaboradores são introduzidos ao funcionamento geral do LabRI/UNESP, à dinâmica de algum projeto integrado específico ou é realizado alguma oficina voltada ao aprendizado de alguma tecnologia digital.






