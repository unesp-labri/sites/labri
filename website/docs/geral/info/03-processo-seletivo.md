---
id: processo-seletivo
title: Informações sobre o processo seletivo
sidebar_label: Processo Seletivo
slug: /geral/info/processo-seletivo
---

## Processo Seletivo 2024



:::info O PROCESSO SELETIVO DO LabRI/UNESP PARA ESTÁGIO REMUNERADO ESTÁ FECHADO

::: 

**RESULTADO FINAL 2024 - [CLIQUE AQUI](https://docs.google.com/document/d/1R4x4mgLY25kyEQ14QtubuBiD73fezzhrdHojM0fioa4/edit)**


Data de inscrição: **19/09/2024** até **03/10/2024**

Carga Horária: **30h/semanais**

Remuneração mensal: **R$900,00**

Vale transporte: **R$68,00**

Período do estágio: **validade inicial de um (1) ano a partir da data de contratação**

Formulário de inscrição: **[CLIQUE AQUI](https://forms.gle/9fzer2d9RHpVpTq17)**

Tendo em vista que um dos principais objetivos do Laboratório de Relações Internacionais é o aprimoramento das atividades desenvolvidas no curso de Relações Internacionais através da incorporação do uso de novas tecnologias da informação e comunicação nas atividades de pesquisa, extensão e docência, as atividades dos interessados em estagiar no Laboratório estarão relacionadas ao aprendizado, operação, auxílio, suporte e disseminação destes diversos conhecimentos, que poderão ser aplicados na área de atuação profissional nas Relações Internacionais a que o estagiário se dedique futuramente. 

O 1º classificado deverá manifestar interesse em assumir a função até as **18 horas do dia 07/10/2024**, entrando em contato com o LabRI/DERI via e-mail: *unesplabri@gmail.com*  - conforme edital.

O resultado do processo será divulgado no dia **04/10/2024 até as 22h**,.

A não manifestação de interesse implicará na convocação do próximo classificado.

Deverá  também enviar, para os e-mails *unesplabri@gmail.com* e *deri.franca@unesp.br* as seguintes informações: Nome completo, endereço completo, número do PIS (se possuir), RG, CPF, e-mail e telefone.

Deverá entrar em contato com a Seção Técnica de Saúde - STS, da FCHS através do e-mail *sts.franca@unesp.br* para informações sobre exame admissional.

Deverá abrir conta no Banco do Brasil (se não possuir) e informar o número da conta corrente, agência e cidade.

### Editais Anteriores

No link a seguir, você pode consultar os editais encerrados para oportunidades de estágio no LabRI:

#### Seleção 2023:

- [Edital 2023](https://docs.google.com/document/d/1kTYcJvJP-Onp6iIegS7P-d0_BnGveE_iNpc55tIdeiA/edit?usp=sharing)
- [Resultado final - 2023](https://docs.google.com/document/d/1__bPc5YqdwnPPIeRS66CWUWqBa02YDTEwf2OTtaNyHg/edit?usp=sharing)

#### Seleção 2022:

- [Edital 2022](https://docs.google.com/document/d/1URuPX1SPdvu3ufb6zsNN56up8NZnwFaIn2cEoPflVak/edit)
- [Resultado final - 2022](https://docs.google.com/document/d/1lXzcKYbfCeDImyuYTOQeDCWM64yzsqHlkI0R0I5MDdc/edit)

#### Seleção 2021:

- [Edital 2021](https://docs.google.com/document/d/1zyil-GuzABcZwkEaDRYwKjxk6QFZzwDcqHu_b-TEGUY/edit)
- [Resultado final - 2021](https://docs.google.com/document/d/1SE8mO0OOaM2g4RuyVH98EVPGVAtZLnheGP-4RI4AOvo/edit)

#### Seleção 2018:

- [Edital 2018](https://docs.google.com/document/d/1UyHQ99sERyZghOUYx7J044-JGpanOZigBkRgytu0g3I/edit)

- [Resultado final - 2018](https://docs.google.com/document/d/1xYcD2d6Q6JWuTiUlDeCLxjzC0Aufw63Tu9hNA32Mo54/edit)

#### Seleção 2016:

- [Edital 2016](https://docs.google.com/document/d/1xJBvAqOrvjzNR18bonDaMbY2K0GG5N7tLiawg2tdjSI/edit)

- [Resultado final - 2016](https://docs.google.com/document/d/1X33x9Go0ZDUp6fvV8kpeO3erNNEnJ-fgJeNAR4VVTmg/edit)

