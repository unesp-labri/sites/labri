---
id: como-publicar
title: Como Publicar
sidebar_label: Como publicar
slug: /cadernos/como-publicar
---

### Procedimentos para publicação

Atualmente, a publicação nos Cadernos LabRI/UNESP está circunscrita a trabalhos técnicos-científicos desenvolvidos pelos docentes, discentes e colaboradores ligados ao curso de graduação em Relações Internacionais da UNESP, campus Franca/SP, e ao Programa de Pós-Graduação em Relações Internacionais San Tiago Dantas (UNESP, UNICAMP, PUC-SP). 
